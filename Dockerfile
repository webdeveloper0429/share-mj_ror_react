FROM ruby:2.4.1
MAINTAINER Christian Dyl <christian.dyl@outlook.com>

# Using bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# ENV vars
ENV RAILS_ENV production
ENV APP_PATH /app

# Installing and updating deps
RUN apt-get update \
    && apt-get install -qq -y --force-yes --fix-missing --no-install-recommends \
    libpq-dev libssl-dev postgresql-client apt-transport-https


# nvm environment variables
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 6.11.1

# install nvm
# https://github.com/creationix/nvm#install-script
RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash

# install node and npm
RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Add Yarn repository
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn -y

# Creating app path
RUN mkdir -p "$APP_PATH/lib/gems" && \
    mkdir -p "$APP_PATH/tmp/cache" && \
    mkdir -p "$APP_PATH/tmp/pids" && \
    mkdir -p "$APP_PATH/tmp/sessions" && \
    mkdir -p "$APP_PATH/tmp/sockets"

# Copying entrypoint
COPY docker/files/start.sh /start
RUN chmod a+x /start

# Setting app work path
WORKDIR $APP_PATH

# Installing gems
COPY Gemfile* $APP_PATH/
RUN bundle install --without test development

# Installing packages
COPY package.json $APP_PATH/
RUN yarn install

# Copy in the application code from your work station at the current directory
COPY . $APP_PATH

# Precompiling assets
RUN bundle exec rails assets:precompile

EXPOSE 3000
ENTRYPOINT ["/start"]
