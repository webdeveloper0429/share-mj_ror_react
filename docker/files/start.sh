#! /bin/bash
set -e

cd $APP_PATH

ACTION="${1:-$START_COMMAND}"

if [ "$ACTION" = "start_rails" ]; then
  echo "Running rails process"
  exec bundle exec puma -C config/puma.rb
elif [ "$ACTION" = "start_sidekiq" ]; then
  echo "Running sidekiq process"
  exec bundle exec sidekiq $SIDEKIQ_OPTS
elif [ "$ACTION" = "start_cable" ]; then
  echo "Running cable process"
  exec bundle exec puma -p 28080 cable/config.ru
elif [ "$ACTION" = "start_clockwork" ]; then
  echo "Running clockwork process"
  exec bundle exec clockwork config/clockwork.rb
else
  echo "Running command"
  exec "$@"
fi
