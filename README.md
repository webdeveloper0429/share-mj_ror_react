# ShareMJ

How to start work with front
```
# git clone project
$ git clone git@bitbucket.org:username/reponame.git

# go to project folder
$ cd sharemj

# Install yarn
$ brew update && brew install yarn

# Install node modules
$ yarn install

# Create front.env
$ cp example.front.env front.env

# Start webpack dev server
$ yarn start

# Go to index page
$ open http://localhost:8080/packs/index.html

# Optional
# You can create soft link to easly access webpack root folder
# ln -s app/javascript/packs app_client 

# Webpack config exists here config/webpack
# Webpack root is here app/javascript/packs
# You can modify env vars for app in  front.env
# Note that you should define API_PATH variable if you use webpack without Rails
```
