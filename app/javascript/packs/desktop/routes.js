import React from 'react';
import { Switch, Route } from 'react-router-dom';

import RootLayout from './components/layouts/RootLayout';
import Footer from './components/layouts/Footer';
import HomePage from './containers/HomePage';
import DashboardPage from './containers/DashboardPage'
import TermsOfService from './components/Pages/TermsOfService';
import PrivacyPolicy from './components/Pages/PrivacyPolicy';
import Copyright from './components/Pages/Copyright';

import { dashboardPath, staticPagesPath } from '../core/paths'
console.log('DashboardPage');
export default () => (
  <main>
    <RootLayout>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/password-recovery" component={HomePage} />
        <Route path={dashboardPath()} component={DashboardPage} />
        <Route path={staticPagesPath('terms-of-service')} component={TermsOfService} />
        <Route path={staticPagesPath('privacy-policy')} component={PrivacyPolicy} />
        <Route path={staticPagesPath('copyright')} component={Copyright} />
      </Switch>
    </RootLayout>
    <Footer />
  </main>
);