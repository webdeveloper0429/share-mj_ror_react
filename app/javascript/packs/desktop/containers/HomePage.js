import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { Helmet } from "react-helmet";
import Login from '../components/Login';
import SignUp from '../components/SignUp';
import { withRouter } from 'react-router';
import {Navbar, Header, Brand, Toggle, Collapse, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';
import { RecoveryRequest } from '../components/RecoveryPassword';
import { RecoveryComplete } from '../components/RecoveryPassword';

import { dashboardPath } from '../../core/paths'

class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showLoginModal: false,
      showSignUpModal: false,
      showRecoveryRequestModal: false,
      showRecoveryCompleteModal: false,
      zipCode: null,
      token: "",
      address: null
    };

    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleLoginModalClose = this.handleLoginModalClose.bind(this);
    this.handleSignUpClick = this.handleSignUpClick.bind(this);
    this.handleSignUpModalClose = this.handleSignUpModalClose.bind(this);

    this.handleRecoveryRequestClick = this.handleRecoveryRequestClick.bind(this);
    this.handleRecoveryRequestModalClose = this.handleRecoveryRequestModalClose.bind(this);

    this.handleRecoveryCompleteClick = this.handleRecoveryCompleteClick.bind(this);
    this.handleRecoveryCompleteModalClose = this.handleRecoveryCompleteModalClose.bind(this);

    this.onChangeZip = this.onChangeZip.bind(this);
    this.handleZipCode = this.handleZipCode.bind(this);
  }

  componentDidMount() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.handleZipCode, (err)=> {
        console.log(err);
      },
      {
        enableHighAccuracy: true, timeout: 20000, maximumAge: 100
      });
    }
  }

  componentWillMount() {
    const urlSearchParams = new URLSearchParams(this.props.location.search);
    const token = urlSearchParams.get("token");
    if(token) {
      this.setState({
        ...this.state, 
        showRecoveryCompleteModal:true, 
        token});
    }
  }

  handleZipCode(position) {
    const geocoder = new google.maps.Geocoder;
    const latlng = {lat: position.coords.latitude, lng:  position.coords.longitude};
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        for(let i=0; i < results[0].address_components.length; i++)
        {
          let component = results[0].address_components[i];
          if(component.types[0] == "postal_code")
          {
            document.getElementById("home-zip-code").value = results[0].formatted_address;
            this.setState({
              ...this.state,
              zipCode: component.long_name,
              address: results[0].formatted_address
            })
          }
        }
      }
    }.bind(this))
  }

  handleLoginClick(e) {
    e.preventDefault();
    this.setState({ ...this.state, showLoginModal: true });
  }

  handleLoginModalClose(successLogin) {
    console.log(successLogin);
    this.setState({ ...this.state, showLoginModal: false });
    if (successLogin) {
      window.location.href = dashboardPath();
    }
  }

  handleSignUpClick(e) {
    this.setState({ ...this.state, showSignUpModal: true });
  }

  handleSignUpModalClose(successSignUp) {
    this.setState({ ...this.state, showSignUpModal: false });
    if (successSignUp) {
      window.location.href = dashboardPath();
    }
  }

  handleRecoveryRequestClick(e) {
    this.setState({ ...this.state, showRecoveryRequestModal: true, showLoginModal: false });
  }

  handleRecoveryRequestModalClose() {
    this.setState({ ...this.state, showRecoveryRequestModal: false });
  }

  handleRecoveryCompleteClick(e) {
    this.setState({ ...this.state, showRecoveryCompleteModal: true});
  }

  handleRecoveryCompleteModalClose() {
    this.setState({ ...this.state, showRecoveryCompleteModal: false });
  }

  onChangeZip(e) {
    const zip = e.target.value;
    if(zip.length == 5) {
      this.setState({...this.state, zipCode:e.target.value, showSignUpModal: true});
    }else{
      this.setState({...this.state, zipCode:e.target.value})
    }
  }

  render() {
    const { showLoginModal, showSignUpModal, showRecoveryRequestModal, showRecoveryCompleteModal, zipCode, address } = this.state;
    return (
      <div id="main-container">

        <Helmet>
            <meta charSet="utf-8" />
            <title>ShareMJ</title>
            <link rel="canonical" href="http://mysite.com/example" />
        </Helmet>

        {showLoginModal ? <Login onClose={this.handleLoginModalClose} handleRecoveryRequestClick={this.handleRecoveryRequestClick}/> : null}
        {showSignUpModal ? <SignUp onClose={this.handleSignUpModalClose} zipCode={zipCode} /> : null}
        {showRecoveryRequestModal ? <RecoveryRequest onClose={this.handleRecoveryRequestModalClose} /> : null}
        {showRecoveryCompleteModal ? <RecoveryComplete onClose={this.handleRecoveryCompleteModalClose} token={this.state.token} /> : null}

        <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <a className="navbar-brand" id="logo" title="ShareMJ" href="/" style={{visibility: 'visible'}}>
                <img height={25} className="header-mj-logo" alt="ShareMJ Logo" src="/shareMJ-white-logo.png" />
              </a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              <NavItem eventKey={1} href="" onClick={this.handleLoginClick}> Sign In</NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <div id="flash-messages" style={{display: 'none'}}>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="row">
                <div className="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 text-center" id="content" style={{marginTop: '80px'}}>
                  <h1>We connect you with your share marijuana community</h1>
                  <h2>Free membership. Additional credits available</h2>
                    <form className="get-connected" action="/users/modal/questions/start" acceptCharset="UTF-8" method="get">
                      <input name="utf8" type="hidden" value={zipCode && address ? address : ""}></input>
                      <input id="home-zip-code" name="zip_code" pattern="[0-9]*" placeholder="Enter zip code" title="Zip code" type="text" onChange={this.onChangeZip}></input>
                      <button type="button" onClick={this.handleSignUpClick}>Get Started</button>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

HomePage.contextTypes = {
  router: PropTypes.object.isRequired
}

function mapStateToProps (state) {
  return {
    session: state.session
  }
}

export default withRouter(connect(mapStateToProps)(HomePage));
