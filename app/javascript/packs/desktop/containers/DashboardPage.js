import React, { Component } from 'react'
import { connect } from 'react-redux'

class DashboardPage extends Component {
  render() {
    return (
      <div id="main-container">
        Dashboard page
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    session: state.session
  }
}

export default connect(mapStateToProps)(DashboardPage)
