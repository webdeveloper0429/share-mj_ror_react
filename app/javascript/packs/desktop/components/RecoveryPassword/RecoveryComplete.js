
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Survey} from 'survey-react';
import {Modal, Button, ButtonToolbar, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import axios from 'axios';

import {recoveryComplete} from '../../../core/actions/authActions';

class RecoveryComplete extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showRecoveryCompleteModal: true,
      password: "",
      repeatPassword: "",
      errors: {
        isSame: {
          active: false,
          message: "Passwords is not same"
        }
      },
      token: this.props.token
    };

    this.close = this.close.bind(this);
    this.onChange = this.onChange.bind(this);
    this.recoveryComplete = this.recoveryComplete.bind(this);
  }

  close() {
    this.setState({ ...this.state, showRecoveryCompleteModal: false });
    setTimeout(() => {
      this.props.onClose();
    }, 1000);
  }

  recoveryComplete() {
    var {errors, password, repeatPassword} = this.state;
    if(password !== repeatPassword) {
      errors.isSame.active = true;
      this.setState({
        errors
      })
    }else{
      errors.isSame.active = false;
      this.setState({
        errors
      })
      this.props.recoveryComplete(this.state.password, this.state.token);
    }
  }

  onChange(e) {
    let state = this.state;
    state[e.target.id] = e.target.value;
    this.setState({
      state
    })
  }

  render() {
    const { showRecoveryCompleteModal, email, errors } = this.state;
    const { recoveryPassword } = this.props;

    return (
       <div>
        <Modal show={showRecoveryCompleteModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Recovery complete</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {recoveryPassword.isSuccess ? <IsSuccessBody /> : <CompleteBody email={email} onChange={this.onChange} recoveryComplete={this.recoveryComplete} errors={errors} recoveryPassword={recoveryPassword}/>}
          </Modal.Body>
        </Modal>
      </div>
    )
  }
}

const IsSuccessBody = () => { 
  return (
    <div className="success"> 
      Password has been successfully changed !
    </div>
  )
}

const CompleteBody = ({password, repeatPassword, onChange, recoveryComplete, errors, recoveryPassword}) => {
  return (
    <div> 
      <FormGroup controlId="password">
        <ControlLabel>Password</ControlLabel>
        <FormControl type="password" value={password} onChange={onChange}/>
      </FormGroup>
      <FormGroup controlId="repeatPassword">
        <ControlLabel>Repeat password</ControlLabel>
        <FormControl type="password" value={repeatPassword} onChange={onChange}/>
      </FormGroup>

      {
        Object.keys(errors).map((res, index) => {
          if(errors[res].active) {
            return <FormGroup key={index}><span className="error"> {errors[res].message} </span></FormGroup>
          }
        })
      }
      {
        recoveryPassword.isError ? <FormGroup><span className="error"> Recovery token has been expired. Request new token please. </span></FormGroup> : ""
      }
      
      <ButtonToolbar>
        <Button bsStyle="success" onClick={recoveryComplete}>Complete password change</Button>
      </ButtonToolbar> 
    </div>
  )
}

RecoveryComplete.contextTypes = {
  router: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    recoveryPassword: state.recoveryPassword
  }
}

export default connect(mapStateToProps, {recoveryComplete})(RecoveryComplete);