import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Survey} from 'survey-react';
import {Modal, Button, ButtonToolbar, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import axios from 'axios';

import {recoveryRequest} from '../../../core/actions/authActions';

class RecoveryRequest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showRecoveryRequestModal: true,
      email: ""
    };

    this.close = this.close.bind(this);
    this.onChange = this.onChange.bind(this);
    this.recoveryRequest = this.recoveryRequest.bind(this);
  }

  componentWillReceiveProps(props) {
    if(props.recoveryPassword.isSuccess) {

    }
  }

  close() {
    this.setState({ ...this.state, showRecoveryRequestModal: false });
    setTimeout(() => {
      this.props.onClose();
    }, 1000);
  }

  recoveryRequest() {
    this.props.recoveryRequest(this.state.email);
  }

  onChange(e) {
    this.setState({
      ...this.state,
      email: e.target.value
    })
  }

  render() {
    const { showRecoveryRequestModal, email } = this.state;
    const { recoveryPassword } = this.props;

    return (
       <div>
        <Modal show={showRecoveryRequestModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Recovery request</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {recoveryPassword.isSuccess ? <IsSuccessBody /> : <RequestBody email={email} onChange={this.onChange} recoveryRequest={this.recoveryRequest}/>}
          </Modal.Body>
        </Modal>
      </div>
    )
  }
}

const IsSuccessBody = () => { 
  return (
    <div className="success"> 
      Request has been successfully sent ! 
      Check your mail please. 
    </div>
  )
}

const RequestBody = ({email, onChange, recoveryRequest}) => {
  return (
    <div> 
      <FormGroup controlId="email">
        <ControlLabel>Email</ControlLabel>
        <FormControl type="email" value={email} onChange={onChange}/>
      </FormGroup>
      <ButtonToolbar>
        <Button bsStyle="success" onClick={recoveryRequest}>Request password change</Button>
      </ButtonToolbar> 
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    recoveryPassword: state.recoveryPassword
  }
}

export default connect(mapStateToProps, {recoveryRequest})(RecoveryRequest);