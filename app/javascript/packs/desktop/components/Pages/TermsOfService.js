import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";
import {Navbar, Header, Brand, Toggle, Collapse, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';
import { staticPagesPath } from '../../../core/paths';

const TermsOfService = () => {
    return (
        <div id="main-container" className="static-page">

            <Helmet>
                <meta charSet="utf-8" />
                <title>ShareMJ</title>
                <link rel="canonical" href="http://mysite.com/example" />
            </Helmet>

            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a className="navbar-brand" id="logo" title="ShareMJ" href="/" style={{visibility: 'visible'}}>
                            <img height={25} className="header-mj-logo" alt="ShareMJ Logo" src="https://d3ely0vl8t3ihb.cloudfront.net/assets/shareMJ-white-logo-041e5131807629716e67996933c2f078578aef75fa4e7770084d179b1bf4ec4e.png" />
                        </a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
            </Navbar>

            <div className="container">
                <div className='row'>
                    <div className='col-xs-12'>
                        <div className='static'>
                            <h1>TERMS OF SERVICE</h1>
                            <p>These terms of service ("Terms") constitute a legal agreement between you (“User”) and Simple Technologies, ("Simple Tech", "we" or "us").  Please read these Terms carefully.  If you do not agree to these Terms, please do not access, install or use our Website, App or Services (each as deﬁned in the next paragraph).</p>
                            <p>By accessing or using our website (<Link to={staticPagesPath('terms-of-service')}>https://www.sharemj.com</Link>) and any subdomains of our website ("Website"), or accessing, installing or using the "ShareMJ" mobile application ("App"), or using or receiving the Website, App and any services supplied to you by Simple Tech (collectively, "Services"), you represent to us that you are legally competent to enter into and agree to these Terms.</p>
                            <p>
                                <b>THE SERVICES ARE INTENDED SOLELY FOR USERS WHO ARE 21 YEARS OF AGE OR OLDER, AND ANY REGISTRATION, USE OF OR ACCESS TO THE SERVICES BY ANYONE UNDER 21 IS STRICTLY PROHIBITED AND IN VIOLATION OF THESE TERMS.</b>
                            </p>
                            <p>
                                <b>BY USING THE THE SERVICES, YOU EXPRESSLY REPRESENT AND WARRANT THAT YOU ARE AT LEAST 21 YEARS OF AGE.</b>
                            </p>
                            <p>
                                <b>PLEASE NOTE THAT THESE TERMS CONTAIN A MANDATORY ARBITRATION OF DISPUTES PROVISION THAT REQUIRES THE USE OF ARBITRATION ON AN INDIVIDUAL BASIS TO RESOLVE DISPUTES (EXCEPT IN CERTAIN CIRCUMSTANCES), RATHER THAN JURY TRIALS OR className ACTION LAW SUITS.</b>
                            </p>
                            <p>
                                You acknowledge that these Terms are supported by reasonable and valuable consideration, the receipt and adequacy of which are hereby acknowledged.  Without limiting the foregoing, you acknowledge that such consideration includes, without limitation, your use of the Services and receipt or use of data, content, products and/or services through the Services.
                            </p>
                            <ol className='indent list-style-outside'>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Services</span></span>
                                        Our Services enable you to connect to your local marijuana community for the purpose of sharing marijuana in states where marijuana and marijuana products are legal.  All requests to connect placed through the App or the Website are accepted, reviewed, and may ultimately be fulﬁlled by users of our Services (“Sharers”), who are not affiliated in any way with Simple Tech, according to the terms that you and the Sharer may agree to.
                                        <b>YOU ACKNOWLEDGE AND AGREE THAT SIMPLE TECH DOES NOT SELL, OFFER TO SELL, INVITE TO SELL, OR SOLICIT ANY OFFERS TO SELL MARIJUANA OR MARIJUANA PRODUCTS.</b>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Fees</span></span>
                                        Simple Tech charges Sharers a fee to use its Services.  Sharers may purchase credits using the Services that allow them to connect to other users (“Requesters”).  Simple Tech does not currently charge Requesters a fee to use the Services.  In the future, we may, in our sole discretion, begin charging Requesters fees to use the Services. We will notify you before any Services you are then using will begin to carry a fee, and if you wish to continue using such Services at that time, you will be required to pay all applicable fees for such Services.
                                    </p>
                                    <p>
                                        <b>YOU AGREE THAT YOU MAY NOT AND WILL NOT PROVIDE SIMPLE TECH OR A SHARER ANY INFORMATION OF, OR USE THE SERVICES TO PROVIDE MARIJUANA OR MARIJUANA PRODUCTS TO ANY INDIVIDUAL UNDER 21 YEARS OF AGE.</b>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Promotional Oﬀers; Credits</span></span>
                                        Simple Tech, in its sole discretion, may oﬀer certain promotions and credits from time to time for users, to the extent permissible under applicable laws.  Depending upon where you are, you may or may not receive or be eligible to participate in these promotional oﬀers and credits. If we choose to extend a promotion or credit to you, Simple Tech will issue it, unless otherwise speciﬁcally stated in the terms and conditions of such promotion or credit.  Any such promotions and credits are non‐transferrable and may only be used in connection with the Services.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>License Grant</span></span>
                                        Simple Tech hereby grants to you, subject to these Terms, a personal, nonexclusive, nontransferable, limited license (without the right to sublicense) to access and use the Services (including updates and upgrades that replace or supplement it in any respect and which are not distributed with a separate license, and any documentation) solely for your personal use on a mobile or other device that you own or control.  These Terms do not permit you to install or use the App on a mobile or other device that you do not own or control and you may not distribute or make all or any portion of the Services available over a network where it could be used by multiple devices at the same time.  All rights not expressly granted herein are reserved by Simple Tech.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Mobile and Other Devices</span></span>
                                        You  are responsible for providing the mobile device, wireless service plan, software, Internet connections and/or other equipment or services that you
                                        need to download, install and use the App, Website or Services.
                                        <b>
                                            WE DO NOT GUARANTEE THAT THE APP, WEBSITE OR SERVICES CAN BE ACCESSED AND USED ON ANY PARTICULAR DEVICE OR WITH ANY PARTICULAR SERVICE PLAN.
                                            WE DO NOT GUARANTEE THAT THE APP, WEBSITE OR SERVICES WILL BE AVAILABLE IN, OR THAT REQUESTS CAN BE MADE FROM, ANY PARTICULAR GEOGRAPHIC LOCATION.
                                        </b>
                                        As part of the Services and to update you regarding the status of deliveries, you may receive push notiﬁcations on your mobile device ("Push Messages"),
                                        as well as via email or other types of messages. You acknowledge that, when you use the App, Website or the Services, your wireless service provider
                                        may charge you fees for data, text messaging and/or other wireless or Internet access, including in connection with Push Messages.
                                        You have control over the Push Messages settings, and can opt in or out of these Push Messages through your mobile device’s operating system
                                        (with the possible exception of infrequent, important service announcements and administrative messages).
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Use and Restrictions</span></span>
                                        You agree to protect the Services, and their proprietary content, information and other materials, from any unauthorized access or use, and you agree that you will not use the Services or such proprietary content, information or other materials except as expressly permitted herein or expressly authorized in writing by Simple Tech. You agree that: (i) you will not use the Services if you are not fully able and legally competent to agree to these Terms; (ii) you will only use the Services for lawful purposes and you will not use the Services for sending or storing any unlawful material or for fraudulent purposes or to engage in any illegal, oﬀensive, indecent or objectionable conduct or to harass, abuse, stalk, threaten, defame or otherwise infringe or violate the rights of any other party; (iii) you will not use the Services to advertise, solicit or transmit commercial advertisements, including "spam"; (iv) you will not use the Services to cause nuisance, annoyance or inconvenience; (v) you will not impair the proper operation of the Services’ network; (vi) you will not try to harm the Services in any way whatsoever; (vii) you will not copy, reproduce, adapt, create derivative works of, translate, localize, port or otherwise modify the Services, any updates, or any part of the Services or content therein without written permission from Simple Tech; (viii) you will only use the Services for your own use, not for commercial use, and will not distribute, sell, assign, encumber, transfer, rent, lease, loan, sublicense, modify, time‐share or otherwise exploit the Services in any unauthorized manner, including but not limited to  by trespass or burdening network capacity, or use the Services in any service bureau arrangement; (ix) you will not attempt to obtain any information or content from the Services using any robot, spider, scraper or other automated means for any purpose; (x) you will keep secure and conﬁdential your account password or any identiﬁcation we provide you which allows access to the Services; (xi) you will only use an access point or data account which you are authorized to use; (xii) you and any recipient of your orders will provide whatever proof of identity and age reasonably requested by the Sharer, its employee or agent, or the shipping or delivery company used by the Sharer or that shipping or delivery company’s employee or agent;  and (xii) you will not permit any third party to engage in any of the acts described in clauses (i) through (xi). You understand and agree that you are not permitted to: (w) remove or alter any copyright or other proprietary rights’ notice or restrictive rights legend contained or included in the Services; (x) decompile, disassemble, reverse compile, reverse assemble, reverse translate or otherwise reverse engineer the App, Website, any updates to or portion of the App or Website (except as and only to the extent any foregoing restriction is prohibited by applicable law or to the extent as may be permitted by the licensing terms governing use of any open sourced components included with the App or Website); (y) use any means to discover the source code of the  App  or  Website  or  to  discover  the  trade  secrets  in  the  Services;  or  (z)  otherwise  circumvent any functionality that controls access to or otherwise protects the Services. Any attempt to do any of the foregoing is a violation of the rights of Simple Tech and its licensors. If you breach these restrictions, you may be subject to prosecution and damages.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Additional Prohibitions</span></span>
                                        All users must comply with all applicable laws, these Terms of Service and all posted website and app rules.  Use of the website, app, or Services is strictly for the purposes described in Section 1 above.  Any use to the contrary or for any other purposes is a violation of these Terms.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Objectionable Material</span></span>
                                        You understand that by using any of the Services, you may encounter content that may be deemed oﬀensive, indecent, or objectionable, which content may or may not be identiﬁed as having explicit language, and that the results of any search or entering of a particular URL may automatically and unintentionally generate links or references to objectionable material. If you object to any part of the Services, you should cease using the Services.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Applicable Laws</span></span>
                                        The laws of the District of Columbia, excluding its conﬂicts of law rules, govern these Terms.  Your use of the Services may also be subject to other local, state, national, or international laws. Simple Tech makes no representation as to any laws, rules or regulations of any jurisdiction regarding the sale transportation, import, shipment or delivery of marijuana or marijuana products. Simple Tech shall not be liable for any loss or damage arising from your failure to comply with the terms set forth in these Terms or to comply with all such applicable laws. You agree to comply at your sole expense with all applicable United States laws and regulations. Simple Tech explicitly reserves the right to refuse access to any portion of the Services at any time without notice for your failure to abide by the terms as set forth in these Terms or your failure to comply with all such applicable laws.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Apple App Store</span></span>
                                        This Paragraph 10 applies to you only if you are using the App from the Apple App Store. Simple Tech and you, the end‐user of the Services, acknowledge that these Terms are entered into by and between Simple Tech and you and not with Apple, Inc. ("Apple"). Notwithstanding the foregoing, you acknowledge that Apple and its subsidiaries are third‐party beneﬁciaries of these Terms and that Apple has the right (and is deemed to have accepted the right) to enforce these Terms. Simple Tech is solely responsible for the App and any content contained therein. You acknowledge that Apple has no obligation whatsoever to furnish any maintenance and support services with respect to the App. You and we acknowledge that in the event of any failure of the App to conform to any applicable warranty, you may notify Apple, and Apple will refund the purchase price, if any, for the App to you. To the maximum extent permitted by applicable law, Apple will have no other warranty obligation whatsoever with respect to the App, and will have no responsibility for any other claims, losses, liabilities, damages, costs or expenses attributable to any failure to conform to any warranty. You and we acknowledge that Apple is not responsible for addressing any claims made by you or any third party relating to the App or your possession and/or use of the App, including, but not limited to: (i) product liability claims, (ii) any claim that the App fails to conform to any applicable legal or regulatory requirement, and (iii) claims arising under consumer protection or similar legislation.
                                        You and we acknowledge that in the event of any third party claim that the App, or your possession or use of the App, infringes such third party’s intellectual property rights, Apple will not be responsible for the investigation, defense, settlement and discharge of any such claim.
                                        You acknowledge that you have reviewed the <a target="_blank" className="underline" href="http://www.apple.com/legal/internet-services/itunes/us/terms.html#APPS">App Store Terms and Conditions</a>.
                                        Capitalized terms not deﬁned in this Paragraph 11 shall have the meanings set forth in the App Store Terms and Conditions. Your use of the App must comply with the App Store Terms and Conditions.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Google Play Store</span></span>
                                        This Paragraph 11 applies to you only if you are using the App from Google, Inc. or one of its aﬃliates or successors ("Google") via Google Play.
                                        To the extent of any conﬂict between the Google Terms of Service, the Google Play Business and Program Policies and such other terms
                                        which Google designates as default end user license terms for Google Play (all of which together are referred to as the "Google Play Terms")
                                        on the one hand and these Terms, the Google Play Terms shall apply with respect to your use of the App.
                                        The Google Play Terms can be accessed here: <a target="_blank" className="underline" href="https://play.google.com/intl/en_us/about/play-terms.html">Google Play Terms</a>.
                                        Simple Tech and you hereby acknowledge that Google does not have any responsibility or liability related to compliance or non‐compliance by Simple Tech or you (or any other user) under these Terms or the Google Play Terms.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Indemniﬁcation</span></span>
                                        By entering into these Terms and using any portion of the Services, you agree that you shall defend, indemnify and hold Simple Tech, its licensors and their respective parent organizations, subsidiaries, aﬃliates, oﬃcers, directors, members, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys’ fees and costs) arising out of or in connection with: (a) your violation or breach of any term of these Terms or any applicable law or regulation, (b) your violation of any rights of any third party, (c) your use or misuse of the Services, or (d) your negligence or willful misconduct.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Your Data</span></span>
                                        When you use the Services, you understand and agree that Simple Tech may collect, use and disclose information about you as
                                        described in our <Link className="underline" to={staticPagesPath("privacy-policy")}>Privacy Policy</Link>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Third Party Services and Materials</span></span>
                                        Certain Services may display, include or make available content, data, information, applications, services or materials from third parties ("Third Party Services and Materials") or provide links to certain third party web sites and apps. By using the Services, you acknowledge and agree that Simple Tech is not responsible for examining or evaluating the content, accuracy, completeness, timeliness, availability, validity, copyright compliance, legality, decency, quality or any other aspect of such Third Party Services and Materials, web sites or apps. Simple Tech does not warrant or endorse and will not have any liability or responsibility to you or any other person for any Third Party Services and Materials. Third Party Services and Materials and links to other web sites are provided solely as a convenience to you.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>DISCLAIMER</span></span>
                                        <b>YOU EXPRESSLY AGREE THAT USE OF THE SERVICES IS AT YOUR SOLE RISK.  WE ARE NOT LIABLE FOR ANY HARM WHATSOEVER RESULTING FROM YOUR USE OF THE SERVICES INCLUDING USER CONTENT OR USER CONDUCT (FOR EXAMPLE, ILLEGAL CONDUCT OF ANY KIND OR SORT, INCLUDING CRIMINAL CONDUCT.)</b>
                                    </p>
                                    <p>THE SERVICES AND ANY CONTENT, INFORMATION, SERVICES, AND PRODUCTS PROVIDED THROUGH OR IN CONNECTION WITH THE SERVICES (INCLUDING, WITHOUT LIMITATION, ANY THIRD PARTY SERVICES AND MATERIALS) ARE PROVIDED TO YOU ON AN "AS IS"  AND "AS AVAILABLE" BASIS WITHOUT WARRANTY OF ANY KIND, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE OR NON‐INFRINGEMENT AND ALL WARRANTIES IMPLIED FROM ANY COURSE OF DEALING OR USAGE OF TRADE, ALL OF WHICH ARE HEREBY DISCLAIMED.  WE MAKE NO PROMISE AS TO THE SERVICES, THEIR COMPLETENESS, ACCURACY, AVAILABILITY, TIMELINESS, PROPRIETY, SECURITY OR RELIABILITY.</p>
                                    <p>NO ORAL OR WRITTEN INFORMATION OR ADVICE PROVIDED BY SIMPLETECH OR ITS AUTHORIZED AGENT OR REPRESENTATIVE WILL BE DEEMED TO CREATE A WARRANTY. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES SO SOME OR ALL OF THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU.</p>
                                    <p>SIMPLE TECH MAKES NO REPRESENTATION OR WARRANTY AS TO YOUR LEGAL RIGHT TO POSSESS MARIJUANA OR MARIJUANA PRODUCTS OR PICK‐UP OR HAVE MARIJUANA or marijuana products TRANSPORTED TO YOU OR YOUR INTENDED RECIPIENT.</p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>LIMITATION OF LIABILITY</span></span>
                                        TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL SIMPLE TECH BE LIABLE FOR DAMAGES OF ANY KIND, INCLUDING DIRECT, INDIRECT, SPECIAL, EXEMPLARY, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, LOSS OF USE, DATA OR PROFITS OR ANY OTHER DAMAGES OR LOSSES, ARISING OUT OF OR RELATED TO YOUR USE OR INABILITY TO USE THE SERVICES, OR FOR ANY OTHER CLAIM, DEMAND OR DAMAGES WHATSOEVER RESULTING FROM OR ARISING OUT OF OR IN CONNECTION WITH THESE TERMS, THE DELIVERY, USE OR PERFORMANCE OF THE SERVICES, OR THE PROCESSING, PURCHASE, SALE, TRANSPORTATION, SHIPMENT, DELIVERY OR CONSUMPTION OF MARIJUANA or marijuana products, INCLUDING ANY CLAIM, DEMAND OR DAMAGES ARISING FROM ANY TRANSACTION THROUGH THE SERVICES INITIATED OR COMPLETED BETWEEN YOU AND ANY RETAILER, HOWEVER CAUSED AND UNDER ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) EVEN IF SIMPLE TECH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM, DEMAND OR DAMAGE. YOUR SOLE AND EXCLUSIVE REMEDY HEREUNDER SHALL BE FOR YOU TO DISCONTINUE YOUR USE OF THE SERVICES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE EXCLUSION OR LIMITATION MAY NOT APPLY TO YOU. Notwithstanding the foregoing, if, for any reason, a court or arbitral body FInds Simple Tech liable for damages notwithstanding the foregoing, in no event shall Simple Tech’s total liability for all damages exceed the amount paid by you to Simple Tech for your use or receipt of the Services. The foregoing limitations will apply even if the above‐stated remedy fails of its essential purpose.
                                    </p>
                                    <p>
                                        NEITHER SIMPLE TECH NOR THE FULFILLING SHARER NOR THE SHIPPING OR DELIVERY COMPANY USED BY THE SHARER SHALL BE LIABLE FOR ANY DAMAGES TO GOODS CAUSED BY WEATHER CONDITIONS AND OTHER "ACTS OF GOD" DURING TRANSPORTATION. WEATHER CONDITIONS MAY CHANGE AT ANY TIME, SO SIMPLE TECH, THE FULFILLING SHARER OR THE SHIPPING OR DELIVERY COMPANY USED BY THE SHARER CANNOT ASSUME LIABILITY FOR DAMAGE CAUSED BY CHANGES IN WEATHER DURING TRANSPORTATION. "ACTS OF GOD" INCLUDE UNFORESEEABLE DELAYS OR ACCIDENTS, PUBLIC UNREST, CONFISCATION AND NATURAL DISASTERS.
                                    </p>
                                    <p>
                                        You agree that the above limitations of liability together with the other provisions in these Terms that limit liability are essential terms of these Terms and that Simple Tech would not be willing to grant you the rights set forth in these Terms but for your agreement to the above limitations of liability; you are agreeing to these limitations of liability to induce Simple Tech to grant you the rights set forth in these Terms.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Ownership</span></span>
                                    </p>
                                    <ol className='indent list-style-outside letters'>
                                        <li>
                                            <p>The Services and their content, including "look and feel" (e.g., text, graphics, images and logos), proprietary content, information and other materials, are protected under intellectual property, copyright, trademark and other laws. You acknowledge and agree that Simple Tech and/or its licensors own all right, title and interest in and to the Services and their content (including without limitation any and all patent, copyright, trade secret, trademark, show‐how, know‐how and any and all other intellectual property rights therein or related thereto) and you agree not to take any action(s) inconsistent with such ownership interests. Except for the limited license to the Services granted to you in Section 6 above, you do not acquire any rights or licenses under any of Simple Tech’s (or its licensors’) patents, patent applications, copyrights, trade secrets, trademarks or other intellectual property rights on account of these Terms.</p>
                                        </li>
                                        <li>
                                            <p>Any and all (i) suggestions for correction, change and modiﬁcation to the Services and other feedback, information and reports provided to Simple Tech by you (collectively "Feedback"), and all (ii) improvements, updates, modiﬁcations or enhancements thereto, whether made, created or developed by Simple Tech or otherwise relating to the Services (collectively, "Revisions"), are and will remain the property of Simple Tech. You acknowledge and expressly agree that any contribution of Feedback or Revisions does not and will not give or grant you any right, title or interest in the Services or in any such Feedback or Revisions. All Feedback and Revisions become the sole and exclusive property of Simple Tech and Simple Tech may use and disclose Feedback and/or Revisions in any manner and for any purpose whatsoever without further notice or compensation to you and without retention by you of any proprietary or other right or claim. You hereby assign to Simple Tech any and all right, title and interest that you may have in and to any and all Feedback and Revisions. At Simple Tech’s request, you will execute any document, registration or ﬁling required to give eﬀect to the foregoing assignment.</p>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Copyright Dispute Policy</span></span>
                                        Simple Tech has adopted and implemented a policy that provides for the termination in appropriate circumstances of users of the Website and App who are
                                        found repeatedly to provide or post protected third party content without necessary rights and permissions, including content on our social media properties.
                                        Please review our complete <Link className="underline" to={staticPagesPath("copyright")}>Copyright Dispute Policy</Link>  and learn how to report potentially infringing content.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Modiﬁcations</span></span>
                                        We may modify these Terms at any time. Modiﬁcations become eﬀective immediately   upon your ﬁrst access to or use of the Services after the "Last Revised" date at the end of these Terms. Your continued access or use of the Services after the modiﬁcations have become eﬀective will be deemed your conclusive acceptance of the modiﬁed Terms. If you do not agree with the modiﬁcations, then please uninstall the App and do not access or use the Services.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Termination</span></span>
                                        These Terms are eﬀective until you or Simple Tech terminates these Terms. Simple Tech may suspend or terminate your account(s) or cease providing you with all or any portion of the Services at any time for any reason, with or without notice to you. Upon termination, you will cease all use of the Services and will disable or destroy all copies (full or partial) of the App in your possession or control. Termination will not limit any of Simple Tech's other rights or remedies at law or in equity. Sections 6, 8, and 10‐26 shall survive termination or expiration of these Terms for any reason, provided however, that the license granted in Section 6 shall cease immediately upon termination or expiration.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Export Laws</span></span>
                                        You agree that you will not export or re‐export, directly or indirectly the App, or any portion of the Services and/or other information or materials provided by Simple Tech hereunder, outside of the United States or outside of states where the recreational use of marijuana or marijuana products is legal. In addition, by using the Services, you represent and warrant that you are not listed on any U.S. Government list of prohibited or restricted parties, including the U.S. Treasury Department's list of Specially Designated Nationals or the U.S. Department of Commerce Denied Person’s List or Entity List.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Injunctive Relief</span></span>
                                        You agree that a breach of these Terms will cause irreparable injury to Simple Tech for which monetary damages would not be an adequate remedy and Simple Tech shall be entitled to seek equitable relief, in addition to any remedies it may have hereunder or at law, without having to post a bond or other security or prove damages.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Dispute Resolution ‐ Arbitration, No className Actions</span></span>
                                        ANY DISPUTE OR CLAIM RELATING IN ANY WAY TO THESE TERMS OR THE SERVICES WILL BE RESOLVED BY BINDING ARBITRATION, RATHER THAN IN COURT,
                                        except that you may assert claims in small claims court if your claims qualify. The Federal Arbitration Act ("FAA") and federal arbitration law apply
                                        to these Terms. If you do not want to arbitrate disputes with Simple Tech and you are an individual, you may opt out of this arbitration
                                        agreement by sending an email to <a href="mailto:disputeresolution@sharemj.com">disputeresolution@sharemj.com</a> within 30 days of the day you ﬁrst access or use the Services.
                                    </p>
                                    <p>THERE IS NO JUDGE OR JURY IN ARBITRATION, AND COURT REVIEW OF AN ARBITRATION AWARD IS LIMITED. HOWEVER, AN ARBITRATOR CAN AWARD ON AN INDIVIDUAL BASIS THE SAME DAMAGES AND RELIEF AS A COURT (INCLUDING INJUNCTIVE AND DECLARATORY RELIEF OR STATUTORY DAMAGES), AND MUST FOLLOW THESE TERMS AS A COURT WOULD.</p>
                                    <p>
                                        If you intend to seek arbitration you must ﬁrst send written notice to Simple Tech’s Customer Service Center of your intent to arbitrate ("Notice").
                                        The Notice to Simple Tech should be sent by any of the following means: (i) electronic mail to <a href="mailto:disputeresolution@sharemj.com">disputeresolution@sharemj.com</a> or (ii) U.S. Postal Service
                                        certiﬁed mail to Simple Tech, LLC, Attn: Customer Service, 455 Massachusetts Ave. N.W., #157, Washington, D.C. 20001.
                                        The Notice must: (x) describe the nature and basis of the claim or dispute; (y) set forth the speciﬁc relief sought; and (z) set forth your name,
                                        address and contact information. If we intend to seek arbitration against you, we will send any notice of dispute to you at the contact
                                        information we have for you. If we do not reach an agreement to resolve the claim within 30 days after the Notice is received, you or Simple
                                        Tech may commence an arbitration proceeding.
                                    </p>
                                    <p>
                                        The arbitration will be conducted before a neutral single arbitrator, whose decision will be ﬁnal and binding, and the arbitral proceedings will
                                        be governed by the American Arbitration Association ("AAA") under its AAA Commercial Arbitration Rules, Consumer Due Process Protocol,
                                        and Supplementary Procedures for Resolution of Consumer Related Disputes, as modiﬁed by these Terms. The AAA's rules are available
                                        at <a target="_blank" className="underline" href="https://www.adr.org/aaa/faces/home">ADR.org</a> or by calling 1‐800‐778‐7879. All issues are for the arbitrator to decide, including the scope of this arbitration clause,
                                        but the arbitrator is bound by these Terms. If you initiate arbitration, your arbitration fees will be limited to the ﬁling fee set forth in the AAA’s Consumer Arbitration Rules. We will reimburse all other AAA ﬁling, administration and arbitrator fees paid by you for claims totaling less than $10,000, unless the arbitrator determines that the arbitration was frivolous or brought for an improper purpose, in which case the payment of all such fees shall be governed by the AAA Rules. In such case, you agree to reimburse Simple Tech for all monies previously disbursed by it that are otherwise your obligation to pay under the AAA Rules. Simple Tech will not seek attorneys' fees and costs in arbitration unless the arbitrator determines the claim or the relief sought is improper or not warranted. The arbitration shall be conducted, at the option of the party seeking relief, by telephone, online, based solely on written submissions or in person in the Commonwealth of Massachusetts or at a mutually agreed location. The arbitration will be conducted in the English language. Judgment on the award rendered by the arbitrator may be entered in any court of competent jurisdiction. No claim or action arising from or concerning the Services or otherwise hereunder may be brought later than one (1) year from the date the claim or cause of action arose. The arbitrator, and not any federal, state, or local court, shall have exclusive authority to resolve any dispute relating to the interpretation, applicability, unconscionability, arbitrability, enforceability, or formation of this arbitration agreement, including any claim that all or any part of this arbitration agreement is void or voidable.
                                    </p>
                                    <p>
                                        WE EACH AGREE THAT TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, ANY DISPUTE RESOLUTION PROCEEDINGS WILL BE CONDUCTED ONLY ON AN INDIVIDUAL BASIS AND NOT IN A className, CONSOLIDATED OR REPRESENTATIVE ACTION. Further, you agree that the arbitrator may not consolidate proceedings or more than one person's claims, and may not otherwise preside over any form of a representative or className proceeding, and that if this speciﬁc proviso is found to be unenforceable, then the entirety of this arbitration clause shall be null and void. If for any reason a claim proceeds in court rather than in arbitration, WE EACH WAIVE ANY RIGHT TO A JURY TRIAL. If a court of competent jurisdiction ﬁnds the foregoing arbitration provisions invalid or inapplicable, you and we each agree to the exclusive jurisdiction and the exercise of personal jurisdiction of the state or federal court located in the District of Columbia for the purpose of litigating all claims or disputes, and waive any objection as to inconvenient forum. We also both agree that Simple Tech may bring suit in court for injunctive relief to enjoin infringement or other misuse of intellectual property rights.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <span className='dot'><span className='underline'>Miscellaneous</span></span>
                                        These Terms  may  not  be  modiﬁed  by  you  except  by  a  writing  executed  by  the  duly‐authorized representatives of Simple Tech. These Terms will inure to the beneﬁt of and will be binding upon each party’s successors and assigns. These Terms and the rights granted under these Terms may be assigned by Simple Tech but may not be assigned by you without the prior express written consent of Simple Tech. If any provision of these Terms is or becomes, at any time or for any reason, unenforceable or invalid, no other provision of these Terms will be aﬀected and the remaining provisions will continue to be enforceable and valid according to the terms of such provisions. It is expressly understood that in the event either party fails to perform any term of these Terms and the other party does not enforce that term, the failure to enforce not constitute a waiver of any term. Nothing contained in these Terms will be deemed to constitute either party as the agent or representative of the other party or both parties as joint venturers or partners for any purpose. No joint venture, partnership, employment, or agency relationship exists between Simple Tech and you or any third party as a result of these Terms or your use of the Services. In the event that either party is prevented from performing, or is unable to perform, any of its obligations under these Terms due to any cause beyond the reasonable control of the party invoking this provision, the aﬀected party's performance will be extended for the period of delay or inability to perform due to such occurrence. You and Simple Tech agree that the United Nations Convention on Contracts for the International Sale of Goods will not apply to the interpretation or construction of these Terms. The headings and captions contained in these Terms will not be considered to be part of these Terms but are for convenience only.
                                    </p>
                                    <p>
                                        You may contact us regarding the Services or these Terms at: Simple Tech, LLC 455 Massachusetts Ave. N.W., #157, Washington, D.C. 20001,
                                        or by email to: <a href="mailto:info@sharemj.com">info@sharemj.com</a>.
                                    </p>
                                    <p>
                                        These Terms and the <Link className="underline" to={staticPagesPath("privacy-policy")}>Privacy Policy</Link> set forth the entire understanding of the parties
                                        with respect to the matters contained in these Terms and the <Link className="underline" to={staticPagesPath("privacy-policy")}>Privacy Policy</Link> and there are
                                        no promises, covenants or undertakings other than those expressly set forth in these Terms and the <Link className="underline" to={staticPagesPath("privacy-policy")}>Privacy Policy</Link>.
                                    </p>
                                    <p>Last Revised:  February 7, 2017</p>
                                </li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default TermsOfService;
