import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";
import {Navbar, Header, Brand, Toggle, Collapse, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';
import { staticPagesPath } from '../../../core/paths';

const PrivacyPolicy = () => {
    return (
        <div id="main-container" className="static-page">

            <Helmet>
                <meta charSet="utf-8" />
                <title>ShareMJ</title>
                <link rel="canonical" href="http://mysite.com/example" />
            </Helmet>

            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a className="navbar-brand" id="logo" title="ShareMJ" href="/" style={{visibility: 'visible'}}>
                            <img height={25} className="header-mj-logo" alt="ShareMJ Logo" src="https://d3ely0vl8t3ihb.cloudfront.net/assets/shareMJ-white-logo-041e5131807629716e67996933c2f078578aef75fa4e7770084d179b1bf4ec4e.png" />
                        </a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
            </Navbar>

            <div className='container'>

                <div className='row'>
                    <div className='col-xs-12'>
                        <div className='static'>
                            <h1 className='text-center'>PRIVACY POLICY</h1>
                            <h2 className='tagline'>EFFECTIVE FEBRUARY 7, 2017</h2>
                            <p>
                                This privacy policy (“Privacy Policy”) describes how Simple Technologies, LLC. (“Simple Tech,” “we” or “us”) collects, uses,
                                and discloses your information in connection with providing you with access to and use of our website, and any subdomains of our website,
                                <Link to={staticPagesPath("privacy-policy")}>https://www.sharemj.com</Link>, (“Website”) and our mobile application named “ShareMJ” (“App”).
                                The Terms of Service governing the use of our Website and App (“Services”) are found <Link className="underline" to={staticPagesPath("terms-of-service")}>here</Link>.
                            </p>
                            <p>
                                Your access or use of the Website, App or Services means that you agree to our collection, use, and disclosure of the information you share with us,
                                as explained in this Privacy Policy.  Please read this Privacy Policy carefully before you access or use the Website, App or Service so that you
                                will understand our practices and your options before you share your information.
                            </p>
                            <p>
                                We may update this Privacy Policy to reflect changes to our information practices. If we make any significant changes,
                                we will notify you by email (sent to the e-mail address specified in your account) or by means of a notice on the Website
                                and in the App prior to the change becoming effective. We encourage you to periodically review this page for the latest
                                information on our information practices.
                            </p>
                            <p>
                                If you have questions about this Privacy Policy, please contact us at <a href="mailto:info@sharemj.com">info@sharemj.com</a>.
                            </p>
                            <p>
                                The Website, App and Service are intended for access or use by individuals who are 21 years of age and older.
                                Persons under the age of 21 are not allowed to access or use the Website, App or Service. We do not knowingly collect Personal Information (as defined below)
                                from or about anyone under the age of 21. If you are under the age of 21, you should not attempt to access or use the Website or App or send any Personal
                                Information about yourself to us. If we discover that we have collected Personal Information from or about somebody under the age of 21, we will
                                delete that information as soon as possible. If you believe that somebody under the age of 21 may have provided us Personal Information or that we may
                                have received Personal Information about somebody under the age of 21, please contact us as soon as possible at <a href="mailto:info@sharemj.com">info@sharemj.com</a>.
                                Please refer to our <Link className="underline" to={staticPagesPath("terms-of-service")}>Terms of Service</Link> for additional information about age verification and eligibility.
                            </p>
                            <h3>Information We Collect</h3>
                            <p>
                                Simple Tech collects information from and about you and your devices that: (i) you actively provide, such as when you register for an account with Simple Tech to use the Service, (ii) we collect from the devices you use to access our Website or App, and (iii) we may acquire from third parties.  We collect both your Personal Information and Non-Personal Information. In this Privacy Policy, “Personal Information” means data that can be used to identify or contact a specific natural person.  Examples of Personal Information include your name, street address, email address and/or telephone number. “Non-Personal Information” means data in a form that does not, on its own, permit direct association with any specific individual. Examples of Non-Personal Information include your IP address and browser type.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Information You Provide</span></span>
                                We collect and store information, which may include Personal Information, that you actively submit to us, such as your name, email address, account password, date of birth, billing information, phone number, delivery address and social media IDs.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Information Automatically Collected through the Website</span></span>
                            </p>
                            <p>
                                <i>Tracking Information and Cookies.</i>
                                Tracking information is automatically collected and stored as you navigate through the Website, including but not limited to, browser type, page views, IP address, referring/exit pages, information about how you interact with the Website’s pages and third party links, and traffic and usage trends on the Website. This tracking information may be collected through the use of cookies.  A cookie is a small amount of data that is sent to your browser from a web server and stored on your browser. A website may use cookies to recognize repeat users or track web usage behavior. Cookies take up minimal room on your device and cannot damage your device’s files. Cookies work by assigning a number to the user that has no meaning outside of the assigning website.
                            </p>
                            <p>
                                Your browser allows you to deny or accept the use of cookies; however, at this time, our Website is not configured to read or respond to
                                “do not track” settings or signals in your browser headings. Also, there may be some features of our Website that require the use of cookies.
                                You should be aware that Simple Tech cannot control the use of cookies (or the resulting information) by third party sites to which you link
                                from the Website or App. The use of cookies or your information by third parties is not covered by our Privacy Policy.
                                We do not have access or control over these cookies. To learn more about cookies, please visit the following third
                                party site: <a target="_blank" className="underline" href="http://www.allaboutcookies.org/">All About Cookies</a>.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Information Automatically Collected through the App</span></span>
                            </p>
                            <p>
                                <i>Geolocation Data.</i>
                                If you use the App through your smart phone or other mobile device, we will automatically collect your geo-location information.  You may at any time opt-out of providing geo-location data through the settings on your device or the App, but if you do so, some features of our App and Service may be limited.
                            </p>
                            <p>
                                <i>Device Type.</i>
                                When you use the App through your smart phone or other mobile device, we automatically collect the device type and advertising identifier, a unique identifier generated within the device, and we may store this information.
                            </p>
                            <p>
                                <i>Tracking Information.</i>
                                Tracking information is automatically collected and stored as you navigate through the App, including but not limited to, page views, referring/exit pages, information about how you interact with the App’s pages and third party links, and traffic and usage trends on the App.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Information Collected from Third Parties</span></span>
                                We may obtain information about you, including Personal Information, from third parties claiming a right to share your information with us, including through third-party advertising arrangements.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Information from Public Sources</span></span>
                                We collect information that is publicly available. For example, information you submit in a public forum (e.g., a blog, chat room, or social network) may be read, collected or used by us and others. You are responsible for the information you choose to submit in any such forum.
                            </p>
                            <h3>How We Use Your Information</h3>
                            <p>We may use the information that you provide to us, that we collect from your device, and information we obtain from third parties and public sources:</p>
                            <ul className='indent'>
                                <li>to provide the Website, App and Service;</li>
                                <li>to determine where we should offer or focus services, features and/or resources;</li>
                                <li>to determine demand for the Service in a particular geographic market and to notify you when the Service becomes available in your geographic location, unless you opt out from receiving such notice;</li>
                                <li>to analyze our performance and evaluate strategies to support and enhance your access and use of the Website, App and Service, in part by monitoring which features of the Website, App and Service you and other users access or use most, and to attribute the sources of our traffic;</li>
                                <li>to recognize your device over time so we can customize the Website and App to your interests and tastes based on your prior activities;</li>
                                <li>to obtain additional information about you from other third parties claiming a right to share your data with us so we can associate that information with information that we have collected so we may further customize your user experience;</li>
                                <li>to notify you about new services or special promotional programs, or send you offers or information, and to measure the responsiveness to such communications and to our other advertising efforts;</li>
                                <li>to engage with you through social media, including through direct messaging;</li>
                                <li>to serve you the correct application version depending on your device type;</li>
                                <li>to diagnose and resolve problems with our Website, App or servers and for other technical and administrative troubleshooting purposes;</li>
                                <li>to send you service-related announcements relating to the Website, App or Service; for instance, if our Website or App or the Service is temporarily suspended for maintenance, we might send you an email notifying you; generally, you may not opt-out of these communications, which are not promotional in nature; if you do not wish to receive them, you have the option to deactivate your account;</li>
                                <li>to provide customer service, such as to send you a welcoming email to the address you provide at registration to verify your username and password, communicate with you in response to your inquiries, provide the services you request, and manage your account; and</li>
                                <li>to investigate and address fraudulent transactions and generally implement fraud prevention measures.</li>
                            </ul>
                            <p>
                                We use information obtained from third parties claiming a right to disclose your information to us in accordance with our agreements with them and in accordance with this Privacy Policy.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Aggregated Information; Non-Personal Information</span></span>
                                We may aggregate your Personal and Non-Personal Information with other users’ information and gather broad demographic data about our users for many purposes, including so that we can improve the Website, App and Service, and for business and administrative purposes.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Advertising</span></span>
                                Targeted or behavioral advertising involves using information we collect about you, as described in this Privacy Policy, to determine which advertisement should be displayed to you via the Website or App.  We also connect such information in order to link your activity on our Website to your activity on our App, allowing us to provide you with a personalized experience regardless of how you interact with us, online or through the App.  If you do not wish to have us use your information for targeted advertising, you should cease using the Website, App and Service.
                            </p>
                            <h3>Our Disclosure of Your Information</h3>
                            <p>We share information collected from or about you only as follows:</p>
                            <p>
                                <span className='dot'><span className='underline'>Aggregated and Anonymized Data</span></span>
                                Simple Tech may share with or sell to third parties aggregated demographic information and Non-Personal Information about our user base, including information on how users interact with our App, Website, partners and advertisers for industry analysis, demographic profiling and to deliver targeted advertising about other products and services.  We will only do so after either removing from such information anything that personally identifies you or combining it with other information so that it no longer personally identifies you.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Third-Party Advertising; Interest-Based and Cross-App Advertising</span></span>
                                We may participate in third-party advertising networks.  These networks collect certain information (e.g., click stream information, browser type, time and date, subject of advertisements clicked or scrolled over, hardware/software information, cookie and session ID) when you access and interact with websites and apps of companies within their networks.  They use this information in combination with other information that they may otherwise already have about you to serve you ads for products and services of companies that are in the same advertising network and that they predict will be relevant to your interests based on your activities across various websites and apps.  For example, if you previously browsed a particular product, you may see an ad for that very same product on another site at a later time.  This type of advertising is called re-targeting and is a form of interest-based or cross-app advertising.  Third-party advertising networks may also be able to use a combination of information to identify you personally. Furthermore, the companies participating in a given network may also be able to obtain similar information and associate the information with your identity based on information they may already have about you. The networks and participating companies may use cookies and similar technologies such as pixels, tags, beacons and other technologies to collect information about you.
                            </p>
                            <p>
                                We also may allow other companies that advertise on our Website and App to use cookies and similar technologies such as pixels, tags, beacons and other technologies to collect information about you, provided it is not Personal Information.  Similarly, we may receive information about you as a result of ads we place on other websites and apps through cookies and similar technologies such as pixels, tags, beacons and other technologies.
                            </p>
                            <p>
                                When we collect information about you through third-party advertising networks or other advertising, we use the information for the purposes described in this Privacy Policy.  The information practices of third-party advertising networks and their participating companies and the information practices of our other advertisers are not covered by this Privacy Policy.
                            </p>
                            <p>
                                A “do not track” feature lets you tell websites that you do not want to have your online activities tracked.
                                At this time, we are not configured to respond to browser “do not track” signals.
                                For more information about online advertising, including information on setting preferences for or opting out of third-party advertising networks,
                                please visit
                                <a className="underline" target="_blank" href="http://www.networkadvertising.org/">Network Advertising Initiative</a>,
                                <a className="underline" target="_blank" href="http://www.aboutads.info/">Digital Advertising Alliance</a> and
                                <a className="underline" target="_blank" href="https://www.google.com/settings/u/0/ads/authenticated">Google Ad Network</a>.
                                If you choose to opt-out, you may still be served ads, but the ads may not be relevant to your interests.
                                Furthermore, some of the features of the Website and App may not be fully functional for you.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Third-Party Analytics</span></span>
                                We may use third-party analytics services, such as Google Analytics, to evaluate the performance of various features of our Website and App and also
                                to evaluate the overall performance of our Website and App.
                                We may also use such services to understand whether and how specific users or users meeting a certain profile are taking advantage of certain features
                                of our Website and App and/or making use of our Website and App generally.
                                For more information about web analytics, including information on setting preferences for or opting out of certain data collection,
                                please visit <a className="underline" target="_blank" href="https://support.google.com/analytics/answer/181881?hl=en">Google Analytics</a>.  If you choose to opt-out, some of the features of the Website and App may not be fully functional for you.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Third-Party Service Providers</span></span>
                                We may employ third party companies and individuals to facilitate our Website, App and Service, to provide the Website, App and Service on our behalf, provide customer support, perform Website- and App-related services (e.g., without limitation, maintenance services, hosting, database management and improvement of the Website's and App’s features) or to assist us in analyzing how our Website, App and Service are used. These third parties are authorized to access and use your Personal Information only to perform these tasks on our behalf.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Law Enforcement</span></span>
                                We will disclose information about you to government or law enforcement officials or private parties as we, in our sole discretion, believe necessary or appropriate to respond to claims and legal process (including but not limited to subpoenas), to protect the property and rights of Simple Tech or a third party, to protect the safety of the public or any person, or to prevent or stop activity we may consider to be, or to pose a risk of being, an illegal, unethical or legally actionable activity.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Acquisition of Simple Tech</span></span>
                                In the event that all or a substantial portion of the assets, business or stock of Simple Tech are acquired by, merged with or transferred to another party, or in the event that Simple Tech goes out of business or enters bankruptcy, your Personal Information would be one of the assets that is transferred to or acquired by the third party. You acknowledge that such transfers may occur, and that any acquirer of Simple Tech or its assets may continue to use your Personal Information as set forth in this Privacy Policy. If any acquirer of Simple Tech or its assets will use your Personal Information contrary to this Privacy Policy, you will receive prior notice.
                            </p>
                            <h3>Other Important Notices Regarding Our Privacy Practices</h3>
                            <p>
                                <span className='dot'><span className='underline'>Updating Your Personal Information and Preferences; Opting Out</span></span>
                                ou may update your Personal Information and preferences at any time by making the change on our user information page. If you have received an email invitation to use our Website, App or Service but do not wish to receive marketing or other promotional messages or additional invitations in the future, you may opt out by following the instructions in the email invitation. We will take commercially reasonable steps to implement your opt-out requests promptly, but you may still receive communications from us for up to 30 days as we process your request.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Retention of Personal Information</span></span>
                                Simple Tech will retain your Personal Information for the period necessary for the purposes outlined in this Privacy Policy unless a longer retention period is required or permitted by law.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Invitations to Simple Tech</span></span>
                                If you choose to invite an individual to use the Service, you agree that you shall not invite anyone less than 21 years of age.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Security</span></span>
                                All information we collect is securely stored within our database, and we use standard, industry-wide, commercially reasonable security practices
                                such as 256-bit encryption, firewalls and SSL (Secure Socket Layers). However, as effective as encryption technology is, no security system is impenetrable.
                                We cannot guarantee the security of our database, nor can we guarantee that information you supply won't be intercepted while being transmitted to
                                us over the Internet, and any information you transmit to Simple Tech you do so at your own risk. We recommend that you use unique numbers,
                                letters and special characters in your password and not disclose your password to anyone. If you do share your password or personal information with others,
                                you are responsible for all actions taken in the name of your account.
                                Please review our <Link className="underline" to={staticPagesPath("terms-of-service")}>Terms of Service</Link>. If your password has been compromised for any reason,
                                you should immediately notify Simple Tech at <a href="mailto:info@sharemj.com">info@sharemj.com</a> and change your password.
                            </p>
                            <p>
                                If you provide us with Personal Information, you consent to the transfer and storage of that information on our servers located in the United States.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>California Privacy Rights</span></span>
                                If you are a California resident, you have the right to request certain information regarding our disclosure of Personal Information to third parties
                                for their direct marketing purposes. To make such a request, please contact us at <a href="mailto:info@sharemj.com">info@sharemj.com</a>.
                            </p>
                            <p>
                                <span className='dot'><span className='underline'>Contact</span></span>
                                Contact us by email at <a href="mailto:info@sharemj.com">info@sharemj.com</a>, or by postal mail at: Simple Tech, LLC, 455 Massachusetts Ave. N.W., #157, Washington, D.C. 20001.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
    );
};

export default PrivacyPolicy;
