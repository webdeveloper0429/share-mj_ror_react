import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";
import {Navbar, Header, Brand, Toggle, Collapse, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';
import { staticPagesPath } from '../../../core/paths';

const Copyright = () => {
    return (
        <div id="main-container" className="static-page">

            <Helmet>
                <meta charSet="utf-8" />
                <title>ShareMJ</title>
                <link rel="canonical" href="http://mysite.com/example" />
            </Helmet>

            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a className="navbar-brand" id="logo" title="ShareMJ" href="/" style={{visibility: 'visible'}}>
                            <img height={25} className="header-mj-logo" alt="ShareMJ Logo" src="https://d3ely0vl8t3ihb.cloudfront.net/assets/shareMJ-white-logo-041e5131807629716e67996933c2f078578aef75fa4e7770084d179b1bf4ec4e.png" />
                        </a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
            </Navbar>

            <div className='container'>

                <div className='row'>
                    <div className='col-xs-12'>
                        <div className='static'>
                            <h1>COPYRIGHT DISPUTE POLICY</h1>
                            <p>
                                Pursuant to 17 U.S.C. 512(i) of the United States Copyright Act (“DMCA”), Simple Technologies LLC. (“Simple Tech”) has adopted and implemented this
                                copyright dispute policy (“Copyright Dispute Policy”) that provides for the termination in appropriate
                                circumstances of users of Simple Tech’s website (<Link to={staticPagesPath("copyright")}>https://www.sharemj.com</Link>) and any subdomains of the website
                                (the “Website”), or the “ShareMJ” mobile application (the “App”), who are repeat infringers.
                                Simple Tech may terminate access for users who are found repeatedly to provide or post protected third party content without necessary
                                rights and permissions, including content on our social media properties.
                                To learn more about the DMCA, <a target="_blank" className="underline" href="https://www.google.com/url?q=http://www.copyright.gov/legislation/dmca.pdf&amp;sa=D&amp;ust=1469160445419000&amp;usg=AFQjCNFd1VkhXBkyCtH3G_r5xtRJP8Rb6w">click here: DMCA</a>.
                            </p>
                            <p>
                                <i>DMCA Take-Down Notices. </i>
                                If you are a copyright owner or an agent thereof and believe, in good faith, that any materials on the Website or App infringe upon your copyrights, you may submit a notification pursuant to the DMCA by sending the following information in writing to Simple Tech’s designated copyright agent at Simple Tech, Inc., Attn: Copyright, Simple Tech, LLC 455 Massachusetts Ave. N.W., #157, Washington, D.C.  20001 or info@sharemj.com:
                            </p>
                            <ol className='indent'>
                                <li>The date of your notification;</li>
                                <li>A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;</li>
                                <li>A description of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online website are covered by a single notification, a representative list of such works at that site;</li>
                                <li>A description of the material that is claimed to be infringing or to be the subject of infringing activity and information sufficient to enable us to locate such work;</li>
                                <li>Information reasonably sufficient to permit the service provider to contact you, such as an address, telephone number, and/or email address;</li>
                                <li>A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law;</li>
                                <li>A statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
                            </ol>
                            <p>
                                <i>Counter-Notices. </i>
                                If you believe that your content that has been removed from the Website or App is not infringing, or that you have the authorization from the copyright owner, the copyright owner’s agent, or pursuant to the law, to post and use the content on the Website or App, you may send a counter-notice containing the following information to our copyright agent using the contact information set forth above:
                            </p>
                            <ol className='indent'>
                                <li>Your physical or electronic signature;</li>
                                <li>A description of the content that has been removed and the location at which the content appeared before it was removed;</li>
                                <li>A statement that you have a good faith belief that the content was removed as a result of mistake or a misidentification of the content; and</li>
                                <li>Your name, address, telephone number, and email address, a statement that you consent to the jurisdiction of the federal court for the judicial district in which your permanent address is, or, if your permanent address is outside the United States, for any judicial district in which Simple Tech is located and a statement that you will accept service of process from the person who provided notification of the alleged infringement.</li>
                            </ol>
                            <p>
                                If a counter-notice is received by the Simple Tech copyright agent, Simple Tech may send a copy of the counter-notice to the original complaining party informing such person that it may reinstate the removed content in 10 business days.  Unless the copyright owner files an action seeking a court order against the content provider, member or user, the removed content may (in Simple Tech’s discretion) be reinstated on the Website and App in 10 to 14 business days or more after receipt of the counter-notice.
                            </p>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    );
};

export default Copyright;
