import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { staticPagesPath } from '../../../core/paths';

const Footer = () => {
    return (
        <footer>
            <div className='container footer'>
                <ul>
                    <li>
                        <Link to={staticPagesPath("terms-of-service")}>Terms of Service</Link>
                        <Link to={staticPagesPath("privacy-policy")}>Privacy Policy</Link>
                        <Link to={staticPagesPath("copyright")}>&copy;Copyright</Link>
                    </li>
                </ul>
            </div>
        </footer>
    );
};

export default Footer;