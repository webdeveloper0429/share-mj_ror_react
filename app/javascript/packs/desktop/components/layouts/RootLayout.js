import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

class RootLayout extends Component {
  render() {
    return (
      <div id="container">
        {this.props.children}
      </div>
    );
  }
}

RootLayout.contextTypes = {
  router: PropTypes.object.isRequired
}

export default connect(null, {})(RootLayout)
