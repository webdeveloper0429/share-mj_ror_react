export default {
 goNextPageAutomatic: true,
 pages: [
  {
   elements: [
    {
     type: "radiogroup",
     choices: [
      {
       value: "mj",
       text: "I want MJ"
      },
      {
       value: "edibles",
       text: "I want Edibles"
      },
      {
        value: "dont_know",
        text: "I don’t know yet. I just want to sign up for now."
      }
     ],
     isRequired: true,
     name: "help",
     title: "How can we help you?"
    }
   ],
   name: "help"
  },
  {
   elements: [
    {
     type: "radiogroup",
     choices: [
      {
       value: "dont_know",
       text: "I don’t know"
      },
      {
       value: "sativa",
       text: "Sativa"
      },
      {
       value: "hybrid",
       text: "Hybrid"
      },
      {
       value: "indica",
       text: "Indica"
      }
     ],
     isRequired: true,
     name: "request_type",
     title: "Which type do you prefer?"
    }
   ],
   name: "type",
   visible: false
  },
  {
   elements: [
    {
     type: "radiogroup",
     choices: [
      {
       value: "1/8",
       text: "1/8 Oz (3.5 grams)"
      },
      {
       value: "1/4",
       text: "1/4 Oz (7 grams)"
      },
      {
       value: "1/2",
       text: "1/2 Oz (14 grams)"
      },
      {
       value: "1",
       text: "1 Oz (28 grams)"
      },
      {
       value: "2",
       text: "2 Oz (56 gram)"
      },
      {
       value: "dont_know",
       text: "I don’t know"
      }
     ],
     isRequired: true,
     name: "weight",
     title: "How much would you like?"
    }
   ],
   name: "weight",
   visible: false
  },
  {
   elements: [
    {
     type: "text",
     isRequired: true,
     name: "nickname",
     placeHolder: "Please enter your nickname",
     title: "Create username"
    }
   ],
   name: "nickname"
  },
  {
    elements: [
      {
        type: "panel",
        elements: [
          {
          type: "html",
          html: "Please click <a href='/privacy-policy' target='_blank'>here</a> to read our Terms & Conditions.",
          name: "question1"
          },
          {
          type: "checkbox",
          choices: [
            {
            value: "1",
            text: "I confrim!"
            }
          ],
          isRequired: true,
          name: "terms_conds_confirmation",
          title: "Please confirm that you agree with our Terms & Conditions"
          }
        ],
        name: "terms",
        title: " "
      },
      {
        type: "checkbox",
        choices: [
          {
          value: "1",
          text: "I confrim!"
          }
        ],
        isRequired: true,
        name: "age_confirmation",
        title: "Please confirm that you are at least 21 years old"
      }
    ],
    name: "terms"
  },
  {
   elements: [
    {
     type: "text",
     inputType: "email",
     isRequired: true,
     name: "email",
     placeHolder: "Please enter your email",
     title: "Email",
     validators: [
      {
       type: "email"
      }
     ]
    },
    {
     type: "text",
     inputType: "password",
     isRequired: true,
     name: "password",
     placeHolder: "Please enter your password",
     title: "Password",
     validators: [
      {
       type: "text",
       text: "Text length should be minimum 3 symbols",
       minLength: "3",
       maxLength: "0"
      }
     ]
    },
    {
     type: "text",
     inputType: "tel",
     name: "phone",
     placeHolder: "Please enter your phone number",
     title: "Phone"
    }
   ],
   name: "sign_up"
  }
 ],
 showCompletedPage: false,
 showPageTitles: false,
 showProgressBar: "bottom",
 showQuestionNumbers: "off",
 triggers: [
  {
   type: "visible",
   operator: "notequal",
   value: "dont_know",
   name: "help",
   pages: [
    "type",
    "weight"
   ]
  }
 ]
}
