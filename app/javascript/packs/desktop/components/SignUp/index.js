import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Survey } from 'survey-react';
import { Modal, Button, ButtonToolbar } from 'react-bootstrap';
import surveyJson from './surveyJson';
import { withRouter } from 'react-router';

import { get } from 'axios';
import { signUp } from '../../../core/actions/authActions';
import { createRequest } from '../../../core/actions/requests';

Survey.cssType = "bootstrap";

const css = {
  navigation: {
    complete: "survey-button survey-nav-complete-btn btn btn-success btn",
		prev: "survey-button survey-nav-prev-btn ",
		next: "survey-button survey-nav-next-btn btn btn-success"
  }
}

const styles = {
  buttonStyle: {
    float: "right",
    marginTop: "30px",
    minWidth: "80px"
  },
  successBtn: {
    marginTop: "30px",
    minWidth: "80px",
    float:"left"
  },
  spanStyle: {
    fontSize: "16px"
  }
}

class SignUp extends Component {
  constructor(props) {
    super(props);

    const urlSearchParams = new URLSearchParams(this.props.location.search);

    this.state = {
      showModal: true,
      signedUp: false,
      position: null,
      ip: null,
      isAgeQuestion: true,
      isAcceptaibleAge: false,
      referral_token: urlSearchParams.get("referral-token")
    };

    this.close = this.close.bind(this);
    this.onSurveyComplete = this.onSurveyComplete.bind(this);
    this.surveyValidateQuestion = this.surveyValidateQuestion.bind(this);
    this.handlePosition = this.handlePosition.bind(this);
    this.onAfterRenderPage = this.onAfterRenderPage.bind(this);
    this.ageQuestionYes = this.ageQuestionYes.bind(this);
    this.ageQuestionNo = this.ageQuestionNo.bind(this);
    this.onAfterRenderQuestion = this.onAfterRenderQuestion.bind(this);
  }
  
  componentDidMount() {
    get('//ipapi.co/json/').then(res => {
      this.setState({ ip: res.data.ip });
    });

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.handlePosition);
    }
  }

  onAfterRenderPage() {
    setTimeout(function() {
      const completeButton = document.querySelector(".survey-nav-complete-btn");
      completeButton ? completeButton.value = "Sign up" : '';
    }, 100);
  }

  handlePosition(position) {
    this.setState({ position });
  }

  close() {
    this.setState({ showModal: false });
    let { signedUp } = this.state;
    setTimeout(() => {
      this.props.onClose(signedUp);
    }, 1000);
  }

  onSurveyComplete(s) {
    const { ip, position } = this.state;
    const { zipCode } = this.props;
    const { request_type, weight, help } = s.valuesHash;
    let payload = { request_type, weight, help, zip: zipCode };
    if (position) {
      payload.latitude = position.coords.latitude;
      payload.longitude = position.coords.longitude;
    } else {
      payload.address = ip;
    }

    if(request_type && weight) {
      this.props.createRequest(payload).then(res => {
        this.close(null, true);
      });
    }
    this.close(null, true);
  }

  surveyValidateQuestion(survey, options) {
    let { email, password, nickname, phone } = options.data;
    const zip_code = this.props.zipCode;
    const { referral_token } = this.state;
    if (email == null || password == null) {
      options.complete();
    } else {
      this.props.signUp({ email, password, nickname, phone, zip_code, referral_token }).then(
        (res) => {
          this.setState({ ...this.state, signedUp: true });
          options.complete()
        }
      ).catch(function (error) {
        if (error.response) {
          let errors = error.response.data.result;
          if (errors.email) {
            options.errors['email'] = errors.email.join(' ');
          }
          if (errors.password) {
            options.errors['password'] = errors.password.join(' ');
          }
          if (errors.phone) {
            options.errors['phone'] = errors.phone.join(' ');
          }
        }
        options.complete();
      });
    }
  }

  ageQuestionYes() {
    this.setState({
      ...this.state,
      isAcceptaibleAge: true,
      isAgeQuestion: false
    })
  }

  ageQuestionNo() {
     this.setState({
      ...this.state,
      isAcceptaibleAge: false,
      isAgeQuestion: false
    })
  }

  onAfterRenderQuestion (survey) {
    if(survey.currentPageValue.name == "sign_up"){
      document.querySelector(".modal-title").textContent = "Almost done!";
      document.querySelector(".survey-nav-complete-btn").value = "Join now";
    }
  }

  render() {
    let { showModal, isAcceptaibleAge, isAgeQuestion} = this.state;
    return (
      <div>
        <Modal show={showModal} onHide={this.close} backdrop={false}>
          <Modal.Header closeButton>
            <Modal.Title>Sign up</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {isAgeQuestion ? <AgeQuestion ageQuestionYes={this.ageQuestionYes} ageQuestionNo={this.ageQuestionNo}/> : ""}
            {isAcceptaibleAge && !isAgeQuestion
              ?<Survey json={surveyJson} css={css} onServerValidateQuestions={this.surveyValidateQuestion} onAfterRenderPage={this.onAfterRenderPage} onComplete={this.onSurveyComplete} onAfterRenderQuestion={this.onAfterRenderQuestion}/>
              : ""
            }

            {!isAcceptaibleAge && !isAgeQuestion ? <span style={styles.spanStyle}>You must be at least 21 years old</span> : ""}
          </Modal.Body>
        </Modal>
      </div>
    )
  }
}

const AgeQuestion = ({ageQuestionYes, ageQuestionNo}) => {
  return(
    <div>
      <span style={styles.spanStyle}>Are you at least 21 year old?</span>
      <ButtonToolbar>
        <Button bsStyle="success" onClick={ageQuestionYes} style={styles.successBtn}>Yes</Button>
        <Button bsStyle="danger" onClick={ageQuestionNo} style={styles.buttonStyle}>No</Button>
      </ButtonToolbar>
    </div>
  )
}

SignUp.propTypes = {
  signUp: PropTypes.func.isRequired,
  createRequest: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  zipCode: PropTypes.string.isRequired,
  location: PropTypes.object.isRequired
}

function mapDispatchToProps(dispatch) {
  return {
    signUp: (data) => dispatch(signUp(data)),
    createRequest: (data) => dispatch(createRequest(data))
  };
}

export default withRouter(connect(null, mapDispatchToProps)(SignUp))
