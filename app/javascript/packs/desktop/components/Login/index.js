import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import { Survey } from 'survey-react'
import { Modal, Button } from 'react-bootstrap'
import axios from 'axios';
import surveyJson from './surveyJson'

import { login } from '../../../core/actions/authActions'

Survey.cssType = "bootstrap";

const css = {
  navigation: {
    complete: "survey-button survey-nav-complete-btn",
		prev: "survey-button survey-nav-prev-btn ",
		next: "survey-button survey-nav-next-btn"
  },
  checkbox: {
		"root": "form-inline",
    "item": "checkbox",
		"other": ""
	}
}

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: true,
      loggedIn: false,
      isLoading: false
    };

    this.close = this.close.bind(this);
    this.onSurveyComplete = this.onSurveyComplete.bind(this);
    this.surveyValidateQuestion = this.surveyValidateQuestion.bind(this);
    this.handleRecoveryRequestClick = this.handleRecoveryRequestClick.bind(this);
  }

  componentDidMount() {
    setTimeout(function() {
      document.querySelector(".survey-nav-complete-btn").value = "Sign in";
      var elem = document.querySelector(".form-inline");
      elem.parentElement.firstElementChild.style.display = "none";
    }, 50);
  }

  close() {
    this.setState({ showModal: false });
    let { loggedIn } = this.state;
    setTimeout(() => {
      this.props.onClose(loggedIn);
    }, 1000);
  }

  onSurveyComplete(result) {
    this.close();
  }

  handleRecoveryRequestClick() {
    this.props.handleRecoveryRequestClick()
  }

  surveyValidateQuestion(survey, options) {
    let { email, password } = options.data;
    this.props.login({ email, password }).then(
      (res) => {
        this.setState({ ...this.state, loggedIn: true });
        options.complete();
      },
      (err) => { 
        options.errors['password'] = 'Invalid credentials';
        options.complete(); 
      }
    ).catch(error => {
      options.errors['password'] = 'Invalid credentials';
      options.complete(); 
    });
  }

  render() {
    let { showModal } = this.state;
    var element = document.querySelector(".survey-nav-complete-btn");
    element ? element.value = "Sign in" : '';
    return (
      <div>
        <Modal show={showModal} onHide={this.close} backdrop={false}>
          <Modal.Header closeButton>
            <Modal.Title>Sign in</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Survey json={surveyJson} css={css} onServerValidateQuestions={this.surveyValidateQuestion} onComplete={this.onSurveyComplete}/>
            <div className="recovery-link" onClick={this.handleRecoveryRequestClick}>Forgot your password</div>
          </Modal.Body>
        </Modal>
      </div>
    )
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
}

export default connect(null, { login })(Login)
