export default {
 showQuestionNumbers: "off",
 showCompletedPage: false,
 pages: [
  {
   elements: [
    {
     type: "text",
     inputType: "email",
     isRequired: true,
     name: "email",
     placeHolder: "Please enter your email",
     title: "Email",
     validators: [
      {
       type: "email"
      }
     ]
    },
    {
     type: "text",
     inputType: "password",
     isRequired: true,
     name: "password",
     placeHolder: "Please enter your password",
     title: "Password",
     validators: [
      {
       type: "text",
       text: "Text length should be between 3 and 10 symbols",
       minLength: "3",
       maxLength: "50"
      }
     ]
    },
    {  
     type: "checkbox",
     choices: [
      "Remember me"
     ],
     name: "remember",
     title: "Remember me"
    }
   ],
   name: "login"
  }
 ]
}
