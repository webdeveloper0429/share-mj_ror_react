import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from '../core/store/configureStore'
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes' 

const store = configureStore()

render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)
