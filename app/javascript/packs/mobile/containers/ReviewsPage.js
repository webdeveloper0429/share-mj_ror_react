import React, { Component } from 'react';
import { Page, Navbar, List, Card, CardContent, CardHeader, GridRow, GridCol } from 'framework7-react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EmptyList from '../components/EmptyList';
import Moment from 'react-moment';

import { fetchReviews } from '../../core/actions/reviews';

const styles = {
  right: {
    textAlign: "right"
  }
}

class ReviewsPage extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      userId: null,
    };
  }

  componentDidMount() {
    const { params: { userId } } = this.context.route;
    this.setState({ userId });
    this.props.fetchReviews(userId);
  }

  render() {
    const { list } = this.props;
    return (
      <Page name="reviews" toolbarFixed>
        <Navbar title="Reviews" backLink="Back" />
          {
            list && list.length > 0 
              ? <List>{list.map(({ id, feedback, score, created_at, reviewer }) => {
                return <Review key={id} text={feedback} score={score} created_at={created_at} reviewer={reviewer} />;
              })} </List>
              : <EmptyList message="Review list is empty"/>
          }      
      </Page>
    )
  }
}

const Review = ({ reviewer, text, score, created_at }) => {
  const { nickname, avatar: { thumb_url } } = reviewer;

  return (
    <Card>
      <CardHeader>
        <GridRow>
          <GridCol>
            <img src={thumb_url} width='50' height='50' />
          </GridCol>
        </GridRow>
        <GridRow>
          <GridCol>
            {nickname}
          </GridCol>
        </GridRow>
      </CardHeader>
      <CardContent>
        <h5 style={styles.right}> <Moment fromNow>{created_at}</Moment></h5>
        <h5><strong>{score} stars</strong></h5>
        <p>{text}</p>
      </CardContent>
    </Card>
  );
};

ReviewsPage.contextTypes = {
  route: PropTypes.object.isRequired
}

function mapStateToProps ({ session, reviews: { list } }) {
  return { session, list }
}

export default connect(mapStateToProps, { fetchReviews })(ReviewsPage)
