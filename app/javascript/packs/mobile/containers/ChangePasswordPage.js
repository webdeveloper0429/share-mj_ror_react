import React, {Component} from 'react'
import {
  Page,
  Navbar,
  List,
  ListButton,
  ListItem,
  Actions,
  ActionsGroup,
  ActionsLabel,
  ActionsButton,
  FormLabel,
  FormInput,
  ButtonsSegmented,
  Button,
  NavRight,
  Link,
  PickerModal,
  Toolbar,
  ContentBlock,
  ContentBlockTitle
} from 'framework7-react'
import {connect} from 'react-redux'
import { updateUser } from '../../core/actions/users';
import validateInput from '../../core/utils/validators/common'

class ChangePasswordPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      pickerIsOpen: false,
      successPickerIsOpen: false,
      passwordChanged: false,
      errors: {},
      validators: {
        current_password: {
          validations:{
            required: true
          }
        },
        password: {
          validations:{
            required: true
          }
        },
        repeat_password: {
          validations:{
            required: true
          }
        }
      }
    }

    this.handleSave = this.handleSave.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.closePicker = this.closePicker.bind(this);
    this.onBackClickHandle = this.onBackClickHandle.bind(this);
  }

  componentWillReceiveProps(props) {
    if(props.users.updated && this.state.passwordChanged) {
      this.setState({
        ...this.state,
        successPickerIsOpen: true,
        passwordChanged: true
      });
    }
  }
  
  onBackClickHandle() {
    this.closePicker();
  }

  closePicker() {
    this.setState({
      ...this.state,
      pickerIsOpen: false,
      successPickerIsOpen: false
    });
  }

  handleSave() {
    let errors = {};
    const { validators } = this.state;

    if(Object.keys(this.state.user).length == 0) return

    for(let key in this.state.user) {
      if(validators.hasOwnProperty(key)){
        const {inputErrors, isValid} = validateInput(this.state.user[key], validators[key].validations);
        if(!isValid) {
          errors[key] = inputErrors;
        }
      }
    }

    if(this.state.user.password != this.state.user.repeat_password) {
      errors["password"] = {
        isSame: "Passwords is not same"
      }
    }

    if(!errors || Object.keys(errors) == 0) {
      this.setState({
        ...this.state,
        passwordChanged: true,
        pickerIsOpen: false
      });
      this.props.updateUser(this.props.session.user.id, this.state.user);
    } else {
      this.setState({...this.state, pickerIsOpen: true, errors});
    }
  }

  handleChange(e) {
    let { user } = this.state;
    const currentElement = e.target.parentElement.id;

    if(user[currentElement] != e.target.value) {
      user[currentElement] = e.target.value;
      this.setState({...this.state, user});
    }
  }

  getErrorMessages(errors) {
    const errorKeys = Object.keys(errors);
    return errorKeys.map(key => {
      const keyName = document.querySelector(`#${key}`).firstElementChild.placeholder;
      const errorMessages = Object.keys(errors[key]).map(errorKey => {
        const message = `${errors[key][errorKey]}`;
        return <span key={errorKey}>{ message } </span>
      })
        return <div key={key}> 
          <ContentBlockTitle>
            {keyName}
          </ContentBlockTitle>
          <ContentBlock>
            {errorMessages}
          </ContentBlock>
        </div>
    })
  }


  render() {
    const { user } = this.props.session;
    const userState = this.state.user;
    const { pickerIsOpen, successPickerIsOpen, errors } = this.state;
    return (
      <Page name="change-password" toolbarFixed>
        <Navbar title="Change Password" backLink="Back" onBackClick={this.onBackClickHandle}>
          <NavRight>
            <Link onClick={this.handleSave}>Save</Link>
          </NavRight>
        </Navbar>
        <List>
          <ListItem>
            <FormLabel>Current Password</FormLabel>
            <FormInput type="password" placeholder="Current Password" id="current_password" value={userState.hasOwnProperty("current_password") ? userState.current_password : (user.current_password || "")} onChange={ this.handleChange }/>
          </ListItem>
          <ListItem>
            <FormLabel>New Password</FormLabel>
            <FormInput type="password" placeholder="New Password" id="password" value={userState.hasOwnProperty("password") ? userState.password : (user.password || "")} onChange={ this.handleChange }/>
          </ListItem>
          <ListItem>
            <FormLabel>Repeat Password</FormLabel>
            <FormInput type="password" required="true" placeholder="Repeat Password" id="repeat_password" value={userState.hasOwnProperty("repeat_password") ? userState.repeat_password : (user.repeat_password || "")} onChange={ this.handleChange }/>
          </ListItem>
        </List>

        <PickerModal opened={pickerIsOpen}>
          <Toolbar>
            <Link onClick={this.closePicker}>Done</Link>
          </Toolbar>
          {
            this.getErrorMessages(errors)
          }
        </PickerModal>

        <PickerModal opened={successPickerIsOpen}>
          <Toolbar>
            <Link onClick={this.closePicker}>Done</Link>
          </Toolbar>
            <span className="success">Password has been successfull changed!</span>
        </PickerModal>
      </Page>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {session: state.session, users: state.users}
}

export default connect(mapStateToProps, { updateUser })(ChangePasswordPage)
