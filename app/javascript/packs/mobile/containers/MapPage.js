import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Navbar, Page, List, ListItem, ListItemSwipeoutActions, ListItemSwipeoutButton, NavLeft, Link, NavCenter,FormLabel, FormInput, ButtonsSegmented, Button} from 'framework7-react';
import { getFramework7 } from '../components/App';
import Map from '../components/Map';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchRequests } from '../../core/actions/requests';

const menuStyles={
  margin:0,
  zIndex: 99999999
}

const buttonSegmentStyles={
  width:"100%"
}

const listItemStyles={
  padding: "5px",
  zIndex: 99999999
}

const sliderInput = {
  width: "75%",
  display: "inline-block",
  float: "left",
  zIndex: 999999999
}

class MapPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: {
        radius: 1,
        location: {
          lat: 41.881832,
          lng: -87.623177
        },
        zoom: 14
      }
    };

    this.onChangeRadiuslider = this.onChangeRadiuslider.bind(this);
    this.backToPreviousPage = this.backToPreviousPage.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleZipCode = this.handleZipCode.bind(this);
    this.setDefaultSliderValue = this.setDefaultSliderValue.bind(this);
    this.handleChangeLocation = this.handleChangeLocation.bind(this);
  }

  componentDidMount() {
    this.setDefaultSliderValue();
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.handleZipCode, (err)=> {
        console.log(err);
      },{
        enableHighAccuracy: true, timeout: 20000, maximumAge: 100
      });
    }
  }

  handleZipCode(position) {
    const latlng = {lat: position.coords.latitude, lng:  position.coords.longitude};
    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        location: latlng
      }
    })
  }

  onChangeRadiuslider(e) {
    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        radius: parseInt(e.target.value)
      }
    })
  }

  handleChangeLocation(location) {
    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        location
      }
    })
  }

  handleSearch() {
    const { location, radius } = this.state.options;
    this.props.fetchRequests("search", {
      lng: location.lng,
      lat: location.lat,
      radius: radius
    });
    setTimeout(function() {
      getFramework7().mainView.router.back();
    }, 200);
  }

  backToPreviousPage() {
    getFramework7().mainView.router.back();
  }

  setDefaultSliderValue() {
    this.sliderInput = ReactDOM.findDOMNode(this.refs.radiusslider);
    this.sliderInput.firstElementChild.firstElementChild.value = 1;
  }

  render() {
    const { options } = this.state;
    return (
      <Page name="map" toolbarFixed>
        <Navbar title="Change location" backLink="Cancel" sliding> </Navbar>
        <div className="map-container">
          <Map options={options} handleChangeLocation={this.handleChangeLocation}/>
        </div>
        <List style={menuStyles}>
          <ListItem style={listItemStyles}>
            <FormInput style={sliderInput} ref="radiusslider" onChange={this.onChangeRadiuslider} type="range" min="1" max="20" step="1" value={options.radius.toString()}></FormInput>
            <span className="radius">{options.radius} miles</span>
          </ListItem>
          <ListItem style={listItemStyles}>
            <ButtonsSegmented style={buttonSegmentStyles}>
              <Button onClick={this.backToPreviousPage}>Cancel</Button>
              <Button onClick={this.handleSearch}>Confirm</Button>
            </ButtonsSegmented>
          </ListItem>
        </List>
      </Page>
    )
  }
}

const  mapStateToProps = ({ }) => {
  return { };
}


export default connect(mapStateToProps, { fetchRequests })(MapPage);