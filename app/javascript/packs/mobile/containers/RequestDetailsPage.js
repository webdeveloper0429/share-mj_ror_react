import React, { Component } from 'react'
import { Page, Navbar, ContentBlock, GridRow, GridCol, CardContent, Button, List, ListItem, PickerModal, Toolbar, Link,
Actions, ActionsGroup, ActionsLabel, ActionsButton} from 'framework7-react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import { fetchRequest, deleteRequest } from '../../core/actions/requests';
import { createChat } from '../../core/actions/chats';

const styles = {
  cost: {
    fontWeight: "bold",
    padding: "15px 0",
    display: "block"
  },
  hide: {
    display:"none"
  }
}

class RequestDetailsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      request: {},
      fetched: false,
      pickerIsOpen: false,
      opened: false
    };
 
    this.handleMessageClick = this.handleMessageClick.bind(this);
    this.closePicker = this.closePicker.bind(this);
    this.deleteClick = this.deleteClick.bind(this);
    this.deleteQuestionClick = this.deleteQuestionClick.bind(this);
    this.closeActionPicker = this.closeActionPicker.bind(this);
  }

  componentDidMount() {
    const { id } = this.context.route.params;
    this.props.fetchRequest(id).then(res => {
      this.setState({ fetched: true, request: res.data.result });
    });
  }

  componentWillReceiveProps(props) {
    if(props.requests.deleted) {
      const { f7 } = this.context;
      f7.mainView.router.loadPage('home?activetab=my');
    }
  }

  closePicker() {
    this.setState({
      ...this.state,
      pickerIsOpen: false
    });
  }

  deleteQuestionClick() {
    this.setState({opened: true});
  }

  deleteClick() {
    const { id } = this.state.request;
    this.props.deleteRequest(id);
  }

  closeActionPicker() {
    this.setState({opened: false});
  }
 
  handleMessageClick(e) {
    const { user } = this.props.session;
    const userId = user.id;
    const targetUserId = this.state.request.user_id;
    const requestId = this.state.request.id;

    if(!user.credits || user.credits < 1) {
      this.setState({
        ...this.state,
        pickerIsOpen: true
      });
    } else {
      this.props.createChat({ userId, targetUserId, requestId }).then(res => {
        const { result: { id } } = res.data;
        this.handleSuccessChatConnect(id); 
      }).catch(err => {
        //if (err.response.data.result == 'Current user already in chat') {
          //this.handleSuccessChatConnect();    
        //} 
      });
    }
  }

  handleSuccessChatConnect(chatId) {
    const { f7 } = this.context;
    f7.mainView.router.loadPage(`chats/${chatId}/messages`);
  }

  render() {
    const { request, fetched, pickerIsOpen, opened } = this.state;
    const { user } = this.props.session;
    return (
      <Page name="request-details" toolbarFixed>
        <Navbar title="Request" backLink="Back" />
        {fetched ? <FetchedContent request={request} sessionUser={user} onClick={this.handleMessageClick} deleteQuestionClick={this.deleteQuestionClick} /> : <div>Loading...</div>}
        <PickerModal opened={pickerIsOpen}>
          <Toolbar>
            <Link onClick={this.closePicker}>Done</Link>
          </Toolbar>
          <span className="low-balance">You have low balance</span>
        </PickerModal>
        <Actions opened={opened} onActionsClose={this.closeActionPicker}>
          <ActionsGroup>
            <ActionsLabel>Are you sure ?</ActionsLabel>
            <ActionsButton color="red" onClick={this.deleteClick}>Delete</ActionsButton>
          </ActionsGroup>
          <ActionsGroup>
            <ActionsButton bold>Cancel</ActionsButton>
          </ActionsGroup>
        </Actions>
      </Page>
    )
  }
}

const FetchedContent = ({ request: { cost, help, type, weight, bio, user }, onClick, sessionUser, deleteQuestionClick }) => {
  const { nickname, avatar: { thumb_url }, id } = user;

  const rating = `${user.rating}`;

  const handleClick = (e) => onClick();
  return (
    <div>
      <List>
        <ListItem
          link={`users/${user.id}`}
          media={`<img src='${thumb_url}' width='100' height='100'>`}
          title={nickname}>
        </ListItem>
        {bio != undefined && bio.length != 0 ? (
          <ListItem
          link={`users/${user.id}`}>
            <p>{bio}</p>
          </ListItem>
        ) : null}
        <ListItem link={`users/${user.id}`}>View profile</ListItem>
        {/*<ListItem link="#">Report member</ListItem>*/}
      </List>
      <ContentBlock inner>
        <CardContent inner>
          <GridRow>
            <GridCol>{help}</GridCol>
            <GridCol>{rating}</GridCol>
          </GridRow>
          <GridRow>
            <GridCol>{type}</GridCol>
            <GridCol>{weight} oz</GridCol>
          </GridRow>
          <GridRow style={sessionUser.id == id ? styles.hide : ''}>
            <GridCol><span style={styles.cost}>{`Message price is ${cost} credits`}</span></GridCol>
          </GridRow>
        </CardContent>
            <p>
              <Button style={sessionUser.id == id ? styles.hide : ''} big onClick={onClick}>Message now</Button>
            </p>
            <p>
              <Button color="red" style={sessionUser.id != id ? styles.hide : ''} big onClick={deleteQuestionClick}>Delete</Button>
            </p>
      </ContentBlock>
    </div>
  );
};

const DeleteRequestConfirmation = (opened) => {
  return (
    <Actions opened={opened}>
      <ActionsGroup>
        <ActionsLabel>Are you sure ?</ActionsLabel>
        <ActionsButton color="red">Delete</ActionsButton>
      </ActionsGroup>
      <ActionsGroup>
        <ActionsButton bold>Cancel</ActionsButton>
      </ActionsGroup>
    </Actions>
  )
}

RequestDetailsPage.propTypes = {
  session: PropTypes.object.isRequired,
  fetchRequest: PropTypes.func.isRequired,
  createChat: PropTypes.func.isRequired
}

RequestDetailsPage.contextTypes = {
  f7: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
}

function mapStateToProps (state) {
  return {
    session: state.session,
    requests: state.requests
  }
}

export default connect(mapStateToProps, { fetchRequest, createChat, deleteRequest })(RequestDetailsPage)
