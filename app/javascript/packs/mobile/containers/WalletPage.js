import React, { Component } from 'react'
import { Page, Navbar, List, ListButton, ListItem, Actions, ActionsGroup, ActionsLabel, ActionsButton } from 'framework7-react'
import { connect } from 'react-redux'

class WalletPage extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      actionsOpened: false,
    };

    this.handleAddCreditsClick = this.handleAddCreditsClick.bind(this);
    this.closePrices = this.closePrices.bind(this);
  }

  openPrices() {
    this.setState({ ...this.state, actionsOpened: true });
  }
  
  closePrices() {
    this.setState({ ...this.state, actionsOpened: false });
  }

  handleAddCreditsClick(e) {
    this.openPrices();
  }

  render() {
    let {actionsOpened} = this.state;

    return (
      <Page name="wallet" toolbarFixed>
        <Navbar title="My wallet" backLink="Back"></Navbar>
        <List>
          <ListItem
            title="Your credits"
            after="<string>134$</strong>">
          </ListItem>
          <ListButton onClick={this.handleAddCreditsClick} color="green">
            Add credits
          </ListButton>
        </List>
        <List>
          <ListItem link="#">Payments history</ListItem>
        </List>

        <Actions opened={actionsOpened}>
          <ActionsGroup>
            <ActionsLabel>Please choose amount</ActionsLabel>
            <ActionsButton>5$</ActionsButton>
            <ActionsButton>10$</ActionsButton>
            <ActionsButton>15$</ActionsButton>
            <ActionsButton>20$</ActionsButton>
            <ActionsButton>40$</ActionsButton>
            <ActionsButton>100$</ActionsButton>
          </ActionsGroup>
          <ActionsGroup>
            <ActionsButton color="red" bold onClick={this.closePrices}>Cancel</ActionsButton>
          </ActionsGroup>
        </Actions>
 
      </Page>
    )
  }
}

function mapStateToProps (state) {
  return {
    session: state.session
  }
}

export default connect(mapStateToProps)(WalletPage)
