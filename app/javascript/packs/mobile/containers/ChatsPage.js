import React, { Component } from 'react'
import { Navbar, Page, List, ListItem, ListItemSwipeoutActions, ListItemSwipeoutButton, NavLeft, Link, NavCenter } from 'framework7-react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import EmptyList from '../components/EmptyList';
import Moment from 'react-moment';
import moment from 'moment';
import { fetchChats, deleteChat } from '../../core/actions/chats';

moment.locale('en');

class ChatsPage extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      chatId: null
    };

    this.onSwipeoutDeleted = this.onSwipeoutDeleted.bind(this);
  }

  componentDidMount() {
    const { session: { user } } = this.props;
    if (user.id) {
      this.props.fetchChats({ userId: user.id });
    }
  }

  onSwipeoutDeleted(id) {
    this.props.deleteChat(id);
  }

  render() {
    const { chats: { list }, session } = this.props;
    const { user } = session;
    const title = <span className='chat-icon'> <img src={user.avatar ? user.avatar.thumb_url : ""}></img></span>;
    return (
      <Page name="chats" toolbarFixed>
         <Navbar > 
          <NavLeft>
            <Link href={`users/${user.id}`}>{title}</Link>
          </NavLeft>
          <NavCenter>Chat</NavCenter>
        </Navbar>
        {
          list && list.length > 0 
            ?<List>
              {list.map(({ id, users, last_message, created_at }) => {
                const targetUser = users[0] == user.id ? users[1] : users[0];
                const { avatar: { thumb_url }, nickname } = targetUser;
                return (
                  <ListItem
                    key={id}
                    link={`chats/${id}/messages`}
                    media={`<img src='${thumb_url}' width='40' height='40'>`}
                    title={`${nickname} <br/> ${moment(created_at).fromNow()}`}
                    after={last_message ? last_message.body : ''} 
                    onSwipeoutDeleted = {this.onSwipeoutDeleted.bind(event, id)}
                    swipeout>
                    <ListItemSwipeoutActions right>
                      <ListItemSwipeoutButton delete>Delete</ListItemSwipeoutButton>
                    </ListItemSwipeoutActions>
                  </ListItem>
                );
              })}
            </List>
            :<EmptyList message="Chat list is empty"/>
          }
      </Page>
    )
  }
}

ChatsPage.propTypes = {
  session: PropTypes.object.isRequired,
  fetchChats: PropTypes.func.isRequired
};

const mapStateToProps = ({ session, chats }) => {
  return { session, chats };
}

export default connect(mapStateToProps, { fetchChats, deleteChat })(ChatsPage)
