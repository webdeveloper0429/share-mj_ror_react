import React, { Component } from 'react'
import { Page, Navbar, NavRight, List, ListItem, FormLabel, FormInput, Link, PickerModal, Toolbar } from 'framework7-react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import faker from 'faker'
import {getFramework7} from '../components/App';

import { reportUser } from '../../core/actions/users';

class ReportPage extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      userId: null,
      message: '',
      pickerIsOpen: false,
      isSended: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.closePicker = this.closePicker.bind(this);
    this.onBackClickHandle = this.onBackClickHandle.bind(this);
  }

  componentDidMount() {
    const { params: { userId } } = this.context.route;
    this.setState({ userId }); 
  }

  componentWillReceiveProps(props) {
      if(props.users.posted != this.state.isSended) {
        this.setState({pickerIsOpen: true, isSended: true});
      }
  }

  closePicker() {
    this.setState({pickerIsOpen: false});
    getFramework7().mainView.router.loadPage("home");
  }

  onBackClickHandle() {
    this.closePicker();
  }

  handleSubmit(e) {
    const { f7 } = this.context;
    const { userId, message } = this.state
    let { reportUser } = this.props;

    reportUser(message, userId);
  }

  handleChange({ target: { name, value } }) {
    this.setState({ [name]: value }); 
  }

  render() {
    const { message, pickerIsOpen } = this.state;
    console.log(this.state);
    return (
      <Page name="report" toolbarFixed>
        <Navbar title="Report member" backLink="Back" onBackClick={this.onBackClickHandle}>
          <NavRight>
            <Link onClick={this.handleSubmit}>Save</Link>
          </NavRight>
        </Navbar>
      
        <List form>
          <ListItem>
            <FormInput
              type="textarea"
              placeholder="Please enter your report message"
              name="message"
              value={message}
              onChange={this.handleChange}>
            </FormInput>
          </ListItem>
        </List>

        <PickerModal opened={pickerIsOpen}>
          <Toolbar>
            <Link onClick={this.closePicker}>Done</Link>
          </Toolbar>
          <span className="report-message-success">Report has been created</span>
        </PickerModal>
      </Page>
    )
  }
}

ReportPage.contextTypes = {
  f7: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
}

const mapStateToProps = ({ session, users }) => {
  return { session, users };
};
export default connect(mapStateToProps, { reportUser })(ReportPage)
