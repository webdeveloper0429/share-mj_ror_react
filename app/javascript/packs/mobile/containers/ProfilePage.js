import React, { Component } from 'react';
import { Page, Navbar, List, ListItem, ListButton } from 'framework7-react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import faker from 'faker';
import {fetchUser} from '../../core/actions/users'

const styles = {
  hideActionButtons: {
    display: 'none'
  }
}

class ProfilePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null
    };
    this.showActionButtons = this.showActionButtons.bind(this);
  }

  componentDidMount() {
    const { params: { id } } = this.context.route;
    this.setState({ id }); 
    this.props.fetchUser(id);
  }

  showActionButtons(show, id) {
    let view = null;
    if(show) {
      view = (
        <List>
          <ListButton link={`users/${id}/reviews`}>View reviews</ListButton>
        </List>
      )
    } else {
      view = (
        <List>
          <ListButton link={`users/${id}/reviews/new`}>Add review</ListButton>
          <ListButton link={`users/${id}/reviews`}>View reviews</ListButton>
          <ListButton link={`users/${id}/report`} color="red">Report member</ListButton>
        </List>
      )
    }

    return view;
  }

  render() {
    const { id } = this.state;
    const { user } = this.props.users;
    const sessionUser = this.props.session.user;
    return (
      <Page name="profile">
        <Navbar title="Profile" backLink="Back" />
        <List>
          <ListItem
            media={`<img src=${user && user.avatar ? user.avatar.thumb_url : ''} width='100' height='100'>`}
            title={user.first_name + ' ' + user.last_name}>
          </ListItem>
        </List>
        <List>
          <ListItem title="Location" after={user.location}></ListItem>
          <ListItem title="Rating" after={user.rating}></ListItem>
          <ListItem title="Requests count" after={user.requests_count}></ListItem>
          <ListItem link="personal-information">Edit profile</ListItem>
          <ListItem link="my-requests">My requests</ListItem>
        </List>
        {
          this.showActionButtons(id == sessionUser.id, id)
        }
      </Page>
    )
  }
}

ProfilePage.contextTypes = {
  route: PropTypes.object.isRequired
}

const mapStateToProps = ({ session, users }) => {
  return { session, users };
};

export default connect(mapStateToProps, {fetchUser})(ProfilePage)
