export default {
  goNextPageAutomatic : true,
  pages : [
    {
      elements: [
        {
          type: "text",
          isRequired: true,
          name: "zip",
          placeHolder: "Please enter your zip code",
          title: "Your zip code"
        }
      ],
      name: "zip"
    }, {
      elements: [
        {
          type: "radiogroup",
          choices: [
            {
              value: "mj",
              text: "I want MJ"
            }, {
              value: "edibles",
              text: "I want Edibles"
            }
          ],
          isRequired: true,
          name: "help",
          title: "How can we help you?"
        }
      ],
      name: "help"
    }, {
      elements: [
        {
          type: "radiogroup",
          choices: [
            {
              value: "dont_know",
              text: "I don’t know"
            }, {
              value: "sativa",
              text: "Sativa"
            }, {
              value: "hybrid",
              text: "Hybrid"
            }, {
              value: "indica",
              text: "Indica"
            }
          ],
          isRequired: true,
          name: "request_type",
          title: "Which type do you prefer?"
        }
      ],
      name: "type",
      visible: true
    }, {
      elements: [
        {
          type: "radiogroup",
          choices: [
            {
              value: "1/8",
              text: "1/8 Oz (3.5 grams)"
            }, {
              value: "1/4",
              text: "1/4 Oz (7 grams)"
            }, {
              value: "1/2",
              text: "1/2 Oz (14 grams)"
            }, {
              value: "1",
              text: "1 Oz (28 grams)"
            }, {
              value: "2",
              text: "2 Oz (56 gram)"
            }, {
              value: "dont_know",
              text: "I don’t know"
            }
          ],
          isRequired: true,
          name: "weight",
          title: "How much would you like?"
        }
      ],
      name: "weight",
      visible: true
    }
  ],
  showCompletedPage : false,
  showPageTitles : false,
  showProgressBar : "bottom",
  showQuestionNumbers : "off",
  triggers : [
    {
      type: "visible",
      operator: "notequal",
      value: "dont_know",
      name: "help",
      pages: ["type", "weight"]
    }
  ]
}
