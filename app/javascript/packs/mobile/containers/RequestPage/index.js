import React, { Component } from 'react';
import { Page, Navbar, ContentBlock, NavLeft, NavCenter, Link } from 'framework7-react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getFramework7 } from '../../components/App';

import { get } from 'axios';

import { Survey } from 'survey-react';
import surveyJson from './surveyJson';

import { createRequest } from '../../../core/actions/requests';

const myCss={
  text: "survey-input-text",
  question: {
    title: "survey-question-title"
  },
  progress: "survey-progress",
  progressBar: "survey-progress-bar",
  navigationButton: "survey-button",
  navigation: {
    complete: "survey-button survey-nav-complete-btn",
		prev: "survey-button survey-nav-prev-btn ",
		next: "survey-button survey-nav-next-btn"
  },
  radiogroup: {
    item: "survey-radiogroup-item"
  }
}

const styles = {
  show: {
    "visibility": "visible",
    "minHeight": "300px"
  },
  hide: {
    "display": "none"
  }
}

class RequestPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      position: null,
      ip: null, 
      isCompleted: false
    };

    this.position = null;
    this.zipCode = null;
    this.ip = null;
    this.questionElement = null;

    this.handleSurveyComplete = this.handleSurveyComplete.bind(this);
    this.onAfterRenderPage = this.onAfterRenderPage.bind(this);
    this.handleZipCode = this.handleZipCode.bind(this);
  }

  componentDidMount() {
    get('//ipapi.co/json/').then((res) => {
      this.ip = res.data.ip;
    });
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        this.handleZipCode,
        (err)=>{
          const { user } = this.props.session;
          this.questionElement ? this.questionElement.htmlElement.querySelector("input").value = user.zip_code : "";
          console.log(err);
        },
        {
          enableHighAccuracy: true, timeout: 20000, maximumAge: 100
        });
    }
  }

  handleZipCode(position) {
    const { session } = this.props;
    const { user } = session;
    this.position = position;
    const geocoder = new google.maps.Geocoder;
    const latlng = {lat: position.coords.latitude, lng:  position.coords.longitude};
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        for(let i=0; i < results[0].address_components.length; i++)
        {
          let component = results[0].address_components[i];
          if(component.types[0] == "postal_code")
          {
            this.zipCode = component.long_name;
            this.questionElement ? this.questionElement.htmlElement.querySelector("input").value = this.zipCode : "";
          }
        }
      }
    }.bind(this))
  }

  onAfterRenderPage(survey, question) {
    this.questionElement = question;
  }

  handleSurveyComplete(s) {
    const { ip, position } = this.state;
    const { request_type, weight, help } = s.valuesHash;
    let payload = { request_type, weight, help };
    if (this.position) {
      payload.latitude = this.position.coords.latitude;
      payload.longitude = this.position.coords.longitude;
    } else {
      payload.address = this.ip;
    }

    this.setState({
      ...this.state,
      isCompleted: true
    })

    this.props.createRequest(payload).then(res => {
      getFramework7().mainView.router.loadPage('my-requests');
    });
  }

  render() {
    const { session } = this.props;
    const { user } = session;
    const title = <span className='chat-icon'> <img src={user.avatar ? user.avatar.thumb_url : ""}></img></span>;
    return (
      <Page name="create_request" toolbarFixed>
        <Navbar > 
          <NavLeft>
            <Link href={`users/${user.id}`}>{title}</Link>
          </NavLeft>
          <NavCenter>New request</NavCenter>
        </Navbar>
        <ContentBlock inner style={this.state.isCompleted ? styles.hide : {}}>
          <Survey json={surveyJson} css={myCss} onComplete={this.handleSurveyComplete} onAfterRenderQuestion={this.onAfterRenderPage}/>
        </ContentBlock>
        <ContentBlock>
          <div className="request-complete" style={this.state.isCompleted ? styles.show : {}}>
            Creating, wait please...
          </div> 
        </ContentBlock>
      </Page>
    )
  }
}

RequestPage.propTypes = {
  createRequest: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  return {
    session: state.session
  }
}

export default connect(mapStateToProps, { createRequest })(RequestPage);
