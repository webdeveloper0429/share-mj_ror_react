import React, { Component } from 'react'
import { Page, Navbar, NavRight, List, ListItem, FormLabel, FormInput, Link } from 'framework7-react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import faker from 'faker'

import { createReview } from '../../core/actions/reviews';

class NewReviewPage extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      userId: null,
      feedback: '',
      score: '1'
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { params: { userId } } = this.context.route;
    this.setState({ userId }); 
  }

  handleSubmit(e) {
    const { f7 } = this.context;
    const { userId, feedback, score } = this.state
    let { createReview } = this.props;

    createReview(userId, { feedback, score }).then(res => {
      f7.mainView.router.back();
    });
  }

  handleChange({ target: { name, value } }) {
    this.setState({ [name]: value }); 
  }

  render() {
    const { feedback, score } = this.state;
    console.log(this.state);
    return (
      <Page name="new_review" toolbarFixed>
        <Navbar title="New review" backLink="Back">
          <NavRight>
            <Link onClick={this.handleSubmit}>Save</Link>
          </NavRight>
        </Navbar>
      
        <List form>
          <ListItem>
            <FormLabel>Score</FormLabel>
            <FormInput
              type="select"
              name="score"
              value={score}
              onChange={this.handleChange}>
              <option value="1">1 star</option>
              <option value="2">2 stars</option>
              <option value="3">3 stars</option>
              <option value="4">4 stars</option>
              <option value="5">5 stars</option>
            </FormInput>
          </ListItem>
          <ListItem>
            <FormInput
              type="textarea"
              placeholder="Please enter your review"
              name="feedback"
              value={feedback}
              onChange={this.handleChange}>
            </FormInput>
          </ListItem>
        </List>
      </Page>
    )
  }
}

NewReviewPage.contextTypes = {
  f7: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
}

const mapStateToProps = ({ session }) => { session };

export default connect(mapStateToProps, { createReview })(NewReviewPage)
