import React, {Component} from 'react'
import {
  Page,
  Navbar,
  List,
  ListButton,
  ListItem,
  Actions,
  ActionsGroup,
  ActionsLabel,
  ActionsButton,
  FormLabel,
  FormInput,
  ButtonsSegmented,
  Button,
  NavRight,
  Link,
  PickerModal,
  Toolbar,
  ContentBlock,
  ContentBlockTitle,
  Icon
} from 'framework7-react'
import {connect} from 'react-redux'
import { updateUser } from '../../core/actions/users';
import validateInput from '../../core/utils/validators/common'

class PersonalInformationPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      pickerIsOpen: false,
      errors: {},
      validators: {
        nickname: {
          validations:{
            required: true
          }
        },
        email: {
          validations:{
            required: true,
            email: true
          }
        }
      },
      hideName: true
    };

    this.handleSave = this.handleSave.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleUploaderChange = this.handleUploaderChange.bind(this);
    this.closePicker = this.closePicker.bind(this);
    this.onBackClickHandle = this.onBackClickHandle.bind(this);
  }

  componentWillUnmount() {
    this.closePicker();
  }

  onBackClickHandle() {
    this.closePicker();
  }

  closePicker() {
    this.setState({pickerIsOpen: false});
  }

  handleSave() {
    let errors = {};
    const { validators } = this.state;
    for(let key in this.state.user) {
      if(validators.hasOwnProperty(key)){
        const {inputErrors, isValid} = validateInput(this.state.user[key], validators[key].validations);
        if(!isValid) {
          errors[key] = inputErrors;
        }
      }
    }

    if(!errors || Object.keys(errors) == 0) {
      this.setState({pickerIsOpen: false});
      this.props.updateUser(this.props.session.user.id, this.state.user);
    } else {
      this.setState({pickerIsOpen: true, errors});
    }
  }

  handleChange(e) {
    let { user } = this.state;
    const currentElement = e.target.parentElement.id;

    if(user[currentElement] != e.target.value) {
      user[currentElement] = e.target.value;
      this.setState({user});
    }
  }

  handleUploaderChange(e) {
    let { user } = this.state;
    const currentElement = e.target.parentElement.id;

    let reader  = new FileReader();
    reader.onloadend = () => {
      user[currentElement] = reader.result;
      this.setState({user});
    };
    reader.readAsDataURL(e.target.files[0]);
  }

  getErrorMessages(errors) {
    const errorKeys = Object.keys(errors);
    return errorKeys.map(key => {
      const keyName = document.querySelector(`#${key}`).firstElementChild.placeholder;
      const errorMessages = Object.keys(errors[key]).map(errorKey => {
        const message = `${errors[key][errorKey]}`;
        return <span>{ message }  <br/></span>
      })
         return <div> 
          <ContentBlockTitle>
            {keyName}
          </ContentBlockTitle>
          <ContentBlock>
            {errorMessages}
          </ContentBlock>
        </div>
    })
  }

  render() {
    const { user } = this.props.session;
    const userState = this.state.user;
    const { pickerIsOpen, errors } = this.state;
    return (
      <Page name="personal-information" toolbarFixed>
        <Navbar title="Personal Information" backLink="Back" onBackClick={this.onBackClickHandle}>
          <NavRight>
            <Link onClick={this.handleSave}>Save</Link>
          </NavRight>
        </Navbar>
        <List>
          <ListItem style={{display:"none"}}>
            <FormLabel>First Name</FormLabel>
            <FormInput type="text" placeholder="First Name" id="first_name" value={userState.hasOwnProperty("first_name") ? userState.first_name : (user.first_name || "")} onChange={ this.handleChange }/>
          </ListItem>
          <ListItem style={{display:"none"}}>
            <FormLabel>Last Name</FormLabel>
            <FormInput type="text" placeholder="Last Name" id="last_name" value={userState.hasOwnProperty("last_name") ? userState.last_name : (user.last_name || "")} onChange={ this.handleChange }/>
          </ListItem>
          <ListItem>
            <FormLabel>Nickname</FormLabel>
            <FormInput type="text" required="true" placeholder="Nickname" id="nickname" value={userState.hasOwnProperty("nickname") ? userState.nickname : (user.nickname || "")} onChange={ this.handleChange }/>
          </ListItem>
          <ListItem>
            <FormLabel>Email</FormLabel>
            <FormInput type="email" placeholder="Email" id="email" value={userState.hasOwnProperty("email") ? userState.email : (user.email || "")} onChange={ this.handleChange }/>
          </ListItem>
          {/* <ListItem>
            <img src={userState.avatar} hidden={!userState.avatar} height='100'/>
          </ListItem> */}
        </List>

        <List>
          <ListButton link={`change-password`}>Change Password </ListButton>
        </List>

        <List>
          <ListItem>
            <label className="upload-avater">
                Upload avatar
                <Icon f7="cloud_upload" />
                <FormInput type="file" id="avatar" style={{display: 'none'}} onChange={ this.handleUploaderChange }/>
            </label>
          </ListItem>
        </List>

        <PickerModal opened={pickerIsOpen}>
          <Toolbar>
            <Link onClick={this.closePicker}>Done</Link>
          </Toolbar>
          {
            this.getErrorMessages(errors)
          }
        </PickerModal>
      </Page>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {session: state.session, users: state.users}
}

export default connect(mapStateToProps, { updateUser })(PersonalInformationPage)
