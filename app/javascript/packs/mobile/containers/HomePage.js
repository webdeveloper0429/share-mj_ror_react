import React, { Component } from 'react';
import { Page, Navbar, Searchbar, Card, GridRow, GridCol, CardContent, NavCenter, NavRight, CardFooter, Link, CardHeader, NavLeft, ButtonsSegmented, Button, Actions, ActionsGroup, ActionsLabel, ActionsButton, Icon, Popup, ContentBlock } from 'framework7-react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import EmptyList from '../components/EmptyList';
import { getCurrentRoute } from '../components/App';
import { fetchRequests } from '../../core/actions/requests';
import Moment from 'react-moment';

const searchbarStyle = {
  background: "transparent"
}

class HomePage extends Component {
  constructor(props) {
    super(props);

    const route = getCurrentRoute();

    this.state = {
      reviewActionsOpened: false
    };

    this.handleReviewClick = this.handleReviewClick.bind(this);
    this.handleReviewActionsClose = this.handleReviewActionsClose.bind(this);
  }
  
  componentDidMount() {
    this.props.fetchRequests('public');
  }
  
  handleReviewActionsClose() {
    setTimeout(() => {
      this.setState({ ...this.state, reviewActionsOpened: false });
    }, 800);
  }

  handleReviewClick() {
    this.handleReviewActionsClose();
    setTimeout(() => {
      this.props.f7.mainView.router.loadPage('/new_review/');
    }, 800);
  }

  render() {
    let { activeTabIndex, reviewActionsOpened } = this.state;
    const { requests: { list }, session } = this.props;
    const targetUser = session.user;
    const title = <span className='chat-icon'> <img src={targetUser.avatar ? targetUser.avatar.thumb_url : ""}></img></span>;
    
    return (
      <Page name="home" toolbarFixed>
        <Navbar > 
          <NavLeft>
            <Link href={`users/${targetUser.id}`}>{title}</Link>
          </NavLeft>
          <NavCenter>
             <form style={searchbarStyle} className="searchbar" >
              <div className="searchbar-input">
                <a href="map"> 
                  <input type="search" placeholder="Search by location" value={targetUser.zip_code} />
                </a>
                <a href="#" className="searchbar-clear"></a>
              </div>
              <div className="searchbar-cancel">Cancel</div>
            </form>
          </NavCenter>
          <NavRight>
            <Link href="request"><Icon f7="add_round" color="white"/></Link>
          </NavRight>
        </Navbar>

        { reviewActionsOpened ? <ReviewActions opened={reviewActionsOpened} onClose={this.handleReviewActionsClose} onClickReview={this.handleReviewClick} /> : null }
        {
          list && list.length > 0 
            ? <Requests items={list} /> 
            : <EmptyList message="Request list is empty"/>
        }
      </Page>
    )
  }
}

const RatingStars = (rating) => {
  const getStars = () => {
    let stars = [];
    for(let i=0; i<rating; i++) {
      stars.push(<Icon style={styles.starStyle} f7="star_fill"/>)
    }

    for(let i=0; i<5-rating; i++) {
      stars.push(<Icon style={styles.starStyle} f7="star"/>)
    }

    return stars;
  }
  return (
    <div>
      {
        getStars()
      }
    </div>
  )
}

const Request = ({ request, buttonValue }) => {
  const {
    id, type, weight, help, created_at, chats_count,
    user: {
      nickname,
      avatar: { thumb_url },
      rating
    }
  } = request;

  return (
    <GridCol>
      <Card>
        <div className="request-background"></div>
        <CardHeader>
          <GridRow>
            <GridCol>
              <img src={thumb_url} width='40' height='40' />
            </GridCol>
          </GridRow>
          <GridRow>
            <GridCol>
              <span style={styles.nickname}>{nickname}</span>
              <span className="request-creation-date">
                <Moment fromNow>{created_at}</Moment>
              </span>
            </GridCol>
          </GridRow>
        </CardHeader>
        <CardContent>
           <GridRow>
            <GridCol> 
              <div className="description-container"> 
                <span className="label">Help:</span>
                <span className="value">{help}</span>
              </div>
              <div className="description-container"> 
                <span className="label">Rating: </span>
                <span className="value">{RatingStars(rating)}</span>
              </div>
            </GridCol>
            <GridCol> 
              <div className="description-container">
                <span className="label">Type:</span>
                <span className="value">{type}</span>
              </div>
              <div className="description-container"> 
                <span className="label">Weight:</span>
                <span className="value">{weight}</span>
              </div>
            </GridCol>
          </GridRow> 
          {
            chats_count == 0
            ? <span className="first-to-respond">Be first to respond!</span> 
            : ""
          }
        </CardContent>
      </Card>
    </GridCol>
  );
};

const Requests = ({ items }) => {
  let requests = [];
  for (let i = 0; i < (items.length); i++) {
    const url = "requests/"+items[i].id;
    requests.push(
      <GridRow key={i} noGutter>
        <a className="request-link" href={url}> 
          <Request request={items[i]} buttonValue={items[i].cost} />
        </a>
      </GridRow>
    );
  }
  
  return <div>{requests}</div>;
};

const ReviewActions = ({ opened, onClickReview, onClose }) => {
  const handleClose = (e) => {
    onClose();
  };

  const handleClick = (e) => {
    onClickReview();
  };
  
  return (
    <Actions opened={opened}>
      <ActionsGroup>
        <ActionsLabel>You have 1 unreviewed member</ActionsLabel>
        <ActionsButton onClick={handleClick}>Give a review</ActionsButton>
      </ActionsGroup>
      <ActionsGroup>
        <ActionsButton color="red" bold onClick={handleClose}>Cancel</ActionsButton>
      </ActionsGroup>
    </Actions>
  );
};

let styles = {
  nickname: {
    fontSize:"16px",
    textTransform: "capitalize",
    textAlign: "right",
    display: "block"
  },
  product: {
    fontSize:"12px",
    textAlign: "left",
    display: "block",
    textTransform: "capitalize"
  },
  type: {
    fontSize:"12px",
    textAlign: "left",
    display: "block",
    textTransform: "capitalize"
  },
  flexible: {
    fontSize:"12px",
    textAlign: "left",
    display: "block",
    textTransform: "capitalize"
  },
  weight: {
    fontSize:"12px",
    textAlign: "left",
    display: "block",
    textTransform: "capitalize"
  }, 
  starStyle: {
    fontSize: "16px"
  }
}

HomePage.contextTypes = {
  f7: PropTypes.object.isRequired
}

HomePage.propTypes = {
  session: PropTypes.object.isRequired,
  fetchRequests: PropTypes.func.isRequired
};

const  mapStateToProps = ({ session, requests }) => {
  return { session, requests };
}

export default connect(mapStateToProps, { fetchRequests })(HomePage)
