import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page, Navbar, ContentBlock, Button, FormInput, List, ListItem, FormLabel, ListButton, Link, NavCenter, NavLeft } from 'framework7-react'
import {
  ShareButtons,
  ShareCounts,
  generateShareIcon
} from 'react-share';

import CopyToClipboard from 'react-copy-to-clipboard';

const {
  FacebookShareButton,
  TwitterShareButton
} = ShareButtons;

class ReferralsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      link: "https://sharemj.com"
    }
  }

  render() {
    const {referral_token} = this.props.session.user;
    const referralLink = this.state.link + '?referral-token=' + referral_token;
    const mailToParams = `mailto:?subject=ShareMJ&body=${referralLink}`;
    const { session } = this.props;
    const { user } = session;
    const title = <span className='chat-icon'> <img src={user.avatar ? user.avatar.thumb_url : ""}></img></span>;
    return (
      <Page name="referrals" toolbarFixed>
        <Navbar > 
          <NavLeft>
            <Link href={`users/${user.id}`}>{title}</Link>
          </NavLeft>
          <NavCenter>Invite friends</NavCenter>
        </Navbar>
        <List form>
          <ListItem>
            <FormLabel>Your invite link</FormLabel>
            <FormInput type="text" placeholder="Your referral url" value={referralLink} />
          </ListItem>
          <CopyToClipboard text={referralLink} onCopy={this.onCopy}>
            <ListButton>
                Click to copy
            </ListButton>
          </CopyToClipboard> 
        </List>
        <ContentBlock inner>
          <p style={{ textAlign: "center" }}>
            <Button iconF7="email" external color="green" big href={mailToParams}> Invite via Email</Button>
          </p>
          <FacebookShareButton title="ShareMJ" url={referralLink} content={referralLink}>
            <p style={{ textAlign: "center" }}>
              <Button iconF7="social_facebook" color="blue" big> Invite via Facebook</Button>
            </p>
          </FacebookShareButton>
          <TwitterShareButton title="ShareMJ" url={referralLink} content={referralLink}>
            <p style={{ textAlign: "center" }}>
              <Button iconF7="social_twitter" color="lightblue" big> Invite via Twitter</Button>
            </p>
          </TwitterShareButton>
        </ContentBlock>
        {/*<ContentBlock inner>
          <h3>Your earnings</h3>
        </ContentBlock>*/}
      </Page>
    )
  }
}

function mapStateToProps (state) {
  return {
    session: state.session
  }
}

export default connect(mapStateToProps)(ReferralsPage)
