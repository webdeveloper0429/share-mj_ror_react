import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Page, Navbar, List, ListButton, ListItem, NavLeft, NavCenter, Link } from 'framework7-react'
import { connect } from 'react-redux'

import { logout } from '../../core/actions/authActions';

const styles = {
  hideStyle: {
    display: 'none'
  }
}

class SettingsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.handleClickLogout = this.handleClickLogout.bind(this);
  }

  handleClickLogout(e) {
    this.props.logout();
    window.location.href = '/';
  }

  render() {
    const { session } = this.props;
    const { user, fetching } = session;
    const title = <span className='chat-icon'> <img src={user.avatar ? user.avatar.thumb_url : ""}></img></span>;
    return (
      <Page name="settings" toolbarFixed>
        <Navbar> 
          <NavLeft>
            <Link href={`users/${user.id}`}>{title}</Link>
          </NavLeft>
          <NavCenter>Settings</NavCenter>
        </Navbar>

        {fetching ? <div></div> : <PageContent user={user} onClickLogout={this.handleClickLogout} />}
      </Page>
    )
  }
}

const PageContent = ({ user, onClickLogout }) => {
  const { id, nickname, email, credits, avatar: { thumb_url } } = user;
  const handleClickLogout = (e) => onClickLogout();
  return (
    <div>
      <List>
        <ListItem
          link={`users/${id}`}
          media={`<img src='${thumb_url}' width='100' height='100'>`}
          title={`${nickname || email}<br/>View your profile`}>
        </ListItem>
      </List>
      <List>
        {/* <ListButton>{`Balance: ${credits} credit(s)`}</ListButton> */}
        <ListItem link="referrals">Invite friends</ListItem>
        <ListItem link="wallet" style={styles.hideStyle}>Setup your wallet</ListItem>
      </List>
      <List>
        <ListButton color="red" onClick={handleClickLogout}>Log out</ListButton>
      </List>
    </div>
  );
};

SettingsPage.propTypes = {
  session: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired
};

const mapStateToProps = ({ session }) => {
  return { session };
}

export default connect(mapStateToProps, { logout })(SettingsPage)
