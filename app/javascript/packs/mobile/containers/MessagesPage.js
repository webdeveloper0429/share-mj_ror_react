import React, { Component, ReactDOM } from 'react'
import { Page, Navbar, Toolbar, Messages, Message, Messagebar, NavRight, Link, Icon, FormInput } from 'framework7-react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import EmptyList from '../components/EmptyList';

import { fetchChat, createMessage, handleNewMessage, fetchChats } from '../../core/actions/chats';

const styles = {
  messageBar: {
    "width": "80%",
    "left": "20%",
  },
  infoIconTooltip: {
    fontSize: "15px",
    padding: "0 10px 0 0"
  }
}

class MessagesPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fetched: false,
      message: '',
      image: '',
      fastQuestionIsActive: false
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleInputSubmit = this.handleInputSubmit.bind(this);
    this.handleNewMessage = this.handleNewMessage.bind(this);
    this.toggleFastMessagesContainer = this.toggleFastMessagesContainer.bind(this);
    this.selectMessage = this.selectMessage.bind(this);
    this.handleUploaderChange = this.handleUploaderChange.bind(this);
    this.checkIsFirstResponse = this.checkIsFirstResponse.bind(this);
  }

  handleUploaderChange(e) {
    let { image, message } = this.state;
    const { chatId, session: { user }, chats } = this.props;
    const currentElement = e.target.parentElement.id;

    let reader  = new FileReader();
    reader.onloadend = () => {
      image = reader.result;
      this.setState({image});
      this.props.createMessage({ userId: user.id, chatId, message, image })
    };
    reader.readAsDataURL(e.target.files[0]);
  }

  fetchMessages() {
    const { params: { chatId } } = this.context.route;
    const { session: { user } } = this.props;
    
    this.setState({ fetched: false });
    this.props.fetchChat({ userId: user.id, chatId }).then(res => {
      this.setState({ fetched: true });
    });
  }

  startListeningChannel() {
    const {
      actionCable,
      route: {
        params: { chatId }
      }
    } = this.context;

    this.channel = actionCable.subscriptions.create({
      channel: 'ChatChannel',
      chat_id: chatId,
    }, {
      connected: () => { console.log('connected'); },
      received: this.handleNewMessage
    });
 
  }

  stopListeningChannel() {
    const { actionCable } = this.context;
    actionCable.subscriptions.remove(this.channel);
  }

  componentDidMount() {
    this.fetchMessages();
    this.startListeningChannel();
  }

  componentWillUnmount() {
    this.stopListeningChannel();
    const {session: { user }} = this.props;
    this.props.fetchChats({ userId: user.id });
  }

  handleNewMessage(data) {
    this.props.handleNewMessage(data);
  }

  handleInputChange(value) {
    this.setState({ message: value });
  }

  handleInputSubmit(_message, clear) {
    const { chatId, session: { user }, chats } = this.props;
    const { chat } = chats;
    const { message } = this.state;

    if (message.length != 0) {
      this.props.createMessage({ userId: user.id, chatId, message })
      if(clear) clear();
    }
  }

  toggleFastMessagesContainer() {
    this.setState({
      ...this.state,
      fastQuestionIsActive: !this.state.fastQuestionIsActive
    })
  }

  checkIsFirstResponse(chat) {
    const { user } = this.props.session
    const currentUserId = user.id;
    const message = chat.messages.find((res)=>{
      return res.user.id == currentUserId;
    })
    if(message) {
      this.setState({
        ...this.state,
        fastQuestionIsActive: false
      })
    }else{
      this.setState({
        ...this.state,
        fastQuestionIsActive: true
      })
    }
  }

  componentWillReceiveProps(props) {
    if(props.chats && props.chats.chat && props.chats.chat.messages) {
      this.checkIsFirstResponse(props.chats.chat);
    }
  }

  selectMessage(message) {
    const { chatId, session: { user }, chats } = this.props;
    this.props.createMessage({ userId: user.id, chatId, message })

    this.setState({
      ...this.state,
      fastQuestionIsActive: false
    })
  }

  render() {
    const { fetched, message, fastQuestionIsActive } = this.state;
    const { session, chats } = this.props;
    const { chat } = chats;
    const currentUser = session.user;

    if (fetched) {
      const { users } = chat;
      var targetUser = users[0] == currentUser.id ? users[1] : users[0];
      const { nickname } = targetUser;
      var title = <span className='chat-icon'> <img src={targetUser.avatar.thumb_url}></img></span>;
      var list = chat.messages || [];
      var link = "users/"+targetUser.id;
    } else {
      var title = '';
      var list = [];
    }

    const fastQuestionClasses = fastQuestionIsActive ? "fast-questions-container active-fast-question" : "fast-questions-container";
    
    return (
      <Page name="chat" noTabbar messages>
        <Navbar title={title} backLink="Back" sliding>
          <NavRight>
            <Link href={link}><Icon f7="info" color="white"/></Link>
          </NavRight>
        </Navbar>
        {
          list && list.length > 0 
            ? <Messages>
              {list.map(({ id, body, created_at, user, data_type, image_url}) => {
                if(data_type == "image"){
                  body = <img src={image_url}/>
                } 
                return (
                  <Message
                    key={id}
                    name={user.id == currentUser.id ? currentUser.nickname : targetUser.nickname}
                    text={body}
                    date={created_at}>
                  </Message>
                );
              })}
            </Messages>
            :<EmptyList message="Message list is empty"/>
        }

        <div className="fast-question-button" style={!fastQuestionIsActive ? {display:"none"} : null} onClick={this.toggleFastMessagesContainer}> 
          <Icon f7="menu"/>
        </div>

        <label className= { !fastQuestionIsActive ? "upload-image upload-image-center" :  "upload-image"}>
          <Icon f7="camera_fill" />
          <FormInput type="file" id="avatar" style={{display: 'none'}} onChange={ this.handleUploaderChange }/>
        </label>

        <div className={fastQuestionClasses}> 
          <p className="tooltip-fast-question">
            <Icon f7="info" color="white" style={styles.infoIconTooltip}/>
            Don’t share your email, phone number, or personal information
          </p>
          <div className="fast-questions-content">
            <span className="title">
              Select a message or type your own
            </span>
            <span className="question" onClick={this.selectMessage.bind( this, "phrase1" )}>phrase1 ? </span>
            <span className="question" onClick={this.selectMessage.bind( this, "phrase2" )}>phrase2 ? </span>
            <span className="question" onClick={this.selectMessage.bind( this, "phrase3" )}>phrase3 ? </span>
          </div>
        </div>

        <Messagebar style={styles.messageBar} placeholder="Message" sendLink="Send" onInput={this.handleInputChange} onSubmit={this.handleInputSubmit}></Messagebar>
      </Page>
    )
  }
}

MessagesPage.contextTypes = {
  f7: PropTypes.object.isRequired,
  actionCable: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
}

MessagesPage.PropTypes = {
  session: PropTypes.object.isRequired,
  chats: PropTypes.object.isRequired,
  fetchChats: PropTypes.func.isRequired,
  onBackClickHandle: PropTypes.func.isRequired
}

function mapStateToProps ({ session, chats }) {
  return { session, chats }
}

export default connect(mapStateToProps, { fetchChat, fetchChats, createMessage, handleNewMessage })(MessagesPage)
