import ProfilePage from './containers/ProfilePage'
import MessagesPage from './containers/MessagesPage'
import WalletPage from './containers/WalletPage'
import RequestDetailsPage from './containers/RequestDetailsPage'
import ReviewsPage from './containers/ReviewsPage'
import NewReviewPage from './containers/NewReviewPage'
import ReportPage from './containers/ReportPage'
import HomePage from './containers/HomePage'
import ChatsPage from './containers/ChatsPage'
import SettingsPage from './containers/SettingsPage'
import RequestPage from './containers/RequestPage'
import ReferralsPage from './containers/ReferralsPage'
import PersonalInformationPage from './containers/PersonalInformationPage'
import ChangePasswordPage from './containers/ChangePasswordPage'
import MapPage from './containers/MapPage'
import MyRequests from './containers/MyRequests'

export const routes = [
  {
    path: 'users/:id',
    component: ProfilePage
  }, {
    path: 'users/:userId/reviews/new',
    component: NewReviewPage
  },{
    path: 'users/:userId/reviews',
    component: ReviewsPage
  },{
    path: 'users/:userId/report',
    component: ReportPage
  },{
    path: 'requests/:id',
    component: RequestDetailsPage
  },{
    path: 'my-requests',
    component: MyRequests
  },{
    path: 'map',
    component: MapPage
  }, {
    path: 'wallet',
    component: WalletPage
  }, {
    path: 'chats/:chatId/messages',
    component: MessagesPage
  }, {
    path: 'home',
    component: HomePage
  }, {
    path: 'settings',
    component: SettingsPage
  }, {
    path: 'referrals',
    component: ReferralsPage
  }, {
    path: 'request',
    component: RequestPage
  }, {
    path: 'chats',
    component: ChatsPage
  }, {
    path: 'personal-information',
    component: PersonalInformationPage
  },{
    path: 'change-password',
    component: ChangePasswordPage
  }
];

