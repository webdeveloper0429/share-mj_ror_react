import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { environment } from '../core/config';
import App from './components/App';
import configureStore from '../core/store/configureStore';

import 'framework7-icons/css/framework7-icons.css';

const store = configureStore()
const elem = document.getElementById('root'); 

const renderComponent = (Comp) => {
  render(
    <Provider store={store}>
      <Comp store={store} />
    </Provider>,
  elem);
};

if (environment == 'development') {
  const RedBox = require('redbox-react').default;
  try {
    renderComponent(App);
  } catch (e) {
    render(<RedBox error={e} />, elem);
  }
} else {
  renderComponent(App);
}
