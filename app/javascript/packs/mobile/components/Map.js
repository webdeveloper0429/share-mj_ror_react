import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const mapStyle = {position: "relative", display:"block", height:"100%", width: "100%"}

const searchInputStyles = {
  marginRight: "20px",
  marginLeft: "0px",
  position: "absolute",
  top: "0px",
  left: "0px",
  width: "100%",
  height: "30px",
  fontSize: "14px",
  textAlign: "center"
}

export default class Map extends React.Component {
  constructor(props) {
    super(props)

    this.marker = null;
    this.sityCircle = null;
    this.options = {
      radius: props.options.radius,
      location: props.options.location,
      zoom: props.options.zoom
    }
    this.map = null;
    this.autocomplete = null;
    this.autocompleteInput = null;
  }

  componentDidMount() {
    this.loadMap();
  }

  componentWillReceiveProps(props) {
    if(props.options.radius !== this.options.radius) {
      this.setRadius(props);
    }

    if(props.options.location !== this.options.location) {
      this.setLocation(props);
    }
  }

  onChangeLocation() {
    this.props.handleChangeLocation(this.options.location);
  }

  setRadius(props) {
    const { radius } = props.options;
    this.options.radius = radius;
    this.clearCircle()
    this.setCircle()
  }

  setLocation(props) {
    const { location } = props.options;
    this.options.location = location;
    const geocoder = new google.maps.Geocoder;
    const latlng = {lat: this.options.location.lat, lng:  this.options.location.lng};
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        this.autocomplete.set("place", results[0])
      }
    }.bind(this));
  }

  setZoom(zoom) {
    this.options.zoom = zoom;
  }

  initAutocomplete() {
    this.autocompleteInput = ReactDOM.findDOMNode(this.refs.autocomplete);

    const types = document.getElementById('type-selector');

    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.autocompleteInput);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    this.autocomplete = new google.maps.places.Autocomplete(this.autocompleteInput);
    this.autocomplete.bindTo('bounds', this.map);

    let infowindow = new google.maps.InfoWindow();
    this.marker = new google.maps.Marker({
      map: this.map
    });

    google.maps.event.addListener(this.autocomplete, 'place_changed', function() {
      let place = this.autocomplete.getPlace();
      this.autocompleteInput.value = place.formatted_address;
      const location = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      }
      this.options.location = location;
      this.map.setCenter(new google.maps.LatLng(location.lat,location.lng));

      this.clearCircle();
      this.setCircle();
      this.onChangeLocation();
    }.bind(this));
  }

  setCircle() {
    this.cityCircle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
      center: this.options.location,
      radius: 1000 * this.options.radius
    });
  }

  clearCircle() {
    if(this.cityCircle) {
      this.cityCircle.setMap(null);
    }
  }

  loadMap() {
    const maps = google.maps;
    const mapRef = this.refs.map;
    const node = ReactDOM.findDOMNode(mapRef);
    const { options } = this;
    const center = new maps.LatLng(options.location.lat, options.location.lng );

    const mapConfig = Object.assign({}, {
      center: center,
      zoom: options.zoom,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    })
    this.map = new maps.Map(node, mapConfig);

    const divElement = document.createElement('div');
    divElement.className = "centerMarker";
    this.map.getDiv().appendChild(divElement);

    if(options.location && options.location.lat && options.location.lng) {
      const geocoder = new google.maps.Geocoder;
      const latlng = {lat: center.lat(), lng:  center.lng()};
      geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
          this.autocomplete.set("place", results[0])
        }
      }.bind(this));
    }

    this.map.addListener('zoom_changed', function(e) {
      const zoom = this.map.getZoom();
      this.setZoom(zoom);
      
      this.map.setCenter(new google.maps.LatLng(this.options.location.lat,this.options.location.lng));
    }.bind(this));

    this.map.addListener('dragend', function() {
      const center = this.map.getCenter();
      const geocoder = new google.maps.Geocoder;
      const latlng = {lat: center.lat(), lng:  center.lng()};
      geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
          this.autocomplete.set("place", results[0])
        }
      }.bind(this));
    }.bind(this));

    this.initAutocomplete();
    this.setCircle()
  }

  render() {
    return (
      <div className="map">
        <div ref='map' style={mapStyle}>
          Loading map...
        </div>
        <input id="pac-input" ref="autocomplete" style={searchInputStyles} className="controls" type="text" placeholder="Street, City, State, Zip" />
      </div>
    )
  }
}