import React from 'react';

const EmptyList = ({message}) => {
  return (
    <div>
      <span className="empty-list">{message}</span>
    </div>
  );
};

export default EmptyList;
