import React, { Component } from 'react';
import { Framework7App, Statusbar, Views, View, Pages, Page, Navbar, NavLeft, NavCenter, Toolbar, Link, PickerModal, Button, NavRight, Icon} from 'framework7-react';
import { connect } from 'react-redux';
import { routes } from '../../routes'
import PropTypes from 'prop-types';
import NavTabs from '../NavTabs';
import HomePage from '../../containers/HomePage'
import ActionCable from 'actioncable';
import { cablePath } from '../../../core/config';
import setAuthToken from '../../../core/utils/setAuthToken';
import { fetchCurrentSession, setCurrentSession } from '../../../core/actions/authActions';
import { Cookies } from 'react-cookie';
import UploadPhoto from '../UploadPhoto';

let framework7;
let currentRoute;

const searchbarStyle = {
  background: "transparent"
}

export const getFramework7 = () => {
  return framework7;
};

export const getCurrentRoute = () => currentRoute;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      f7Init: false,
      isLandscape: false,
      route: {
        url: "/home/"
      },
      pickerIsOpen: false,
      showUploadPhoto: false
    };

    this.f7 = null;
    this.actionCable = null;

    this.f7DidInitialized = this.f7DidInitialized.bind(this);
    this.handleRouteChange = this.handleRouteChange.bind(this);
    this.checkSize = this.checkSize.bind(this);
    this.cookies = new Cookies();

    this.closeAvatarRequestModal = this.closeAvatarRequestModal.bind(this);
    this.uploadAvatar = this.uploadAvatar.bind(this);
  }

  componentDidMount() {
    const { setCurrentSession, fetchCurrentSession, session: { isAuthenticated } } = this.props;
    const authToken = this.cookies.get("authToken");
    
    if (authToken && !isAuthenticated) {
      setAuthToken(authToken);
      setCurrentSession({ fetching: true,  user: {} });
      fetchCurrentSession();
      const token = authToken.split(" ")[1];
      this.actionCable = ActionCable.createConsumer(`${cablePath}?auth_token=${token}`);
    } else {
      if (!isAuthenticated) {
        this.actionCable = null;
        window.location.href = '/';
      }
    }

    window.onresize = function(event) {
      this.checkSize();
    }.bind(this);
    this.checkSize();
  }

  componentWillReceiveProps(props) {
    if(props.session.user && props.session.user.hasOwnProperty("auth_token")) {
      this.checkAvatarRequestQuestion(props.session); 
    }
  }

  componentWillUpdate(nextProps) {
    const { session: { isAuthenticated, fetching } } = nextProps;
    if (!isAuthenticated && !fetching) {
      this.actionCable = null;
      window.location.href = '/';
    } else {
      if (!this.state.f7Init) {
        this.setState({ f7Init: true });
        this.f7.init();
      }
    }
  }

  checkAvatarRequestQuestion(session) {
    const { user } = session;
    const avatarLoaded = this.cookies.get("avatarLoaded");
    if((!avatarLoaded || avatarLoaded == "false") && (!user.avatar || !user.avatar.url || user.avatar.url == "" || user.avatar.url.match("fakeimg"))) {
      this.setState({
        ...this.state, 
        pickerIsOpen: true
      });
    }
  }

  closeAvatarRequestModal() {
    this.setState({
      ...this.state, 
      pickerIsOpen: false
    });

    this.cookies.set("avatarLoaded", true);
  }

  uploadAvatar() {
    this.setState({
      ...this.state, 
      showUploadPhoto: true
    });

    this.cookies.set("avatarLoaded", true);
  }

  checkSize() {
    const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    if(window.innerWidth < 768 && window.innerWidth > window.innerHeight && isMobile) {
      this.setState({isLandscape:true});
    }else if(!this.isLandscape){
      this.setState({isLandscape:false});
    }
  }

  getChildContext() {
    return {
      f7: this.f7,
      actionCable: this.actionCable,
      route: this.state.route
    };
  }

  f7DidInitialized(f7) {
    this.f7 = f7;
    window.f7 = f7;
    framework7 =f7;
  }

  handleRouteChange(route) {
    currentRoute = route;
    this.setState({ route });
  }

  render() {
    const { store, session  } = this.props;
    let { route: { url }, isLandscape, pickerIsOpen, showUploadPhoto } = this.state;
    const targetUser = session ? session.user : {};
    const title = <span className='chat-icon'> <img src={targetUser.avatar ? targetUser.avatar.thumb_url : ""}></img></span>;
    return (
      <Framework7App themeType="ios" routes={routes} onFramework7Init={this.f7DidInitialized} onRouteChange={this.handleRouteChange} pushState={true} pushStateSeparator="" pushStateRoot="/dashboard/">
        <Statusbar />
        <Views theme="custom">
          <View main dynamicNavbar>
          <Navbar > 
            <NavLeft>
              <Link href={`users/${targetUser.id}`}>{title}</Link>
            </NavLeft>
            <NavCenter>
              <form style={searchbarStyle} className="searchbar">
                <div className="searchbar-input">
                  <a href="map"> 
                    <input type="search" placeholder="Search by location" value={targetUser.zip_code} />
                  </a>
                  <a href="#" className="searchbar-clear"></a>
                </div>
                <div className="searchbar-cancel">Cancel</div>
              </form>
            </NavCenter>
            <NavRight>
              <Link href="request"><Icon f7="add_round" color="white"/></Link>
            </NavRight>
          </Navbar>
            <Pages navbarThrough toolbarThrough>	
              <Page></Page>
            </Pages>

            <NavTabs currentPath={url} />

            <PickerModal opened={pickerIsOpen}>
              <Toolbar>
                Add profile picture for best results
              </Toolbar>
              {
                showUploadPhoto 
                ? <UploadPhoto postAction = {this.closeAvatarRequestModal} /> 
                : <div> 
                    <Button className="add-profile-picture-button" onClick={this.uploadAvatar}>Upload avatar</Button>
                    <Button color="red" className="add-profile-picture-button" onClick={this.closeAvatarRequestModal}>No thanks</Button>
                  </div>
              }
            </PickerModal>
          </View>
        </Views>

        <div className={isLandscape ? "is-landscape landscape-active" : "is-landscape"}>
          <span>Turn your phone please</span>
        </div>
      </Framework7App>
    );
  }
}

App.childContextTypes = {
  f7: PropTypes.object,
  actionCable: PropTypes.object,
  route: PropTypes.object
};

App.propTypes = {
  session: PropTypes.object.isRequired,
  fetchCurrentSession: PropTypes.func.isRequired,
  setCurrentSession: PropTypes.func.isRequired
}

const mapStateToProps = ({ session }) => {
  return { session };
}

export default connect(mapStateToProps, { fetchCurrentSession, setCurrentSession })(App);
