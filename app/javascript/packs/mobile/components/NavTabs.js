import React from 'react';
import { Toolbar, Link } from 'framework7-react';

const NavTabs = ({ currentPath }) => {
  return (
    <Toolbar tabbar labels>
      <TabLink href="home" text="Home" icon="home" currentPath={currentPath} />
      <TabLink href="chats" text="Chat" icon="chat" currentPath={currentPath} />
      <TabLink href="request" text="Request" icon="add" currentPath={currentPath} />
      <TabLink href="referrals" text="Invite" icon="paper_plane" currentPath={currentPath} />
      <TabLink href="settings" text="Settings" icon="settings" currentPath={currentPath} />
    </Toolbar>
  );
};

const TabLink = ({ icon, text, href, currentPath }) => {
  return (
    <Link href={href} active={currentPath == href} animatePages={false} force>
      <i className="f7-icons icon">{icon}</i>
      <span className="tabbar-label">{text}</span>
    </Link>
  );
};

export default NavTabs;
