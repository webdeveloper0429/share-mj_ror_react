import React, {Component} from 'react'
import {
  Page,
  Navbar,
  List,
  ListButton,
  ListItem,
  Actions,
  ActionsGroup,
  ActionsLabel,
  ActionsButton,
  FormLabel,
  FormInput,
  ButtonsSegmented,
  Button,
  NavRight,
  Link,
  PickerModal,
  Toolbar,
  ContentBlock,
  ContentBlockTitle,
  Icon
} from 'framework7-react'
import {connect} from 'react-redux'
import {updateUser} from '../../../core/actions/users';

class UploadPhoto extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      errors: {}
    };

    this.handleUploaderChange = this.handleUploaderChange.bind(this);
    this.postAction = this.postAction.bind(this);
  }

  handleUploaderChange(e) {
    let {user} = this.state;
    const currentElement = e.target.parentElement.id;
    let reader = new FileReader();
    reader.onloadend = () => {
      user[currentElement] = reader.result;
      this.setState({user});

      this.props.updateUser(this.props.session.user.id, this.state.user);
      this.postAction();
    };
    reader.readAsDataURL(e.target.files[0]);
  }

  postAction() {
    this.props.postAction();
  }

  render() {
    return (
      <List>
        <ListItem>
          <label className="upload-avater">
            Upload avatar
            <Icon f7="cloud_upload"/>
            <FormInput type="file" id="avatar" style={{ display: 'none'}} onChange={this.handleUploaderChange} />
          </label>
        </ListItem>
      </List>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {session: state.session, users: state.users}
}

export default connect(mapStateToProps, {updateUser})(UploadPhoto)
