import {
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  REPORT_USER_REQUEST,
  REPORT_USER_SUCCESS,
  REPORT_USER_FAILURE
} from '../actions/types';

const initialState = {
  updating: false,
  updated: false,
  fetching: false,
  fetched: false,
  posting: false,
  posted: false,
  error: {},
  user: {}
}

export default(state = initialState, action = {}) => {
  switch (action.type) {
    case UPDATE_USER_REQUEST:
      return {
        ...state,
        updated: false,
        updating: true
      }
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        updated: true,
        updating: false,
        user: action.payload
      }
    case UPDATE_USER_FAILURE:
      return {
        ...state,
        updated: false,
        updating: false,
        error: action.payload
      }
    case FETCH_USER_REQUEST:
      return {
        ...state,
        fetching: true,
        fetched: false,
        user: {},
        error: {}
      }
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        fetching: false,
        fetched: true,
        user: action.payload,
        error: {}
      }
    case FETCH_USER_FAILURE:
      return {
        ...state,
        fetching: true,
        fetched: false,
        user: {},
        error: action.payload
      }
    case REPORT_USER_REQUEST:
      return {
        ...state,
        posting: true,
        posted: false,
        user: {},
        error: {}
      }
    case REPORT_USER_SUCCESS:
      return {
        ...state,
        posting: false,
        posted: true,
        error: {}
      }
    case REPORT_USER_FAILURE:
      return {
        ...state,
        posting: false,
        posted: false,
        user: {},
        error: action.payload
      }
    default:
      return state;
  }
}
