import {RECOVERY_PASSWORD_REQUEST_SUCCESS, RECOVERY_PASSWORD_REQUEST_ERROR, RECOVERY_PASSWORD_COMPLETE_SUCCESS,
  RECOVERY_PASSWORD_COMPLETE_ERROR} from '../actions/types';

const initialState = {
  result: {},
  error: {},
  isSuccess: false,
  isError: false
}

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case RECOVERY_PASSWORD_REQUEST_SUCCESS:
      return { 
        isSuccess: true,
        isError: false,
        result: action.payload
      };
    case RECOVERY_PASSWORD_REQUEST_ERROR:
      return { 
        isError: true,
        isSuccess: false,
        error: action.payload 
      };
    case RECOVERY_PASSWORD_COMPLETE_SUCCESS:
      return {
        isError: false,
        isSuccess: true,
        error: action.payload 
      }
    case RECOVERY_PASSWORD_COMPLETE_ERROR:
      return {
        isError: true,
        isSuccess: false,
        error: action.payload 
      }
    default: return state;
  }
};
