import { SET_REVIEWS } from '../actions/types';

const initialState = {
  list: []
}

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case SET_REVIEWS:
      return { list: action.payload };
    default: return state;
  }
};
