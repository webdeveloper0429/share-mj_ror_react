import { SET_CHATS, SET_CHAT, DELETE_CHAT_SUCCESS, DELETE_CHAT_ERROR } from '../actions/types';

const initialState = {
  list: [],
  chat: {},
  deleted: false,
  errors: {}
}

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case SET_CHATS:
      return { list: action.payload, deleted: false };
    case SET_CHAT:
      return { ...state, chat: action.payload, deleted: false };
    case DELETE_CHAT_SUCCESS:
      return {
        ...state,
        deleted: true,
        errors: {}
      }
    case DELETE_CHAT_ERROR: 
      return {
         ...state,
        deleted: false,
        errors: action.payload
      }
    default: return state;
  }
};
