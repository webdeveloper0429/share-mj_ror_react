import { SET_CURRENT_SESSION } from '../actions/types';
import isEmpty from 'lodash/isEmpty';

const initialState = {
  isAuthenticated: false,
  fetching: true,
  user: {
    email: null
  }
}

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case SET_CURRENT_SESSION:
      //const {
        //user: { id, email, phone, nickname, avatar },
        //isAuthenticated, fetching
      //} = action.payload;
      return { ...state, ...action.payload }
    default: return state;
  }
}
