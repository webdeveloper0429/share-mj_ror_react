import { FETCH_REQUESTS_REQUEST, FETCH_REQUESTS_SUCCESS, FETCH_REQUESTS_FAILURE, DELETE_REQUESTS_SUCCESS, DELETE_REQUESTS_FAILURE } from '../actions/types';

const initialState = {
  data: {},
  error: {},
  list: [],
  updating: false,
  updated: false,
  fetching: false,
  fetched: false,
  deleted: false
}

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case FETCH_REQUESTS_REQUEST:
      return { 
        ...state,
        fetching: true,
        fetched: false,
        updating: false,
        updated: false,
        error: {},
        data: {},
        list: [],
        deleted: false
      };
    case FETCH_REQUESTS_SUCCESS:
      return { 
        ...state,
        fetching: false,
        fetched: true,
        updating: false,
        updated: false,
        error: {},
        data: {},
        list: action.payload,
        deleted: false
       };
    case FETCH_REQUESTS_FAILURE:
      return { 
        ...state,
        fetching: false,
        fetched: false,
        updating: false,
        updated: false,
        error: action.payload,
        data: {},
        list: [],
        deleted: false
       };
      
      case DELETE_REQUESTS_SUCCESS:
        return { 
          ...state,
          fetching: false,
          fetched: false,
          updating: false,
          updated: false,
          deleted: true,
          error: {},
          data: {},
          list: []
        };
      case DELETE_REQUESTS_FAILURE:
        return { 
          ...state,
          fetching: false,
          fetched: false,
          updating: false,
          updated: false,
          deleted: false,
          error: action.payload,
          data: {},
          list: []
        };
    default: return state;
  }
};