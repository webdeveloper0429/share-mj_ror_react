import { combineReducers } from 'redux';

import flashMessages from './flashMessages';
import session from './session';
import requests from './requests';
import chats from './chats';
import reviews from './reviews';
import users from './users';
import recoveryPassword from './recoveryPassword';

export default combineReducers({
  flashMessages,
  session,
  requests,
  chats,
  reviews,
  users,
  recoveryPassword
});
