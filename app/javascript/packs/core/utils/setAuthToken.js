import { defaults } from 'axios';
import { Cookies } from 'react-cookie';

export default (token) => {
  const cookies = new Cookies();
  if (token) {
    cookies.set('authToken', token);
    defaults.headers.common['Authorization'] = token;
  } else {
    cookies.remove('authToken');
    delete defaults.headers.common['Authorization'];
  }
}

