import {isEmpty, isUndefined} from 'lodash'

export default function validateInput(value, validations) {
  let inputErrors = {};

  for (let key in validations) {
    switch (key) {
      case "max_length":
        if (checkMaxLength(value, validations.max_length)) {
          inputErrors['max_length'] = `Max length is ${validations.max_length} characters`;
        }
        break
      case "min_length":
        if (checkMinLength(value, validations.min_length)) {
          inputErrors['min_length'] = `Min length is ${validations.min_length} characters`;
        }
        break
      case "required":
        if(checkIsRequired(value, true)) {
          inputErrors['required'] = `Field is required`;  
        }
        break
      case "email":
        if(checkEmailFormat(value)) {
          inputErrors['email'] = `Field have an incorrect format`;  
        }
        break
    }
  }

  return {inputErrors, isValid: isEmpty(inputErrors)};
}

const checkMaxLength = (data, length) => {
  if (isUndefined(data) || data.length > length) {
    return true;
  }
}

const checkMinLength = (data, length) => {
  if (isUndefined(data) ||  data.length < length) {
    return true;
  }
}

const checkIsRequired = (data, isRequired) => {
  if ((isUndefined(data) || isEmpty(data)) && isRequired) {
    return true;
  }
}

const checkEmailFormat = (data) => {
  if(!isUndefined(data)) {
    let filter = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!filter.test(data)) {
      return true;
    }
  }
}