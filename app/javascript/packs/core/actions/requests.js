import React from 'react';
import {get, post} from 'axios';
import axios from 'axios';
import sortBy from 'lodash/sortBy';

import {FETCH_REQUESTS_REQUEST, FETCH_REQUESTS_SUCCESS, FETCH_REQUESTS_FAILURE, DELETE_REQUESTS_SUCCESS, DELETE_REQUESTS_FAILURE} from '../actions/types';

import {apiRequestsPath, apiRequestPath} from '../paths';

export const fetchRequestsRequest = () => {
  return {
    type: FETCH_REQUESTS_REQUEST
  }
}

export const fetchRequestsSuccess = (payload) => {
  return {
    type: FETCH_REQUESTS_SUCCESS,
    payload
  }
}

export const fetchRequestsFailure = (payload) => {
  return {
    type: FETCH_REQUESTS_FAILURE,
    payload
  }
}

export const fetchRequests = (type, query) => {
  return dispatch => {
    dispatch(fetchRequestsRequest);
    get(apiRequestsPath(type, query)).then(res => {
      let results = null;
      if(res.data && res.data.result){
        results = sortBy( res.data.result, 'created_at' ).reverse();
      }
      
      dispatch(fetchRequestsSuccess(results));
    }, err => {
      dispatch(fetchRequestsFailure(err.data.result));
    })
  }
}

export const createRequest = (payload) => {
  return (dispatch, _getState) => {
    return post(apiRequestsPath({type: null, query: null}), {request: payload});
  }
};

export const fetchRequest = (id) => {
  return (dispatch, _getState) => {
    return get(apiRequestPath(id));
  }
};

export const removeRequestSucess = (payload) => {
  return {
    type: DELETE_REQUESTS_SUCCESS,
    payload
  }
};

export const removeRequestError= (payload) => {
  return {
    type: DELETE_REQUESTS_FAILURE,
    payload
  }
};

export const deleteRequest = (id) => {
  return dispatch => {
    axios.delete(apiRequestPath(id)).then(res => {
      dispatch(removeRequestSucess(res.data.result));
    }, err => {
      dispatch(removeRequestError(err.data.result));
    })
  }
}
