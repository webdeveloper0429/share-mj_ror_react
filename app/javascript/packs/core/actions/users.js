import {put, get, post} from 'axios';
import {UPDATE_USER_REQUEST, UPDATE_USER_SUCCESS, UPDATE_USER_FAILURE,
FETCH_USER_REQUEST, FETCH_USER_SUCCESS, FETCH_USER_FAILURE, REPORT_USER_REQUEST, REPORT_USER_SUCCESS, REPORT_USER_FAILURE} from '../actions/types';
import {apiUserPath, apiUsersPath, apiUserReportPath} from '../paths';

import { fetchCurrentSession } from './authActions';

export const updateUserRequest = () => {
  return {type: UPDATE_USER_REQUEST}
}

export const updateUserSuccess = (payload) => {
  return {type: UPDATE_USER_SUCCESS, payload}
}

export const updateUserFailure = (payload) => {
  return {type: UPDATE_USER_FAILURE, payload}
}

export const updateUser = (userId, user) => {
  return (dispatch, getState) => {
    dispatch(updateUserRequest());
    put(apiUserPath({ userId }),{user}).then(res => {
      dispatch(fetchCurrentSession());
      dispatch(updateUserSuccess(res.data.result));
    }, err => {
      dispatch(updateUserFailure(err.data.result));
    })
  }
}

export const fetchUserRequest = () => {
  return {type: FETCH_USER_REQUEST}
}

export const fetchUserSuccess = (payload) => {
  return {type: FETCH_USER_SUCCESS, payload}
}

export const fetchUserFailure = (payload) => {
  return {type: FETCH_USER_FAILURE, payload}
}

export const fetchUser = (userId) => {
  return (dispatch, getState) => {
    dispatch(fetchUserRequest());
    get(apiUserPath({ userId })).then(res => {
      dispatch(fetchUserSuccess(res.data.result));
    }, err => {
      dispatch(fetchUserFailure(err.data.result));
    })
  }
}

export const reportUserRequest = () => {
  return {type: REPORT_USER_REQUEST}
}

export const reportUserSuccess = (payload) => {
  return {type: REPORT_USER_SUCCESS, payload}
}

export const reportUserFailure = (payload) => {
  return {type: REPORT_USER_FAILURE, payload}
}

export const reportUser = (message, userId) => {
  return (dispatch, getState) => {
    dispatch(reportUserRequest());
    post(apiUserReportPath({ userId }), {report: {comment: message}}).then(res => {
      dispatch(reportUserSuccess(res.data.result));
    }, err => {
      dispatch(reportUserFailure(err.result));
    })
  }
}