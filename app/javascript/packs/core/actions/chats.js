import { get, post } from 'axios';
import axios from 'axios';
import { SET_CHATS, SET_CHAT, DELETE_CHAT_SUCCESS, DELETE_CHAT_ERROR } from '../actions/types';

import {
  apiChatsPath,
  apiChatPath,
  apiChatMessagesPath
} from '../paths';

export const setChats = (payload) => {
  return {
    type: SET_CHATS,
    payload
  };
};

export const setChat = (payload) => {
  return {
    type: SET_CHAT,
    payload
  };
};

export const fetchChats = ({ userId }) => {
  return (dispatch, _getState) => {
    return get(apiChatsPath({ userId })).then(res => {
      dispatch(setChats(res.data.result));
    });
  }
};

export const fetchChat = ({ userId, chatId }) => {
  return (dispatch, _getState) => {
    return get(apiChatPath({ userId, chatId })).then(res => {
      dispatch(setChat(res.data.result));
    });
  }
};

export const createChat = ({ userId, targetUserId, requestId }) => {
  return (dispatch, _getState) => {
    return post(apiChatsPath({ userId }), {
      chat: { target_user_id: targetUserId, request_id: requestId } 
    });
  }
};

export const createMessage = ({ userId, chatId, message, image }) => {
  return (dispatch, _getState) => {
    let currentState = _getState();
    let { chat } = currentState.chats;
    post(apiChatMessagesPath({ userId, chatId }), {
      message: { 
        body: message,
        image: image
      } 
    }).then(res => {
      chat.messages.push(res.data.result);
      dispatch(setChat(chat));
    });
  }
};

export const handleNewMessage = ({ message }) => {
  return (dispatch, _getState) => {
    let currentState = _getState();
    let { chat } = currentState.chats;
    chat.messages.push(message);
    dispatch(setChat(chat));
  }
};


export const deleteChatSuccess = (payload) => {
  return {
    type: DELETE_CHAT_ERROR,
    payload
  };
};

export const deleteChatError = (payload) => {
  return {
    type: DELETE_CHAT_SUCCESS,
    payload
  };
};


export const deleteChat = (id) => {
  return (dispatch, _getState) => {
    let session = _getState().session;
    axios.delete(apiChatPath({userId:session.user.id, chatId: id})).then(res=>{
      dispatch(deleteChatSuccess(res.data.result));
    }, err=>{
      dispatch(deleteChatError(err.result));
    })
  }
}