import { get, post } from 'axios';
import { SET_REVIEWS } from '../actions/types';

import {
  apiReviewsPath
} from '../paths';

export const setReviews = (payload) => {
  return {
    type: SET_REVIEWS,
    payload
  };
};

export const fetchReviews = (userId) => {
  return (dispatch, _getState) => {
    return get(apiReviewsPath(userId)).then(res => {
      dispatch(setReviews(res.data.result));
    });
  }
};

export const createReview = (userId, { feedback, score }) => {
  return (dispatch, _getState) => {
    return post(apiReviewsPath(userId), {
      review: { feedback, score } 
    });
  }
};
