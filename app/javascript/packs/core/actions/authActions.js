import React from 'react';
import { post, get, put } from 'axios';
import setAuthToken from '../utils/setAuthToken';
import { Cookies } from 'react-cookie';

import {
  SET_CURRENT_SESSION,
  SET_SESSION_FETCHING,
  RECOVERY_PASSWORD_REQUEST_SUCCESS,
  RECOVERY_PASSWORD_REQUEST_ERROR,
  RECOVERY_PASSWORD_COMPLETE_SUCCESS,
  RECOVERY_PASSWORD_COMPLETE_ERROR
} from '../actions/types';

import {
  apiSessionsPath,
  apiUsersPath,
  apiPasswordRecoveries
} from '../paths';

export function setCurrentSession(payload) {
  return {
    type: SET_CURRENT_SESSION,
    payload
  };
}

export function fetchCurrentSession() {
  return dispatch => {
    return get(apiSessionsPath()).then(
      res => {
        const { result } = res.data;
        dispatch(setCurrentSession({ isAuthenticated: true, fetching: false, user: result }));
      },
      err => {
        dispatch(setCurrentSession({ isAuthenticated: false, fetching: false, user: {} }));
      }
    );
  }
}

export function logout() {
  return dispatch => {
    const cookies = new Cookies();
    cookies.remove('authToken');
    setAuthToken(false);
    dispatch(setCurrentSession({ isAuthenticated: false, user: {} }));
  }
}

export const login = ({ email, password }) => {
  return (dispatch, getState) => {
    return post(apiSessionsPath(), {
      session: { email, password }
    }).then(res => {
      const { result } = res.data;
      const { auth_token } = result;
      setAuthToken(auth_token);
      dispatch(setCurrentSession({ isAuthenticated: true, user: result }));
    });
  }
}

export function signUp(data) {
  return (dispatch, getState) => {
    return post(apiUsersPath(), {
      user: {
        email: data.email,
        password: data.password,
        phone: data.phone,
        zip_code: data.zip_code,
        referral_token: data.referral_token
      }
    }).then(res => {
      const { result } = res.data;
      const { auth_token } = result;
      setAuthToken(auth_token);
      dispatch(setCurrentSession({ isAuthenticated: true, user: result }));
    });
  }
}

export function recoveryPasswordRequestSuccess(payload) {
  return {
    type: RECOVERY_PASSWORD_REQUEST_SUCCESS,
    payload
  };
}

export function recoveryPasswordRequestError(payload) {
  return {
    type: RECOVERY_PASSWORD_REQUEST_ERROR,
    payload
  };
}

export function recoveryRequest(email) {
  return (dispatch, getState) => {
    post(apiPasswordRecoveries(), { 
      password_recovery: {
        email: email
      }
    }).then(res => {
      dispatch(recoveryPasswordRequestSuccess(res));
    }, err=>{
      dispatch(recoveryPasswordRequestError(err));
    });
  }
};


export function recoveryPasswordCompleteSuccess(payload) {
  return {
    type: RECOVERY_PASSWORD_COMPLETE_SUCCESS,
    payload
  };
}

export function recoveryPasswordCompleteError(payload) {
  return {
    type: RECOVERY_PASSWORD_COMPLETE_ERROR,
    payload
  };
}

export function recoveryComplete(password, token) {
  return (dispatch, getState) => {
    put(apiPasswordRecoveries(), { 
      password_recovery: {
        password: password,
        token: token
      }
    }).then(res => {
      dispatch(recoveryPasswordCompleteSuccess(res));
    }, err=>{
      dispatch(recoveryPasswordCompleteError(err));
    });
  }
};