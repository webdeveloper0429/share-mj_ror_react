import { apiPath, appPath } from './config';

export function dashboardPath() {
  return `${appPath}/dashboard`;
}

export function staticPagesPath(path) {
  return `${appPath}/${path}`;
}

export function apiSessionsPath() {
  return `${apiPath}/v1/sessions`;
}

export function apiUsersPath(opts={}) {
  const params = opts.byEmail ? `?by_email=${opts.byEmail}` : '';
  return `${apiPath}/v1/users${params}`;
}

export function apiUserPath(opts={}) {
  const userId = opts.userId ? `/${opts.userId}` : '';
  return `${apiPath}/v1/users${userId}`;
}

export function apiUserReportPath(opts={}) {
  const userId = opts.userId ? `/${opts.userId}` : '';
  return `${apiPath}/v1/users${userId}/report`;
}

export const apiRequestsPath = (type='public', query=null) => { 
  switch (type) {
    case 'public':
      return `${apiPath}/v1/requests`;
    case 'my':
      return `${apiPath}/v1/requests?filter=my`;
    case 'search':
      return `${apiPath}/v1/requests?lng=${query.lng}&lat=${query.lat}&radius=${query.radius}`;
    default:
      return `${apiPath}/v1/requests`;
  }
};
export const apiRequestPath = (id) => `${apiPath}/v1/requests/${id}`;

export const apiChatsPath = ({ userId }) => `${apiPath}/v1/users/${userId}/chats`;
export const apiChatPath = ({ userId, chatId }) => `${apiPath}/v1/users/${userId}/chats/${chatId}`;

export const apiChatMessagesPath = (opts) => `${apiChatPath(opts)}/messages`;

export const apiReviewsPath = (userId) => `${apiPath}/v1/users/${userId}/reviews`;
export const apiReviewPath = (userId, id) => `${apiReviewsPath(userId)}/${id}`;

export const apiPasswordRecoveries = () => `${apiPath}/v1/password_recoveries`;
