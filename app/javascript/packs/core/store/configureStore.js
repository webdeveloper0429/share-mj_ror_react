import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';
import { environment } from '../config';

export default function configureStore(initialState) {
  const create = () => {
    if (environment == 'development') {
      const { composeWithDevTools } = require('redux-devtools-extension');
      const { logger } = require('redux-logger');
      return createStore(rootReducer, composeWithDevTools(
        applyMiddleware(thunk, logger)
      ));
    } else {
      return createStore(rootReducer, applyMiddleware(thunk));
    }
  };

  const store = create();

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers')
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}
