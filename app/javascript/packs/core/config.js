export const environment = process.env.NODE_ENV;
export const apiPath = process.env.API_PATH || '/api';
export const cablePath = process.env.WEBSOCKET_PATH || '/websocket';
export const appPath = '';

