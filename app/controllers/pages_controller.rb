class PagesController < ApplicationController
  def home
    @react_entrypoint = 'entrypoint_desktop'
  end

  def dashboard
    @react_entrypoint = 'entrypoint_mobile'
    render 'home'
  end
end
