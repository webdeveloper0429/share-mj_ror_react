module ApiErrorsHandling
  extend ActiveSupport::Concern

  included do
    permission_denied = Sharemj::Exceptions::PermissionDenied

    rescue_from Exception, with: :render_internal_server_error
    rescue_from ActiveRecord::RecordInvalid, with: :render_record_invalid
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
    rescue_from ActionController::RoutingError, with: :render_not_found
    rescue_from permission_denied, with: :render_permission_denied
    rescue_from Sharemj::Exceptions::CustomError, with: :render_custom_error
  end

  def render_record_invalid(error)
    render response_unprocessable_entity error.record.errors.messages
  end

  def render_not_found(error)
    render response_not_found error.message
  end

  def render_internal_server_error(error)
    description = 'Exception caught caused return 500'
    log_error(error, description: description, notify: Rails.env.production?)
    render response_internal_server_error error.message
  end

  def render_permission_denied(error)
    render response_unauthorized error.message
  end

  def render_custom_error(error)
    render response_custom_error error.to_api
  end

  private

  def log_error(error, **opts)
    description = opts[:description]
    notify = opts[:notify] || false

    Sharemj::Logger.error("#{description}: #{error.message}")
    Sharemj::Logger.debug(error.backtrace.join("\n"))

    notify_error(error) if notify
  end

  def notify_error(error)
    session = current_user.nil? ? {} : current_user.to_api
    #Airbrake.notify(error,
      #params: params,
      #session: session
    #)
  end
end

