class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def admin_login
    @email = @password = nil
    if request.post?
      permit = [:email, :password, :remember_me]
      data = params.require(:login).permit(permit).to_h

      @email = data[:email]
      @password = data[:password]

      # Authorizing user
      user = User.authenticate(@email, @password)

      if user && user.role?('admin')
        cookies[:auth_token] = user.auth_token
        redirect_to admin_root_path
      else
        @error = "Invalid credentials"
      end
    else
      render layout: 'admin_login'
    end
  end

  def admin_logout
    cookies[:auth_token] = nil

    redirect_to admin_login_path
  end

  def authenticate_admin_user!
    if cookies[:auth_token].nil?
      redirect_to admin_login_path
      return false
    else
      auth_token = cookies[:auth_token]
      @current_user = User.find_by_auth_token(auth_token)

      if @current_user && @current_user.role?('admin')
        return true
      else
        admin_logout
        return false
      end
    end
  end

  def current_admin_user
    @current_user
  end
end
