class ApiController < ActionController::API
  include ApiErrorsHandling
  include ApiResponseHandling

  attr_reader :current_user

  before_action :verify_auth_token

  def verify_auth_token
    token = request.headers['Authorization'].present? ? request.headers['Authorization'].split(' ').last : nil
    @current_user = User.find_by_auth_token(token)
  end

  def ensure_logged_in
    @current_user || raise(Sharemj::Exceptions::PermissionDenied)
  end
end

