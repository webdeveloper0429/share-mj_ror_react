class Api::V1::SessionsController < ApiController
  before_action :ensure_logged_in, except: [:create]

  def create
    data = params.require(:session).permit(:email, :password)    
    user = User.authenticate!(data[:email], data[:password])
    
    render response_ok user.to_api(layers: [:auth_token])
  end

  def show
    render response_ok current_user.to_api(layers: [:auth_token])
  end
end

