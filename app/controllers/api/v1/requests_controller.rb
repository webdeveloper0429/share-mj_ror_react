class Api::V1::RequestsController < ApiController
  before_action :ensure_logged_in
  before_action :get_request, except: [:index, :create, :show]

  def index
    if params[:query].present?
      requests = Request.preload(:user, :chats).all_active.order(created_at: :desc).near(params[:query])
    elsif params[:lng] && params[:lat] && params[:radius]
      coords = [params[:lng], params[:lat]]
      range = params[:radius]
      requests = Request.preload(:user, :chats).all_active.order(created_at: :desc).near(coords, range)
    else
      requests = case params[:filter]
        when 'my' then Request.preload(:user, :chats).all_active.order(created_at: :desc).all_by_requester_id(@current_user.id)
        else Request.preload(:user).all_active
      end
    end

    render response_ok requests.to_api(layers: [:user])
  end

  def show
    render response_ok Request.find(params[:id]).to_api(layers: [:user])
  end

  def create
    service = RequestService.new(user: current_user)
    request = service.create(filtered_params).request
    current_user.update(zip_code: request.zip)

    render response_created request.to_api
  end

  def update
    service = RequestService.new(request: @request)
    request = service.update(filtered_params).request
    render response_ok request.to_api
  end

  def destroy
    service = RequestService.new(request: @request)
    service.destroy
    render response_ok
  end

  private

  def get_request
    @request = Request.find_by!(id: params[:id], user_id: @current_user.id)
  end

  def filtered_params
    params.require(:request).permit(:zip, :help, :request_type, :weight, :address, :longitude, :latitude)
  end
end

