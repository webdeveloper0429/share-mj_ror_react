class Api::V1::MessagesController < ApiController
  before_action :ensure_logged_in
  before_action :get_user
  before_action :get_chat
  before_action :define_service, except: [:create]

  def create
    message = MessageService.new.create(
      filtered_params.merge({ user: @user, chat: @chat })
    ).message
    
    render response_created message.to_api
  end

  def destroy
    @service.destroy
    render response_ok
  end

  private

  def get_user
    raise 'Restricted permissions' if current_user.id != params[:user_id].to_i
    @user = current_user
  end

  def get_chat
    @chat = UserChat.find_by!(chat_id: params[:chat_id], user_id: @user.id).chat
  end

  def define_service
    @service = MessageService.new(
      Message.find_by!(id: params[:id], chat_id: @chat.id, user_id: @user.id)
    )
  end

  def filtered_params
    params.require(:message).permit(:body, :data_type, :image)
  end
end

