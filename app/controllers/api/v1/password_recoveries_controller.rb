class Api::V1::PasswordRecoveriesController < ApiController
  def show
    User.find_by_password_recovery_token(params[:token])
    render response_ok
  end

  def create
    attrs = params.require(:password_recovery).permit(:email)
    user = User.find_by_email(attrs[:email])
    user.send_password_recovery_request_email

    render response_created
  end

  def update
    attrs = params.require(:password_recovery).permit(:token, :password)

    user = User.find_by_password_recovery_token(attrs[:token])
    user.password = attrs[:password]
    user.save

    render response_ok
  end
end

