class Api::V1::ChatsController < ApiController
  before_action :ensure_logged_in
  before_action :get_user
  before_action :define_service, except: [:index, :create]

  def index
    chats = @user.chats.order(created_at: :desc).includes(:messages, :users)
    chats = (chats.sort_by { |c| c.created_at }).reverse.map { |c| c.to_api(layers: [:last_message]) }
    render response_ok chats
  end

  def show
    render response_ok @service.chat.to_api(layers: [:messages])
  end

  def create
    chat = ChatService.new.create(
      @user,
      User.find(filtered_params[:target_user_id]),
      Request.find(filtered_params[:request_id])
    ).chat
    
    render response_created chat.to_api
  end

  def destroy
    @service.destroy(current_user)
    render response_ok
  end

  private

  def get_user
    raise 'Restricted permissions' if current_user.id != params[:user_id].to_i
    @user = current_user
  end

  def define_service
    @service = ChatService.new(
      UserChat.find_by!(chat_id: params[:id], user_id: @user.id).chat
    )
  end

  def filtered_params
    params.require(:chat).permit(:target_user_id, :request_id)
  end
end

