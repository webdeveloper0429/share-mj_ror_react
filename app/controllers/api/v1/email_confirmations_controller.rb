class Api::V1::EmailConfirmationsController < ApiController
  def create
    attrs = params.require(:email_confirmation).permit(:email)
    user = User.find_by_email!(attrs[:email])

    check_already_confirmed(user)

    user.send_email_confirmation_email

    render response_created
  end

  def update
    attrs = params.require(:email_confirmation).permit(:token)
    user = User.find_by_email_confirmation_token(attrs[:token])

    check_already_confirmed(user)

    user.email_confirmed = true
    user.save

    render response_ok user.to_api(layers: [:auth_token])
  end

  def check_already_confirmed(user)
    if user.email_confirmed
      raise ActionController::RoutingError, 'Email is already confirmed'
    end
  end
end

