class Api::V1::ReviewsController < ApiController
  before_action :ensure_logged_in
  before_action :get_user
  before_action :define_service, except: [:index, :create]

  def index
    data = case(params[:filter] || 'received')
      when 'received'
        @user.received_reviews.preload(:reviewer).to_api(layers: [:reviewer])
      when 'sent'
        @user.sent_reviews.preload(:reviewed).to_api(layers: [:reviewed])
    end
    
    render response_ok data
  end

  def show
    render response_ok @service.review.to_api(layers: [:reviewed, :reviewer])
  end

  def create
    review = ReviewService.new.create(current_user, @user, filtered_params).review
    render response_created review.to_api(layers: [:reviewed])
  end

  def update
    review = @service.update(filtered_params).review
    render response_ok review.to_api(layers: [:reviewed])
  end

  def destroy
    @service.destroy
    render response_ok
  end

  private

  def get_user
    @user = User.find(params[:user_id])
  end

  def define_service
    @service = ReviewService.new(
      Review.find_by!(
        id: params[:id],
        reviewer_id: current_user.id,
        reviewed_id: @user.id
      )
    )
  end

  def filtered_params
    params.require(:review).permit(:feedback, :score)
  end
end

