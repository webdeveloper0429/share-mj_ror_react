class Api::V1::UsersController < ApiController
  before_action :ensure_logged_in, except: [:index, :create]

  def create
    if params[:user][:referral_token]
      referral_user = User.find_by_referral_token(params[:user][:referral_token])
      referral_user.update(credits: (referral_user.credits || 0) + User::REFERRAL_CREDITS)
    end
    user = User.create(filtered_params.merge(sign_up_ip: request.remote_ip, credits: User::DEFAULT_CREDITS))

    if user.valid?
      render response_created(user.to_api(layers: [:auth_token]))
    else
      render response_unprocessable_entity(user.errors.messages)
    end
  end

  def update
    current_user.update!(filtered_params)
    render response_ok current_user.to_api
  end

  def report
    target_user = User.find(params[:user_id])
    if target_user.id == current_user.id
      raise ArgumentError.new("You can't report your self")
    else
      Report.find_or_create_by(
        complainer_id: current_user.id,
        target_id: target_user.id,
        comment: params.require(:report).permit(:comment)[:comment]
      )
      render response_ok
    end
  end

  def show
    render response_ok User.find(params[:id]).to_api
  end

  private

  def filtered_params
    params.require(:user).permit(:email, :password, :first_name, :last_name, :phone, :nickname, :avatar)
  end
end

