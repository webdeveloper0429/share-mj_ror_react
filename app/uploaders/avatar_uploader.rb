class AvatarUploader < ApplicationUploader

  version :thumb do
    process resize_to_fit: [100, 100]
  end

  def extension_whitelist
    %w(jpg jpeg gif png)
  end
end

