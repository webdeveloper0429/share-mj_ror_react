class Review < ApplicationRecord
  include ApiExtension

  STATUS_ACTIVE = 'active'

  belongs_to :reviewer, class_name: 'User', foreign_key: 'reviewer_id'
  belongs_to :reviewed, class_name: 'User', foreign_key: 'reviewed_id'

  def api_attributes(layers)
    attrs = {
      id: id,
      score: score,
      feedback: feedback,
      created_at: created_at.iso8601
    }

    if layers.include? :reviewer
      attrs[:reviewer] = reviewer.to_api
    end
    
    if layers.include? :reviewed
      attrs[:reviewed] = reviewed.to_api
    end

    attrs
  end  
end
