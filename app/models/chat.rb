class Chat < ApplicationRecord
  include ApiExtension
  
  attr_accessor :user1_id, :user2_id
 
  has_many :user_chats, dependent: :destroy
  has_many :users, through: :user_chats
  has_many :messages, { dependent: :destroy }, -> { visible }
  belongs_to :request

  validates :key, presence: true, length: { minimum: 2 }

  before_validation do
    self.key ||= Chat.ids_to_key([user1_id, user2_id])
  end

  after_create do
    if user1_id.present? && user2_id.present? 
      UserChat.create(chat_id: id, user_id: user1_id)
      UserChat.create(chat_id: id, user_id: user2_id)
      self.user1_id = nil
      self.user2_id = nil
    end

    ChatChannel.broadcast_to(self, {
      action: ChatChannel::ACTION_NEW_CHAT, 
      data: to_api
    })
  end

  after_destroy do
    ChatChannel.broadcast_to(self, {
      action: ChatChannel::ACTION_DESTROYED_CHAT, 
      data: { id: id }
    })
  end

  def api_attributes(layers)
    attrs = {
      id: id,
      users: users.to_api,
      created_at: created_at.iso8601
    }

    if layers.include?(:last_message) && messages.last
      attrs[:last_message] = messages.last.to_api(layers: [:short])
    end
    
    if layers.include?(:messages)
      attrs[:messages] = messages.order(:id).to_api
    end

    attrs
  end

  def self.ids_to_key(ids)
    ids.sort.join
  end

  def self.find_by_ids(ids)
    Chat.find_by(key: ids_to_key(ids))
  end
end

