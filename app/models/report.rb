class Report < ApplicationRecord
  belongs_to :complainer, class_name: 'User', foreign_key: 'complainer_id'
  belongs_to :target, class_name: 'User', foreign_key: 'target_id' 
end

