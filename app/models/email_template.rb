class EmailTemplate < ApplicationRecord
  def render(vars={})
    {
      subject: Liquid::Template.parse(subject).render(vars),
      body: Liquid::Template.parse(body).render(vars)
    }
  end
end

