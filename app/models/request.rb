class Request < ApplicationRecord
  include ApiExtension

  STATUS_ACTIVE = 'active'
  STATUS_INACTIVE = 'inactive'

  TYPES = ['sativa', 'hybrid', 'indica', 'dont_know']
  HELPS = ['mj', 'edibles', 'dont_know']
  WEIGHTS = ['1/8', '1/4', '1/2', '1', '2', 'dont_know']

  geocoded_by :address do |obj, results|
    if geo = results.first
      obj.latitude = geo.coordinates[0]
      obj.longitude = geo.coordinates[1]
      obj.city = geo.city
      obj.zip ||= geo.postal_code
      obj.state = geo.state
      obj.state_code = geo.state_code
    end
  end 
  reverse_geocoded_by :latitude, :longitude do |obj, results|
    if geo = results.first
      obj.city = geo.city
      obj.zip ||= geo.postal_code
      obj.state = geo.state
      obj.state_code = geo.state_code
    end
  end

  store_accessor :metadata, [:address, :zip, :help, :request_type, :weight, :city, :zip, :state, :state_code]

  belongs_to :user
  has_many :chats

  validates :status, inclusion: { in: [STATUS_ACTIVE, STATUS_INACTIVE] }
  validates :request_type, inclusion: { in: TYPES }
  validates :help, inclusion: { in: HELPS }
  validates :weight, inclusion: { in: WEIGHTS }

  after_validation do
    if address.blank?
      reverse_geocode if latitude.present? && longitude.present? && latitude_changed? && longitude_changed?
    else
      geocode
    end
  end

  def api_attributes(layers)
    attrs = {
      id: id,
      status: status,
      user_id: user_id,
      zip: zip,
      help: help,
      type: request_type,
      weight: weight,
      cost: cost, 
      created_at: created_at.iso8601
    }

    if layers.include?(:user)
      attrs[:user] = user.to_api
      # TODO redo in future (to many requests to database)!!!
      attrs[:chats_count] = chats.count
    end

    attrs
  end

  def self.all_active
    Request.where(status: STATUS_ACTIVE)
  end

  def self.all_by_requester_id(id)
    Request.where(user_id: id)
  end

  def cost
    case weight
    when WEIGHTS[0] then 2
    when WEIGHTS[1] then 6
    when WEIGHTS[2] then 4 #TODO remove in future
    when WEIGHTS[3] then 8
    when WEIGHTS[4] then 10
    else 2
    end
  end
end
