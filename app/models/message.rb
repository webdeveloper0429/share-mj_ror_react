class Message < ApplicationRecord
  include ApiExtension
  
  STATUS_VISIBLE = 'visible'
  STATUS_DELETED = 'deleted'

  DATA_TYPE_TEXT = 'text'
  DATA_TYPE_IMAGE = 'image'

  mount_base64_uploader :image, MessageImageUploader

  belongs_to :chat
  belongs_to :user

  scope :visible, -> { where(status: STATUS_VISIBLE) }

  after_create do
    ChatChannel.broadcast_to(chat, {
      action: ChatChannel::ACTION_NEW_MESSAGE, 
      data: to_api
    })
  end

  after_destroy do
    ChatChannel.broadcast_to(chat, {
      action: ChatChannel::ACTION_DESTROYED_MESSAGE, 
      data: { id: id }
    })
  end

  def api_attributes(layers)
    attrs = {
      id: id,
      data_type: data_type || DATA_TYPE_TEXT,
      body: layers.include?(:short) ? short_body : body,
      user: {
        id: user.id,
        name: user.nickname,
        avatar_url: 'https://fakeimg.pl/100x100/'
      },
      created_at: created_at.iso8601
    }

    if data_type == DATA_TYPE_IMAGE
      attrs[:image_url] = image.url rescue nil
    end

    attrs
  end

  def short_body
    "#{body[0..12]}..."
  end
end

