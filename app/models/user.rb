class User < ApplicationRecord
  include ApiExtension

  DEFAULT_CREDITS = 1000.freeze
  REFERRAL_CREDITS = 5.freeze

  attr_accessor :password, :sudo 

  store_accessor :metadata, [
    :sign_up_ip,
    :stripe_customer_id,
    :bio,
    :requests_count,
    :zip_code
  ]

  has_many :roles, dependent: :destroy
  has_many :requests
  has_many :user_chats
  has_many :chats, through: :user_chats
  has_many :received_reviews, class_name: 'Review', foreign_key: 'reviewed_id'
  has_many :sent_reviews, class_name: 'Review', foreign_key: 'reviewer_id'

  mount_base64_uploader :avatar, AvatarUploader

  before_create :generate_auth_token
  before_save :encrypt_new_password, if: :password_present?
  after_save :change_admin_status, if: :sudo?
  after_create :send_welcome_email
  after_create :send_email_confirmation_email

  #validates :first_name, presence: true, length: { :in => 1..30 }
  #validates :last_name, presence: true, length: { :in => 1..30 }
  validates :email, presence: true, uniqueness: true, format: { with: /\A[^@\s]+@([^@.\s]+\.)*[^@.\s]+\z/ }
  validates :password, confirmation: true
  validates_length_of :password, in: 6..20, on: :create

  def self.authenticate(email, password)
    user = where('lower(email) = ?', email.downcase).first 
    if user
      encrypted_password = BCrypt::Engine.hash_secret(password, user.salt)
      user.encrypted_password == encrypted_password ? user : nil
    else
      nil
    end
  end
  
  def self.authenticate!(email, password)
    user = authenticate(email, password) 
    user || raise(Sharemj::Exceptions::PermissionDenied)
  end

  def self.find_by_email_confirmation_token(token)
    data = JWT.decode token, Rails.application.secrets.jwt_email_confirmation_key
    user = find(data[0]['uid'])

    user
  rescue
    raise ActiveRecord::RecordNotFound, "Can't find user by email confirmation token id"
  end

  def self.find_by_password_recovery_token(token)
    data = JWT.decode token, Rails.application.secrets.jwt_password_recovery_key
    user = find(data[0]['uid'])

    user
  rescue
    raise ActiveRecord::RecordNotFound, "Can't find user by token id"
  end

  def self.find_by_referral_token(token)
    data = JWT.decode token, Rails.application.secrets.jwt_password_recovery_key
    find(data[0]['uid'])
  rescue
    raise ActiveRecord::RecordNotFound, "Can't find user by token id"
  end

  def api_attributes(layers)
   attrs = {
      id: id,
      email: email,
      first_name: first_name,
      last_name: last_name,
      nickname: nickname,
      phone: phone,
      phone_confirmed: phone_confirmed,
      email_confirmed: email_confirmed,
      bio: bio,
      location: ['New York', 'Paris', 'Kiev', 'Berlin', 'Warsaw'].sample,
      rating: rand(1..5),
      requests_count: requests_count || rand(0..30),
      avatar: {
        thumb_url: avatar.thumb.url ? avatar.thumb.url : 'https://fakeimg.pl/100x100/',
        url: avatar.url ? avatar.url : 'https://fakeimg.pl/500x500/'
      },
      zip_code: zip_code,
      created_at: created_at.iso8601
    }

    if layers.include?(:auth_token)
      attrs[:auth_token] = "Bearer #{auth_token}"
      attrs[:referral_token] = referral_token
      attrs[:credits] = credits || 0.0
    end

    attrs
  end

  def encrypt_new_password
    self.salt = BCrypt::Engine.generate_salt
    self.encrypted_password = BCrypt::Engine.hash_secret(password, salt)
    self.password = nil
  end

  def change_admin_status
    if admin
      unless role?(Role::ROLE_ADMIN)
        Role.create(user_id: id, name: Role::ROLE_ADMIN)
      end
    else
      if role?(Role::ROLE_ADMIN)
        role = Role.find_by(user_id: id, name: Role::ROLE_ADMIN)
        role.destroy unless role.nil?
      end
    end
  end

  def admin=(val)
    @admin = val == true || val == "1"
  end

  def admin
    @admin.nil? ? role?(Role::ROLE_ADMIN) : @admin
  end

  def role?(role)
    roles.exists?(name: role)
  end

  def password_present?
    password.present?
  end
 
  def generate_auth_token
    self.auth_token = Digest::SHA1.hexdigest([Time.now, rand].join)
  end

  def sudo?
    sudo == true || sudo == "1"
  end

  def to_s
    "#{first_name} #{last_name} (#{email})"
  end

  def email_confirmation_token
    if @email_confirmation_token.nil?
      payload = { uid: id, exp: 10.minutes.from_now.to_i }
      jwt_key = Rails.application.secrets.jwt_email_confirmation_key
      @email_confirmation_token = JWT.encode(payload, jwt_key, 'HS512')
    end

    @email_confirmation_token
  rescue
    return nil
  end

  def send_email_confirmation_email
    UserMailer.email_confirmation_email(email, email_confirmation_token).deliver_now
  end

  def send_password_recovery_request_email
    UserMailer.create_password_recovery_request_email(email, password_recovery_token).deliver_now
  end

  def password_recovery_token
    if @password_recovery_token.nil?
      expires = 10.minutes.from_now.to_i

      payload = { uid: id, exp: expires }
      jwt_key = Rails.application.secrets.jwt_password_recovery_key
      @password_recovery_token = JWT.encode(payload, jwt_key, 'HS512')
    end

    @password_recovery_token
  end

  def stripe_customer_exists?
    !stripe_customer_id.blank?
  end

  def stripe_customer
    @stripe_customer ||= Stripe::Customer.retrieve(stripe_customer_id)
  end

  def referral_token
    expires = 30.days.from_now.to_i

    payload = { uid: id, exp: expires }
    jwt_key = Rails.application.secrets.jwt_password_recovery_key
    JWT.encode(payload, jwt_key, 'HS512')
  end

  private

  def send_welcome_email
    UserMailer.welcome_email(self).deliver_now
  end
end

