module ApiExtension
  extend ActiveSupport::Concern

  def to_api(**opts)
    layers = opts[:layers] || []

    unless layers.is_a? Array
      raise ArgumentError, 'Argument "layers" is not an array'
    end

    api_attributes(layers)
  end

  class_methods do
    def to_api(**opts)
      layers = opts[:layers] || []
      paginate = opts[:paginate]

      unless layers.is_a? Array
        raise ArgumentError, 'Argument "layers" is not an array'
      end

      records = all
      records = records.paginate(paginate) unless paginate.nil?

      records.map { |m| m.api_attributes(layers) }
    end
  end
end

