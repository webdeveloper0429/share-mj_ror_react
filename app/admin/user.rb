ActiveAdmin.register User do
  permit_params :email, :first_name, :last_name, :phone, :phone_confirmed, :email_confirmed, :admin, :password, :sudo

  index do
    column :id
    column :email
    column :name, sortable: :first_name do |u|
      "#{u.first_name} #{u.last_name}"
    end
    column :phone
    column :sign_up_ip
    column :admin do |u|
      u.admin ? status_tag("yes", :ok) : status_tag("no")
    end
    actions
  end

  form do |f|
    inputs 'Details' do
      input :email, as: :email
      input :first_name
      input :last_name
      input :phone
      input :phone_confirmed
      input :email_confirmed
      input :admin, as: :boolean
      input :sudo, as: :hidden, input_html: { value: "1" } 
    end
    inputs 'Password change' do
      input :password, label: 'New password', as: :password
    end
    actions
  end
end

