ActiveAdmin.register EmailTemplate do
  permit_params :subject, :body
  actions :all, except: [:new, :create, :destroy]

  index do
    column :name
    column :updated_at

    actions
  end

  form do |f|
    inputs 'General' do
      input :subject
      input :body, as: :html_editor
    end
    actions
  end
end

