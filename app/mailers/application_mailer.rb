class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'

  private

  def send_mail(email, tpl_name, tpl_vars)
    tpl = EmailTemplate.find_by_name(tpl_name).render(tpl_vars)

    mail(
      to: email,
      from: 'ShareMJ <noreply@sharemj.com>', 
      content_type: "text/html",
      subject: tpl[:subject],
      body: tpl[:body]
    )
  rescue
    puts "Email template doesn't exists"
  end
end
