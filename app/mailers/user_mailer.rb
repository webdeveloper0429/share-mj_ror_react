class UserMailer < ApplicationMailer
  def welcome_email(user)
    send_mail(user.email, 'welcome', {
      'full_name' => 'Someone'
    })
  end

  def email_confirmation_email(email, token)
    send_mail(email, 'email_confirmation', {
      'url' => "#{Rails.application.secrets.app_path}/email-confirmation?token=#{token}"
    })
  end

  def create_password_recovery_request_email(email, token)
    send_mail(email, 'password_recovery_request', {
      'url' => "#{Rails.application.secrets.app_path}/password-recovery/edit?token=#{token}"
    })
  end
end

