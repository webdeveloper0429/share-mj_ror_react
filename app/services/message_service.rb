class MessageService
  attr_reader :message

  def initialize(message = nil)
    @message = message
  end

  def create(opts)
    opts[:data_type] = opts[:image] ? Message::DATA_TYPE_IMAGE : Message::DATA_TYPE_TEXT
    @message = Message.create!(opts)
    self
  end

  def destroy
    @message.destroy
    self
  end
end

