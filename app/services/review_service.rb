class ReviewService
  attr_reader :review

  def initialize(review = nil)
    @review = review
  end

  def create(reviewer, reviewed, attrs)
    @review = Review.create(
      attrs.merge({ reviewer_id: reviewer.id, reviewed_id: reviewed.id })
    )
    self
  end

  def update(attrs)
    @review.update!(attrs)
    self
  end

  def destroy
    @review.destroy
    self
  end
end

