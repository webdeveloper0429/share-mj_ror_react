class RequestService
  attr_reader :request

  def initialize(attrs)
    @user = attrs[:user]
    @request = attrs[:request]
  end

  def create(attrs)
    @request = Request.create!(attrs.merge(user_id: @user.id, status: Request::STATUS_ACTIVE)) 
    self
  end
  
  def update(attrs)
    @request.update!(attrs)
    self
  end

  def destroy
    @request.destroy
    self
  end
end

