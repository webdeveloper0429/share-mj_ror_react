class ChatService
  attr_reader :chat

  def initialize(chat = nil)
    @chat = chat
  end

  def create(current_user, target_user, request)
    @chat = Chat.find_by_ids([current_user.id, target_user.id]) 

    unless @chat
      if (current_user.credits.to_f < request.cost)
        raise ArgumentError.new("Current user doesn't have enought credits to open chat")
      end

      @chat = Chat.create(
        user1_id: current_user.id,
        user2_id: target_user.id,
        request_id: request.id
      )

      current_user.update(credits: current_user.credits.to_f - request.cost)
    end

    chats_count = Chat.where(request_id: request.id).count
    if chats_count >= 3
      request.update(status: Request::STATUS_INACTIVE)
    end

    self
  end

  def destroy(user)
    UserChat.find_by!(chat_id: @chat.id, user_id: user.id).destroy
    self
  end
end

