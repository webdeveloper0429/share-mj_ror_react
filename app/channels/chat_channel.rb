class ChatChannel < ApplicationCable::Channel
  ACTION_NEW_MESSAGE = 'new_message'
  ACTION_DESTROYED_MESSAGE = 'destroyed_message'
  ACTION_NEW_CHAT = 'new_chat'
  ACTION_DESTROYED_CHAT = 'destroyed_chat'

  def subscribed
    chat = Chat.find(params[:chat_id])
    stream_for chat
  end
end

