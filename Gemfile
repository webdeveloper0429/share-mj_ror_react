source 'https://rubygems.org'

ruby '2.4.1'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.1'
# Pg is the Ruby interface to the PostgreSQL 
gem 'pg', '~> 0.20.0'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0.6'
#gem 'sass-rails', github: "rails/sass-rails"
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker'
# The fastest JSON parser and object serializer
gem 'oj', '~> 2.18.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
# Simple, efficient background processing for Ruby
gem 'sidekiq', '~> 4.2.10'
# The administration framework for Ruby on Rails.
gem 'activeadmin', '~> 1.0.0'
# Rich text editor for Active Admin using wysihtml5.
gem 'active_admin_editor', github: 'boontdustie/active_admin_editor'
# A secure, non-evaling end user template engine with aesthetic markup.
gem 'liquid', '~> 4.0.0'
# Faker, a port of Data::Faker from Perl, is used to easily generate fake data
gem 'faker', require: false
# Middleware that will make Rack-based apps CORS compatible
gem 'rack-cors', '~> 0.4.1'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
# A pure ruby implementation of the RFC 7519 OAuth JSON Web Token (JWT) standard.
gem 'jwt'
# Provides object geocoding (by street or IP address), reverse geocoding
gem 'geocoder'
# Stripe is the easiest way to accept payments online. See https://stripe.com for details.
gem 'stripe', '~> 2.11.0'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'

# Upload files in your Ruby applications, map them to a range of ORMs, store them on different backends.
gem 'carrierwave', '~> 1.1.0'
# This gem can be useful, if you need to upload files to your API from mobile devises.
gem 'carrierwave-base64', '~> 2.5.0'
# Manipulate images with minimal use of memory via ImageMagick / GraphicsMagick
gem 'mini_magick', '~> 4.7.0'
# This library can be used as a module for `fog` or as standalone provider to use the Amazon Web Services in applications..
gem 'fog-aws', '~> 1.3.0'
# ExecJS lets you run JavaScript code from Ruby.
gem 'execjs'
# Call JavaScript code and manipulate JavaScript objects from Ruby
gem 'therubyracer', :platforms => :ruby

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13.0'
  # WebDriver is a tool for writing automated tests of websites
  gem 'selenium-webdriver'
  # Process manager for applications with multiple components
  gem 'foreman', require: false
  # Great Ruby dubugging companion: pretty print Ruby objects to visualize their structure
  gem 'awesome_print'
end

group :test do
  # rspec-rails is a testing framework for Rails
  gem 'rspec-rails', '3.5.2'
  # factory_girl_rails provides integration between factory_girl and rails
  gem 'factory_girl_rails'
  # Strategies for cleaning databases. Can be used to ensure a clean state for testing.
  gem 'database_cleaner'
  # factory_girl provides a framework and DSL for defining and using factories
  gem 'factory_girl'
  # Fake (In-memory) driver for redis-rb
  gem 'fakeredis', require: "fakeredis/rspec"
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  # The Listen gem listens to file modifications and notifies you about the changes. Works everywhere!
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  # Makes spring watch files using the listen gem.
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
