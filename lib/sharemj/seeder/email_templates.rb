module Sharemj
  module Seeder
    def self.seed_email_templates
      EmailTemplate.destroy_all
      EmailTemplate.create(
        name: 'welcome',
        subject: 'Welcome to ShareMJ',
        body: 'Welcome to ShareMJ community'
      )
      EmailTemplate.create(
        name: 'email_confirmation',
        subject: 'Please confirm your email',
        body: 'Please go to this <a href="{{ url }}">link</a> to confirm your email'
      )
      EmailTemplate.create(
        name: 'password_recovery_request',
        subject: 'Password recovery request',
        body: 'Please go to this <a href="{{ url }}">link</a> to reset your password'
      )
    end
  end
end

