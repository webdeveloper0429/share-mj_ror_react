require 'faker'

module Sharemj
  module Seeder
    def self.seed_demo_requests(count = 1)
      Array.new(count) do
        Request.create(
          user_id: User.all.sample.id,
          status: Request::STATUS_ACTIVE,
          zip: Faker::Address.zip,
          help: Request::HELPS.sample,
          request_type: Request::TYPES.sample,
          weight: Request::WEIGHTS.sample,
          address: Faker::Address.state,
        )
      end
    end
  end
end

