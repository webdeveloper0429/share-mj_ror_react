require 'faker'

module Sharemj
  module Seeder
    def self.seed_users
      user = User.create(
        email: 'admin@admin.com',
        phone: '0674685678',
        password: 'Password1'
      )
      Role.create(user: user, name: Role::ROLE_ADMIN)    
    end

    def self.seed_demo_users(count=1, password='Password1')
      Array.new(count) do
        User.create(
          email: Faker::Internet.email,
          phone: Faker::PhoneNumber.cell_phone,
          first_name: Faker::Name.first_name,
          last_name: Faker::Name.last_name,
          nickname: Faker::Internet.user_name,
          password: password,
          phone_confirmed: true,
          email_confirmed: true,
          credits: [1.0, 10.0, 25.0, 100.0].sample
        )
      end
    end
  end
end

