require 'faker'

module Sharemj
  module Seeder
    def self.seed_demo_reviews(count = 1)
      users = User.all
      users.first
      Array.new(count) do
        users = users.shuffle
        Review.create(
          reviewer_id: users[0].id,
          reviewed_id: users[1].id,
          feedback: Faker::Lorem.sentence,
          score: [1.0, 2.0, 3.0, 4.0, 5.0].sample
        )
      end
    end
  end
end

