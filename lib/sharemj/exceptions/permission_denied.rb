module Sharemj
  module Exceptions
    class PermissionDenied < StandardError
      def message
        "Error 550, you can't have access to this object."
      end
    end
  end
end

