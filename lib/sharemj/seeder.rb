require 'sharemj/seeder/users'
require 'sharemj/seeder/email_templates'
require 'sharemj/seeder/requests'
require 'sharemj/seeder/reviews'

module Sharemj::Seeder
  def self.seed
    seed_users
    seed_email_templates
  end 

  def self.seed_demo_data
    seed_demo_users(50)
    seed_demo_requests(50)
    seed_demo_reviews(50)
  end
end
