class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.bigint :complainer_id
      t.bigint :target_id
      t.string :comment
      t.timestamps
    end
    add_index :reports, [:complainer_id, :target_id], unique: true
  end
end
