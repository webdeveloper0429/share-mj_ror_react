class CreateChats < ActiveRecord::Migration[5.1]
  def change
    create_table :chats do |t|
      t.integer :key   
      t.timestamps
    end

    add_index :chats, :key
  end
end
