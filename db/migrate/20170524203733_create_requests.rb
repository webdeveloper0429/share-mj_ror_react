class CreateRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :requests do |t|
      t.belongs_to :user
      t.string :status, default: Request::STATUS_ACTIVE
      t.float :latitude
      t.float :longitude
      t.hstore :metadata
      t.timestamps
    end

    add_index :requests, [:latitude, :longitude]
  end
end
