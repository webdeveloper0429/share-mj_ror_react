class AddDataTypeAndImageToMessages < ActiveRecord::Migration[5.1]
  def change
    add_column :messages, :data_type, :string
    add_column :messages, :image, :string
  end
end
