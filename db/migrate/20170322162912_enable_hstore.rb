class EnableHstore < ActiveRecord::Migration::Compatibility::V5_0
  def self.up
    enable_extension 'hstore'
  end

  def self.down
    disable_extension 'hstore'
  end
end
