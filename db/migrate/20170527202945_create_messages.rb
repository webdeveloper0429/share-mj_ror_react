class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.belongs_to :chat
      t.belongs_to :user
      t.string :status, null: false, default: Message::STATUS_VISIBLE
      t.string :body, limit: 420, null: false
      t.timestamps
    end
  end
end
