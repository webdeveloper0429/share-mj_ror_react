class AddRequestIdToChats < ActiveRecord::Migration[5.1]
  def change
    add_column :chats, :request_id, :string
  end
end
