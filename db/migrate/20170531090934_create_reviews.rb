class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.integer :reviewer_id, null: false
      t.integer :reviewed_id, null: false
      t.string :status, default: Review::STATUS_ACTIVE
      t.float :score, null: false
      t.string :feedback, limit: 420, null: false 
      t.timestamps
    end

    add_index :reviews, :reviewer_id
    add_index :reviews, :reviewed_id
  end
end
