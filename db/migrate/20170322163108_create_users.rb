class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :status
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :nickname
      t.float :credits
      t.boolean :phone_confirmed, default: false
      t.boolean :email_confirmed, default: false
      t.string :encrypted_password, default: false
      t.string :salt
      t.string :auth_token
      t.json :metadata
      t.timestamps
    end

    add_index :users, :email, unique: true
    add_index :users, :auth_token, unique: true
  end
end
