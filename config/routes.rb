Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  root to: 'pages#home'
  get 'dashboard', to: redirect('/dashboard/home')
  get '/dashboard/*path', to: 'pages#dashboard'
  get '/password-recovery', to: 'pages#home'
  get '/password-recovery/*path', to: 'pages#home'
  get '/privacy-policy', to: 'pages#home'
  get '/terms-of-service', to: 'pages#home'
  get '/copyright', to: 'pages#home'

  scope :api, module: :api, as: :api do
    namespace :v1 do
      resource :sessions, only: [:create, :show]
      resources :users, only: [:create, :update, :show] do
        post :report
        resources :reviews, only: [:index, :show, :create, :update, :destroy]
        resources :chats, only: [:create, :show, :index, :destroy] do
          resources :messages, only: [:create, :destroy]
        end
      end
      resource :email_confirmations, only: [:create, :update]
      resource :password_recoveries, only: [:show, :create, :update]
      resources :requests, only: [:index, :show, :create, :update, :destroy]
    end
  end

  ActiveAdmin.routes(self)

  get 'admin/login', to: 'application#admin_login'
  post 'admin/login', to: 'application#admin_login'
  get 'admin/logout', to: 'application#admin_logout'
end
