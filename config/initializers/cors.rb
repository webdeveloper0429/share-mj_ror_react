Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    domains = Rails.application.secrets.cors_origins
    origins(domains.blank? ? '*' : domains.split(',').map { |h| h.strip })

    resource '*',
      headers: :any,
      methods: [:get, :post, :put, :patch, :delete, :options, :head]
  end
end

