redis_opts = { url: Rails.application.secrets.redis_url }
redis_opts.merge!(driver: Redis::Connection::Memory) if defined?(Redis::Connection::Memory)

Sidekiq.configure_server do |config|
  config.redis = redis_opts
end

Sidekiq.configure_client do |config|
  config.redis = redis_opts
end

