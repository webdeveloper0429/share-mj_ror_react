CarrierWave.configure do |config|
  case Rails.env
    when 'production'
      config.fog_provider = 'fog/aws'
      config.fog_credentials = {
          provider: 'AWS',
          aws_access_key_id: Rails.application.secrets.aws_access_key_id,
          aws_secret_access_key: Rails.application.secrets.aws_secret_access_key,
          region: Rails.application.secrets.aws_region
      }
      config.storage = :fog
      config.fog_directory = Rails.application.secrets.aws_s3_bucket
      config.fog_public = true
      config.cache_dir = "#{Rails.root}/tmp/uploads"
    when 'test'
      config.storage = :file
      config.enable_processing = false
      config.cache_dir = "#{Rails.root}/tmp/test_uploads/tmp"
  end
end

