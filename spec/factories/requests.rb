FactoryGirl.define do
  factory :request do
    status Request::STATUS_ACTIVE
    zip '20001'
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    help { Request::HELPS.sample }
    request_type { Request::TYPES.sample } 
    weight { Request::WEIGHTS.sample }
  end

  factory :request_by_ip, class: Request do
    status Request::STATUS_ACTIVE
    zip '20001'
    address { Faker::Internet.public_ip_v4_address }
    help { Request::HELPS.sample }
    request_type { Request::TYPES.sample } 
    weight { Request::WEIGHTS.sample }
  end
end

