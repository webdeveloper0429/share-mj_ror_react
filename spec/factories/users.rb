FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    phone { Faker::PhoneNumber.phone_number }
    nickname { Faker::Internet.user_name }
    email do
      "#{first_name}.#{last_name}@#{["gmail.com","outlook.com","yandex.ua"].sample}".downcase
    end
    password 'Password1'
  end
end

