FactoryGirl.define do
  factory :message do
    body { Faker::Lorem.sentence(rand(1..20)) }
  end
end

