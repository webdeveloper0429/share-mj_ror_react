FactoryGirl.define do
  factory :review do
    score { [1.0, 2.0, 3.0, 4.0, 5.0].sample }
    feedback { Faker::Lorem.sentence(rand(1..20)) }
  end
end

