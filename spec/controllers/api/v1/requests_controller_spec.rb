require "spec_helper"  
require 'sharemj/seeder'

describe Api::V1::RequestsController , :type => :api do  
  context 'requests API' do
    before do
      Sharemj::Seeder.seed_email_templates
      @user = create(:user)
      @headers = { "HTTP_AUTHORIZATION" => "Bearer #{@user.auth_token}" }
    end

    it 'get public requests list' do
      users = Array.new(10) do
        create(:user)
      end
      requests = Array.new(rand(10)) do
        create(:request, user_id: users.sample.id)
      end

      get "/api/v1/requests", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Array
      expect(json['result'].count).to eq requests.count

      json['result'].each do |result|
        expect(result['id']).to be_a Integer
        expect(result['zip']).to be_a String
        expect(result['help']).to be_a String
        expect(result['type']).to be_a String
        expect(result['cost']).to be_a Integer
        expect(result['chats_count']).to be_a Integer
        expect(result['created_at']).to be_a String
      end
    end

    it 'get my requests list' do
      users = Array.new(10) do
        create(:user)
      end
      my_requests = Array.new(rand(10)) do
        create(:request, user_id: @user.id)
      end
      Array.new(rand(30)) do
        create(:request, user_id: users.sample.id)
      end

      get "/api/v1/requests", { filter: 'my' }, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Array
      expect(json['result'].count).to eq my_requests.count

      json['result'].each do |result|
        expect(result['id']).to be_a Integer
        expect(result['zip']).to be_a String
        expect(result['help']).to be_a String
        expect(result['type']).to be_a String
        expect(result['cost']).to be_a Integer
        expect(result['chats_count']).to be_a Integer
        expect(result['created_at']).to be_a String
      end
    end

    it 'get public requests by location' do
      users = Array.new(10) do
        create(:user)
      end
      Array.new(5) do
        create(:request_by_ip, user_id: users.sample.id, address: 'new york')
      end
      Array.new(5) do
        create(:request_by_ip, user_id: users.sample.id, address: 'washington dc')
      end

      get "/api/v1/requests", { query: 'New York' }, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Array
      expect(json['result'].count).to eq 5

      json['result'].each do |result|
        expect(result['id']).to be_a Integer
        expect(result['zip']).to be_a String
        expect(result['help']).to be_a String
        expect(result['type']).to be_a String
        expect(result['cost']).to be_a Integer
        expect(result['chats_count']).to be_a Integer
        expect(result['created_at']).to be_a String
      end
    end

    it 'create request with coords' do
      payload = {
        request: attributes_for(:request)
      }
      post "/api/v1/requests", payload, @headers

      expect(last_response.status).to eq 201
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash

      result = json['result']
      expect(result['id']).to be_a Integer
      
      expect(result['zip']).to eq(payload[:request][:zip])
      expect(result['help']).to eq(payload[:request][:help])
      expect(result['type']).to eq(payload[:request][:request_type])
      expect(result['cost']).to be_a Integer
      expect(result['weight']).to eq(payload[:request][:weight])
      expect(result['created_at']).to be_a String
    end

    it 'create request with ip' do
      payload = {
        request: attributes_for(:request_by_ip)
      }
      post "/api/v1/requests", payload, @headers

      expect(last_response.status).to eq 201
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash

      result = json['result']
      expect(result['id']).to be_a Integer
      
      expect(result['zip']).to eq(payload[:request][:zip])
      expect(result['help']).to eq(payload[:request][:help])
      expect(result['type']).to eq(payload[:request][:request_type])
      expect(result['weight']).to eq(payload[:request][:weight])
      expect(result['created_at']).to be_a String

      user = User.find(@user.id)
      expect(user.zip_code).to eq(payload[:request][:zip]) 
    end

    it 'update request' do
      attrs = attributes_for(:request)
      request = create(:request, user_id: @user.id)

      payload = {
        request: {
          zip: attrs[:zip],
          help: attrs[:help],
          request_type: attrs[:request_type]
        }
      }
      put "/api/v1/requests/#{request.id}", payload, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      
      result = json['result']
      expect(result['id']).to be_a Integer
      expect(result['zip']).to eq(payload[:request][:zip])
      expect(result['help']).to eq(payload[:request][:help])
      expect(result['type']).to eq(payload[:request][:request_type])
      expect(result['created_at']).to be_a String
    end

    it 'show request' do
      request = create(:request, user_id: @user.id)

      get "/api/v1/requests/#{request.id}", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      
      result = json['result']
      expect(result['id']).to be_a Integer
      expect(result['zip']).to be_a String
      expect(result['help']).to be_a String
      expect(result['type']).to be_a String
      expect(result['created_at']).to be_a String
    end

    it 'destroy request' do
      request = create(:request, user_id: @user.id)
      
      headers = { "HTTP_AUTHORIZATION" => "Bearer #{@user.auth_token}" }
      delete "/api/v1/requests/#{request.id}", {}, headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
     
      exists = Request.exists?(request.id)
      expect(exists).to eq(false)
    end
  end
end

