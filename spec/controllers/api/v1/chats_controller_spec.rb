require "spec_helper"  
require 'sharemj/seeder'

describe Api::V1::ChatsController , :type => :api do  
  context 'chats API' do
    before do
      Sharemj::Seeder.seed_email_templates
      @user = create(:user, credits: User::DEFAULT_CREDITS)
      @headers = { "HTTP_AUTHORIZATION" => "Bearer #{@user.auth_token}" }
      @seed_chats = Proc.new do
        users = Array.new(5) { create(:user, credits: User::DEFAULT_CREDITS) }
        chats = users.map do |u|
          request = create(:request, user_id: u.id)
          chat = ChatService.new.create(@user, u, request).chat
          rand(1..5).times do
            user_id = [@user.id, u.id].sample
            create(:message, chat_id: chat.id, user_id: user_id)
          end

          chat
        end

        { users: users, chats: chats }
      end
    end

    it 'get chats' do
      seeds = @seed_chats.call
      chats = seeds[:chats]

      get "/api/v1/users/#{@user.id}/chats", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Array
      expect(json['result'].count).to eq chats.count

      json['result'].each do |result|
        expect(result['id']).to be_a Integer
        expect(result['users']).to be_a Array
        expect(result['users'].count).to eq 2
        expect(result['last_message']).to be_a Hash
        expect(result['last_message']['id']).to be_a Integer
        expect(result['last_message']['body']).to be_a String
        expect(result['last_message']['user']).to be_a Hash
        expect(result['last_message']['user']['id']).to be_a Integer
        expect(result['last_message']['user']['name']).to be_a String
        expect(result['last_message']['user']['avatar_url']).to be_a String
        expect(result['last_message']['created_at']).to be_a String
        expect(result['created_at']).to be_a String
      end
    end
 
    it 'create chat' do
      users = Array.new(2) { create(:user, credits: User::DEFAULT_CREDITS) }
      request = create(:request, user_id: users[1].id)

      payload = {
        chat: {
          target_user_id: users[1].id,
          request_id: request.id
        }
      }
      post "/api/v1/users/#{@user.id}/chats", payload, @headers

      expect(last_response.status).to eq 201
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      result = json['result']

      expect(result['id']).to be_a Integer
      expect(result['created_at']).to be_a String
    end

    it 'create 3 chats' do
      users = Array.new(3) { create(:user, credits: User::DEFAULT_CREDITS) }
      request = create(:request, user_id: users[1].id)

      users.each do |u|
        payload = {
          chat: {
            target_user_id: u.id,
            request_id: request.id
          }
        }
        post "/api/v1/users/#{@user.id}/chats", payload, @headers

        expect(last_response.status).to eq 201
        expect(json['success']).to be true
        expect(json['result']).to be_a Hash
        result = json['result']

        expect(result['id']).to be_a Integer
        expect(result['created_at']).to be_a String
      end

      request = Request.find(request.id)
      expect(request.status).to eq Request::STATUS_INACTIVE
    end

    it 'show chat' do
      seeds = @seed_chats.call
      chats = seeds[:chats]
  
      get "/api/v1/users/#{@user.id}/chats/#{chats[0].id}", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      result = json['result']

      expect(result['id']).to be_a Integer
      expect(result['created_at']).to be_a String

      expect(result['messages']).to be_a Array
      result['messages'].each do |result|
        expect(result['id']).to be_a Integer
        expect(result['body']).to be_a String
        expect(result['user']).to be_a Hash
        expect(result['user']['id']).to be_a Integer
        expect(result['user']['name']).to be_a String
        expect(result['user']['avatar_url']).to be_a String
        expect(result['created_at']).to be_a String
      end
    end

    it 'destroy chat' do
      seeds = @seed_chats.call
      chat = seeds[:chats][0]
      user_chat = UserChat.find_by(chat_id: chat.id, user_id: @user.id)
      expect(user_chat).not_to be_nil
  
      delete "/api/v1/users/#{@user.id}/chats/#{chat.id}", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      
      user_chat = UserChat.find_by(chat_id: chat.id, user_id: @user.id)
      expect(user_chat).to be_nil
    end
  end
end

