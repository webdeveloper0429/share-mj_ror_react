require 'rails_helper'
require 'sharemj/seeder'

describe 'Password recovery API', type: :api do
  before do
    Sharemj::Seeder.seed_email_templates
  end

  before(:each) do
    @user = create(:user)
  end

  it 'Create password recovery request' do
    post "/api/v1/password_recoveries", password_recovery: { email: @user.email }
    expect(last_response.status).to eq(201)

    last_delivery = ActionMailer::Base.deliveries.last
    results = /<a[^>]* href="([^"]*)"/.match(last_delivery.body.raw_source)

    recovery_token = results[1].split('?token=').last
  end

  it 'Verify recovery request token' do
    post "/api/v1/password_recoveries", password_recovery: { email: @user.email }
    expect(last_response.status).to eq(201)

    last_delivery = ActionMailer::Base.deliveries.last
    results = /<a[^>]* href="([^"]*)"/.match(last_delivery.body.raw_source)

    token = results[1].split('?token=').last

    get "/api/v1//password_recoveries?token=#{token}"

    expect(last_response.status).to eq(200)
  end

  it 'Create a new password' do
    post "/api/v1/password_recoveries", password_recovery: { email: @user.email }
    expect(last_response.status).to eq(201)

    last_delivery = ActionMailer::Base.deliveries.last
    results = /<a[^>]* href="([^"]*)"/.match(last_delivery.body.raw_source)
    token = results[1].split('?token=').last

    params = {
      token: token,
      password: 'newpass123'
    }
    put "/api/v1/password_recoveries", password_recovery: params

    expect(last_response.status).to eq(200)

    user = User.authenticate(@user.email, 'newpass123')
    expect(user).not_to be_nil
  end

end

