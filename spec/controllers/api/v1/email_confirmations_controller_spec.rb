require 'rails_helper'
require 'sharemj/seeder'

describe 'Email confirmations API', type: :api do
  before do
    Sharemj::Seeder.seed_email_templates
  end

  before(:each) do
    @user = create(:user)

    last_delivery = ActionMailer::Base.deliveries.last
    results = /<a[^>]* href="([^"]*)"/.match(last_delivery.body.raw_source)
    confirmation_token = results[1].split('/').last

    expect(confirmation_token).not_to be_blank
  end

  it 'Create email confirmation email' do
    attrs = {
      email: @user.email
    }

    post "/api/v1/email_confirmations", email_confirmation: attrs
    expect(last_response.status).to eq(201)

    last_delivery = ActionMailer::Base.deliveries.last
    results = /<a[^>]* href="([^"]*)"/.match(last_delivery.body.raw_source)
    confirmation_token = results[1].split('?token=').last

    expect(confirmation_token).not_to be_blank
  end

  it 'Create email confirmation request' do
    attrs = {
      email: @user.email
    }

    post "/api/v1/email_confirmations", email_confirmation: attrs
    expect(last_response.status).to eq(201)

    last_delivery = ActionMailer::Base.deliveries.last
    results = /<a[^>]* href="([^"]*)"/.match(last_delivery.body.raw_source)
    confirmation_token = results[1].split('?token=').last

    expect(confirmation_token).not_to be_blank
  end

  it 'Confirm email' do
    attrs = {
      token: @user.email_confirmation_token
    }
    put "/api/v1/email_confirmations", email_confirmation: attrs

    expect(last_response.status).to eq(200)

    user = User.find(@user.id)
    expect(user.email_confirmed).to be(true)
  end

end

