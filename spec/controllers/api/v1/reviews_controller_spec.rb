require "spec_helper"  
require 'sharemj/seeder'

describe Api::V1::ReviewsController , :type => :api do  
  context 'chats API' do
    before do
      Sharemj::Seeder.seed_email_templates
      @user = create(:user)
      @headers = { "HTTP_AUTHORIZATION" => "Bearer #{@user.auth_token}" }

      @reviewed_user = create(:user)
      @reviewers = Array.new(5) { create(:user) }
      @request = create(:request, user_id: @reviewed_user.id) 
    end

    it 'get received reviews' do
      reviews = Array.new(rand(1..6)) do
        create(:review, reviewer_id: @reviewers.sample.id, reviewed_id: @reviewed_user.id)
      end

      get "/api/v1/users/#{@reviewed_user.id}/reviews", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Array
      expect(json['result'].count).to eq reviews.count

      json['result'].each do |result|
        expect(result['id']).to be_a Integer
        expect(result['score']).to be_a Float
        expect(result['feedback']).to be_a String
        expect(result['reviewer']).to be_a Hash
        expect(result['created_at']).to be_a String
      end
    end

    it 'get sent reviews' do
      reviews = Array.new(rand(1..6)) do
        create(:review, reviewed_id: @reviewers.sample.id, reviewer_id: @reviewed_user.id)
      end

      get "/api/v1/users/#{@reviewed_user.id}/reviews", { filter: 'sent' }, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Array
      expect(json['result'].count).to eq reviews.count

      json['result'].each do |result|
        expect(result['id']).to be_a Integer
        expect(result['score']).to be_a Float
        expect(result['feedback']).to be_a String
        expect(result['reviewed']).to be_a Hash
        expect(result['created_at']).to be_a String
      end
    end

    it 'create review' do
      payload = {
        review: attributes_for(:review)
      }

      post "/api/v1/users/#{@reviewed_user.id}/reviews", payload, @headers

      expect(last_response.status).to eq 201
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      result = json['result']

      expect(result['id']).to be_a Integer
      expect(result['score']).to be_a Float
      expect(result['feedback']).to be_a String
      expect(result['reviewed']).to be_a Hash
      expect(result['created_at']).to be_a String
    end

    it 'show review' do
      review = create(:review, reviewer_id: @user.id, reviewed_id: @reviewed_user.id)

      get "/api/v1/users/#{@reviewed_user.id}/reviews/#{review.id}", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      result = json['result']

      expect(result['id']).to be_a Integer
      expect(result['score']).to be_a Float
      expect(result['feedback']).to be_a String
      expect(result['reviewer']).to be_a Hash
      expect(result['reviewed']).to be_a Hash
      expect(result['created_at']).to be_a String
    end

    it 'update review' do
      review = create(:review, reviewer_id: @user.id, reviewed_id: @reviewed_user.id)
      
      payload = {
        review: attributes_for(:review)
      }

      put "/api/v1/users/#{@reviewed_user.id}/reviews/#{review.id}", payload, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      result = json['result']

      expect(result['id']).to be_a Integer
      expect(result['score']).to be_a Float
      expect(result['feedback']).to be_a String
      expect(result['reviewed']).to be_a Hash
      expect(result['created_at']).to be_a String
    end
    
    it 'destroy review' do
      review = create(:review, reviewer_id: @user.id, reviewed_id: @reviewed_user.id)

      delete "/api/v1/users/#{@reviewed_user.id}/reviews/#{review.id}", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
    
      expect(Review.exists?(review.id)).to be false
    end
  end
end

