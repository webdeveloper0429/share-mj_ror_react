require "spec_helper"  
require 'sharemj/seeder'

describe Api::V1::SessionsController , :type => :api do  
  context 'when the cat does not exist' do
    before do
      Sharemj::Seeder.seed_email_templates
    end

    it 'login user' do
      attrs = attributes_for(:user)
      password = attrs[:password]
      user = User.create(attrs)

      payload = {
        session: {
          email: user.email,
          password: password
        }
      }
      post "/api/v1/sessions", payload

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash

      result = json['result']
      expect(result['first_name']).to eq(attrs[:first_name])
      expect(result['last_name']).to eq(attrs[:last_name])
      expect(result['email']).to eq(attrs[:email])
      expect(result['location']).to be_a String
      expect(result['rating']).to be_a Integer
      expect(result['requests_count']).to be_a Integer
      expect(result['auth_token']).to be_a String
      expect(result['referral_token']).to be_a String
    end

    it 'login user with upper case email' do
      attrs = attributes_for(:user)
      password = attrs[:password]
      user = User.create(attrs)

      payload = {
        session: {
          email: user.email.upcase,
          password: password
        }
      }
      post "/api/v1/sessions", payload

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash

      result = json['result']
      expect(result['first_name']).to eq(attrs[:first_name])
      expect(result['last_name']).to eq(attrs[:last_name])
      expect(result['email']).to eq(attrs[:email])
      expect(result['auth_token']).to be_a String
      expect(result['referral_token']).to be_a String
      expect(result['credits']).to be_a Float
    end


    it 'show user session' do
      user = create(:user)

      headers = { "HTTP_AUTHORIZATION" => "Bearer #{user.auth_token}" }
      get "/api/v1/sessions", {}, headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash

      result = json['result']
      expect(result['first_name']).to eq(user.first_name)
      expect(result['last_name']).to eq(user.last_name)
      expect(result['email']).to eq(user.email)
      expect(result['auth_token']).to be_a String
      expect(result['referral_token']).to be_a String
      expect(result['credits']).to be_a Float
    end
  end
end

