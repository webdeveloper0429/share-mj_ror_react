require "spec_helper"  
require 'sharemj/seeder'

describe Api::V1::MessagesController , :type => :api do  
  context 'messages API' do
    before do
      Sharemj::Seeder.seed_email_templates
      @user = create(:user, credits: User::DEFAULT_CREDITS)
      @headers = { "HTTP_AUTHORIZATION" => "Bearer #{@user.auth_token}" }
      @request = create(:request, user_id: @user.id) 
      @chat = ChatService.new.create(@user, create(:user), @request).chat 
    end

    it 'create message' do
      payload = { message: attributes_for(:message) }
      post "/api/v1/users/#{@user.id}/chats/#{@chat.id}/messages", payload, @headers

      expect(last_response.status).to eq 201
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      result = json['result']

      expect(result['id']).to be_a Integer
      expect(result['data_type']).to eq Message::DATA_TYPE_TEXT
      expect(result['body']).to be_a String
      expect(result['user']).to be_a Hash
      expect(result['user']['id']).to be_a Integer
      expect(result['user']['name']).to be_a String
      expect(result['user']['avatar_url']).to be_a String
      expect(result['created_at']).to be_a String

      expect(result['body']).to eq payload[:message][:body]
    end

    it 'create image message' do
      base64_image = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB3CAMAAAD/7HQ1AAAAA1BMVEUAAACnej3aAAAAJElEQVRoge3BAQ0AAADCoPdPbQ8HFAAAAAAAAAAAAAAAAABwZjg/AAHohd1fAAAAAElFTkSuQmCC'

      attrs = attributes_for(:message)

      payload = { message: attrs.merge(image: base64_image) }
      post "/api/v1/users/#{@user.id}/chats/#{@chat.id}/messages", payload, @headers

      expect(last_response.status).to eq 201
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      result = json['result']

      expect(result['id']).to be_a Integer
      expect(result['data_type']).to eq Message::DATA_TYPE_IMAGE
      expect(result['image_url']).to be_a String
      expect(result['body']).to be_a String
      expect(result['user']).to be_a Hash
      expect(result['user']['id']).to be_a Integer
      expect(result['user']['name']).to be_a String
      expect(result['user']['avatar_url']).to be_a String
      expect(result['created_at']).to be_a String

      expect(result['body']).to eq payload[:message][:body]
    end

    it 'destroy message' do
      message = create(:message, user_id: @user.id, chat_id: @chat.id)

      delete "/api/v1/users/#{@user.id}/chats/#{@chat.id}/messages/#{message.id}", {}, @headers

      expect(last_response.status).to eq 200
      expect(json['success']).to be true

      expect(Message.exists?(message.id)).to be false
    end
  end
end

