require "spec_helper"  
require 'sharemj/seeder'

describe Api::V1::UsersController , :type => :api do
  base64_image = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB3CAMAAAD/7HQ1AAAAA1BMVEUAAACnej3aAAAAJElEQVRoge3BAQ0AAADCoPdPbQ8HFAAAAAAAAAAAAAAAAABwZjg/AAHohd1fAAAAAElFTkSuQmCC'

  context 'when the cat does not exist' do
    before do
      Sharemj::Seeder.seed_email_templates
    end

    it 'create user' do
      attrs = attributes_for(:user).merge(avatar: base64_image)
      post "/api/v1/users", user: attrs

      expect(last_response.status).to eq 201
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash

      result = json['result']
      expect(result['first_name']).to eq(attrs[:first_name])
      expect(result['last_name']).to eq(attrs[:last_name])
      expect(result['email']).to eq(attrs[:email])
      expect(result['nickname']).to eq(attrs[:nickname])
      expect(result['phone']).to eq(attrs[:phone])
      expect(result['phone_confirmed']).to eq(false)
      expect(result['email_confirmed']).to eq(false)
      expect(result['auth_token']).to be_a String
      expect(result['referral_token']).to be_a String
      expect(result['credits']).to be_a Float
      expect(result['avatar']['url']).to be_a String
      expect(result['avatar']['thumb_url']).to be_a String

      user = User.authenticate(attrs[:email], attrs[:password])
      expect(user).not_to be nil
      expect(user.sign_up_ip).to eq '127.0.0.1'
    end

    it 'create referral user' do
      referral_user = create(:user)
      attrs = attributes_for(:user)
      attrs[:referral_token] = referral_user.referral_token
      post "/api/v1/users", user: attrs

      expect(last_response.status).to eq 201
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash

      result = json['result']
      expect(result['first_name']).to eq(attrs[:first_name])
      expect(result['last_name']).to eq(attrs[:last_name])
      expect(result['email']).to eq(attrs[:email])
      expect(result['nickname']).to eq(attrs[:nickname])
      expect(result['phone']).to eq(attrs[:phone])
      expect(result['phone_confirmed']).to eq(false)
      expect(result['email_confirmed']).to eq(false)
      expect(result['auth_token']).to be_a String
      expect(result['referral_token']).to be_a String
      expect(result['credits']).to be_a Float

      user = User.authenticate(attrs[:email], attrs[:password])
      expect(user).not_to be nil
      expect(user.sign_up_ip).to eq '127.0.0.1'

      referral_user = User.find(referral_user.id)
      expect(referral_user.credits).to eq User::REFERRAL_CREDITS
    end

    it 'update user' do
      attrs = attributes_for(:user)
      user = User.create(attrs)
      
      attrs[:email] = 'random.person@mail.com'

      payload = { format: :json, user: attrs }
      headers = { "HTTP_AUTHORIZATION" => "Bearer #{user.auth_token}" }
      put "/api/v1/users/#{user.id}", payload, headers 

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      
      result = json['result']
      expect(result['first_name']).to eq(attrs[:first_name])
      expect(result['last_name']).to eq(attrs[:last_name])
      expect(result['email']).to eq(attrs[:email])
      expect(result['nickname']).to eq(attrs[:nickname])
    end

    it 'show user' do
      attrs = attributes_for(:user)
      user = User.create(attrs)

      headers = { "HTTP_AUTHORIZATION" => "Bearer #{user.auth_token}" }
      get "/api/v1/users/#{user.id}", {}, headers 

      expect(last_response.status).to eq 200
      expect(json['success']).to be true
      expect(json['result']).to be_a Hash
      
      result = json['result']
      expect(result['first_name']).to eq(attrs[:first_name])
      expect(result['last_name']).to eq(attrs[:last_name])
      expect(result['email']).to eq(attrs[:email])
      expect(result['phone']).to eq(attrs[:phone])
      expect(result['phone_confirmed']).to eq(false)
      expect(result['email_confirmed']).to eq(false)
      expect(result['auth_token']).to be_nil
      expect(result['referral_token']).to be_nil
    end
    
    it 'report user' do
      users = Array.new(2) { create :user }

      headers = { "HTTP_AUTHORIZATION" => "Bearer #{users[0].auth_token}" }
      payload = { report: { comment: '' } }
      post "/api/v1/users/#{users[1].id}/report", payload, headers 

      expect(last_response.status).to eq 200
      expect(json['success']).to be true

      report = Report.last
      expect(report.complainer_id).to eq users.first.id
      expect(report.target_id).to eq users.second.id
    end

    it 'user try to report him self' do
      users = Array.new(1) { create :user }

      headers = { "HTTP_AUTHORIZATION" => "Bearer #{users[0].auth_token}" }
      payload = { report: { comment: '' } }
      post "/api/v1/users/#{users[0].id}/report", payload, headers 
      
      expect(last_response.status).to eq 500
      expect(json['success']).to be false
    end

  end
end

